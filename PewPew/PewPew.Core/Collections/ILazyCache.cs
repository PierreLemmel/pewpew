﻿using System.Collections.Generic;

namespace PewPew
{
    public interface ILazyCache<TKey, TValue> : IReadOnlyDictionary<TKey, TValue> where TKey : notnull { }
}