using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace PewPew
{
    public class LazyCache<TKey, TValue> : ILazyCache<TKey, TValue> where TKey : notnull
    {
        private readonly IDictionary<TKey, TValue> cache;
        private readonly Func<TKey, TValue> resultFactory;

        public LazyCache(Func<TKey, TValue> resultFactory)
        {
            this.resultFactory = resultFactory;
            cache = new Dictionary<TKey, TValue>();
        }

        public TValue this[TKey key]
        {
            get
            {
                if (!TryGetValue(key, out TValue result))
                {
                    result = resultFactory(key);
                    cache[key] = result;
                }

                return result;
            }
        }

        public IEnumerable<TKey> Keys => cache.Keys;
        public IEnumerable<TValue> Values => cache.Values;
        public int Count => cache.Count;

        public bool ContainsKey(TKey key) => cache.ContainsKey(key);

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => cache.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => cache.GetEnumerator();

        public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TValue value) => cache.TryGetValue(key, out value);
    }
}