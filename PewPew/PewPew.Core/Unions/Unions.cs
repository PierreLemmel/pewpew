using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace PewPew
{
    public static class Unions
    {
        public static Union<T1, T2, T3, T4, T5, T6, T7, T8>? AsUnionOrDefault<T1, T2, T3, T4, T5, T6, T7, T8>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            where T8 : notnull
            => value switch
            {
                T1 t1 => new(t1),
                T2 t2 => new(t2),
                T3 t3 => new(t3),
                T4 t4 => new(t4),
                T5 t5 => new(t5),
                T6 t6 => new(t6),
                T7 t7 => new(t7),
                T8 t8 => new(t8),
                _ => null
            };

        public static Union<T1, T2, T3, T4, T5, T6, T7, T8> AsUnion<T1, T2, T3, T4, T5, T6, T7, T8>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            where T8 : notnull
            => value.AsUnionOrDefault<T1, T2, T3, T4, T5, T6, T7, T8>()!;

        public static bool TryMatchOneOf<T1, T2, T3, T4, T5, T6, T7, T8>(this object value, [NotNullWhen(true)] out Union<T1, T2, T3, T4, T5, T6, T7, T8>? result)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            where T8 : notnull
            => (result = value.AsUnionOrDefault<T1, T2, T3, T4, T5, T6, T7, T8>()) is not null;



        public static Union<T1, T2, T3, T4, T5, T6, T7>? AsUnionOrDefault<T1, T2, T3, T4, T5, T6, T7>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            => value switch
            {
                T1 t1 => new(t1),
                T2 t2 => new(t2),
                T3 t3 => new(t3),
                T4 t4 => new(t4),
                T5 t5 => new(t5),
                T6 t6 => new(t6),
                T7 t7 => new(t7),
                _ => null
            };

        public static Union<T1, T2, T3, T4, T5, T6, T7> AsUnion<T1, T2, T3, T4, T5, T6, T7>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            => value.AsUnionOrDefault<T1, T2, T3, T4, T5, T6, T7>()!;

        public static bool TryMatchOneOf<T1, T2, T3, T4, T5, T6, T7>(this object value, [NotNullWhen(true)] out Union<T1, T2, T3, T4, T5, T6, T7>? result)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            => (result = value.AsUnionOrDefault<T1, T2, T3, T4, T5, T6, T7>()) is not null;



        public static Union<T1, T2, T3, T4, T5, T6>? AsUnionOrDefault<T1, T2, T3, T4, T5, T6>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            => value switch
            {
                T1 t1 => new(t1),
                T2 t2 => new(t2),
                T3 t3 => new(t3),
                T4 t4 => new(t4),
                T5 t5 => new(t5),
                T6 t6 => new(t6),
                _ => null
            };

        public static Union<T1, T2, T3, T4, T5, T6> AsUnion<T1, T2, T3, T4, T5, T6>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            => value.AsUnionOrDefault<T1, T2, T3, T4, T5, T6>()!;

        public static bool TryMatchOneOf<T1, T2, T3, T4, T5, T6>(this object value, [NotNullWhen(true)] out Union<T1, T2, T3, T4, T5, T6>? result)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            => (result = value.AsUnionOrDefault<T1, T2, T3, T4, T5, T6>()) is not null;



        public static Union<T1, T2, T3, T4, T5>? AsUnionOrDefault<T1, T2, T3, T4, T5>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            => value switch
            {
                T1 t1 => new(t1),
                T2 t2 => new(t2),
                T3 t3 => new(t3),
                T4 t4 => new(t4),
                T5 t5 => new(t5),
                _ => null
            };

        public static Union<T1, T2, T3, T4, T5> AsUnion<T1, T2, T3, T4, T5>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            => value.AsUnionOrDefault<T1, T2, T3, T4, T5>()!;

        public static bool TryMatchOneOf<T1, T2, T3, T4, T5>(this object value, [NotNullWhen(true)] out Union<T1, T2, T3, T4, T5>? result)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            => (result = value.AsUnionOrDefault<T1, T2, T3, T4, T5>()) is not null;



        public static Union<T1, T2, T3, T4>? AsUnionOrDefault<T1, T2, T3, T4>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            => value switch
            {
                T1 t1 => new(t1),
                T2 t2 => new(t2),
                T3 t3 => new(t3),
                T4 t4 => new(t4),
                _ => null
            };

        public static Union<T1, T2, T3, T4> AsUnion<T1, T2, T3, T4>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            => value.AsUnionOrDefault<T1, T2, T3, T4>()!;

        public static bool TryMatchOneOf<T1, T2, T3, T4>(this object value, [NotNullWhen(true)] out Union<T1, T2, T3, T4>? result)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            => (result = value.AsUnionOrDefault<T1, T2, T3, T4>()) is not null;



        public static Union<T1, T2, T3>? AsUnionOrDefault<T1, T2, T3>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            => value switch
            {
                T1 t1 => new(t1),
                T2 t2 => new(t2),
                T3 t3 => new(t3),
                _ => null
            };

        public static Union<T1, T2, T3> AsUnion<T1, T2, T3>(this object value)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            => value.AsUnionOrDefault<T1, T2, T3>()!;

        public static bool TryMatchOneOf<T1, T2, T3>(this object value, [NotNullWhen(true)] out Union<T1, T2, T3>? result)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            => (result = value.AsUnionOrDefault<T1, T2, T3>()) is not null;



        public static Union<T1, T2>? AsUnionOrDefault<T1, T2>(this object value)
            where T1 : notnull
            where T2 : notnull
            => value switch
            {
                T1 t1 => new(t1),
                T2 t2 => new(t2),
                _ => null
            };

        public static Union<T1, T2> AsUnion<T1, T2>(this object value)
            where T1 : notnull
            where T2 : notnull
            => value.AsUnionOrDefault<T1, T2>()!;

        public static bool TryMatchOneOf<T1, T2>(this object value, [NotNullWhen(true)] out Union<T1, T2>? result)
            where T1 : notnull
            where T2 : notnull
            => (result = value.AsUnionOrDefault<T1, T2>()) is not null;
    }
}