using System;

namespace PewPew
{
    public sealed class Union<T1, T2>
        where T1 : notnull
        where T2 : notnull
    {
        private readonly object value;

        public Union(T1 value) => this.value = value;
        public Union(T2 value) => this.value = value;

        public void Match(Action<T1> case1, Action<T2> case2)
        {
            switch (value)
            {
                case T1 v1: case1(v1); break;
                case T2 v2: case2(v2); break;
            }
        }

        public TResult Match<TResult>(Func<T1, TResult> case1, Func<T2, TResult> case2) => value switch
        {
            T1 v1 => case1(v1),
            T2 v2 => case2(v2),
            _ => throw new InvalidOperationException($"Unexpected type in union: {value.TypeName()}")
        };

        public static explicit operator T1(Union<T1, T2> union) => (T1)union.value;
        public static explicit operator T2(Union<T1, T2> union) => (T2)union.value;

        public override string? ToString() => value.ToString();
    }

    public sealed class Union<T1, T2, T3>
        where T1 : notnull
        where T2 : notnull
        where T3 : notnull
    {
        private readonly object value;

        public Union(T1 value) => this.value = value;
        public Union(T2 value) => this.value = value;
        public Union(T3 value) => this.value = value;

        public void Match(Action<T1> case1, Action<T2> case2, Action<T3> case3)
        {
            switch (value)
            {
                case T1 v1: case1(v1); break;
                case T2 v2: case2(v2); break;
                case T3 v3: case3(v3); break;
            }
        }


        public TResult Match<TResult>(
            Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3)
            => value switch
            {
                T1 v1 => case1(v1),
                T2 v2 => case2(v2),
                T3 v3 => case3(v3),
                _ => throw new InvalidOperationException($"Unexpected type in union: {value.TypeName()}")
            };

        public static explicit operator T1(Union<T1, T2, T3> union) => (T1)union.value;
        public static explicit operator T2(Union<T1, T2, T3> union) => (T2)union.value;
        public static explicit operator T3(Union<T1, T2, T3> union) => (T3)union.value;

        public override string? ToString() => value.ToString();
    }

    public sealed class Union<T1, T2, T3, T4>
        where T1 : notnull
        where T2 : notnull
        where T3 : notnull
        where T4 : notnull
    {
        private readonly object value;

        public Union(T1 value) => this.value = value;
        public Union(T2 value) => this.value = value;
        public Union(T3 value) => this.value = value;
        public Union(T4 value) => this.value = value;

        public void Match(Action<T1> case1, Action<T2> case2, Action<T3> case3, Action<T4> case4)
        {
            switch (value)
            {
                case T1 v1: case1(v1); break;
                case T2 v2: case2(v2); break;
                case T3 v3: case3(v3); break;
                case T4 v4: case4(v4); break;
            }
        }


        public TResult Match<TResult>(
            Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3, Func<T4, TResult> case4)
            => value switch
            {
                T1 v1 => case1(v1),
                T2 v2 => case2(v2),
                T3 v3 => case3(v3),
                T4 v4 => case4(v4),
                _ => throw new InvalidOperationException($"Unexpected type in union: {value.TypeName()}")
            };

        public static explicit operator T1(Union<T1, T2, T3, T4> union) => (T1)union.value;
        public static explicit operator T2(Union<T1, T2, T3, T4> union) => (T2)union.value;
        public static explicit operator T3(Union<T1, T2, T3, T4> union) => (T3)union.value;
        public static explicit operator T4(Union<T1, T2, T3, T4> union) => (T4)union.value;

        public override string? ToString() => value.ToString();
    }

    public sealed class Union<T1, T2, T3, T4, T5>
        where T1 : notnull
        where T2 : notnull
        where T3 : notnull
        where T4 : notnull
        where T5 : notnull
    {
        private readonly object value;

        public Union(T1 value) => this.value = value;
        public Union(T2 value) => this.value = value;
        public Union(T3 value) => this.value = value;
        public Union(T4 value) => this.value = value;
        public Union(T5 value) => this.value = value;

        public void Match(
            Action<T1> case1, Action<T2> case2, Action<T3> case3, Action<T4> case4, Action<T5> case5)
        {
            switch (value)
            {
                case T1 v1: case1(v1); break;
                case T2 v2: case2(v2); break;
                case T3 v3: case3(v3); break;
                case T4 v4: case4(v4); break;
                case T5 v5: case5(v5); break;
            }
        }


        public TResult Match<TResult>(
            Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3, Func<T4, TResult> case4, Func<T5, TResult> case5)
            => value switch
            {
                T1 v1 => case1(v1),
                T2 v2 => case2(v2),
                T3 v3 => case3(v3),
                T4 v4 => case4(v4),
                T5 v5 => case5(v5),
                _ => throw new InvalidOperationException($"Unexpected type in union: {value.TypeName()}")
            };

        public static explicit operator T1(Union<T1, T2, T3, T4, T5> union) => (T1)union.value;
        public static explicit operator T2(Union<T1, T2, T3, T4, T5> union) => (T2)union.value;
        public static explicit operator T3(Union<T1, T2, T3, T4, T5> union) => (T3)union.value;
        public static explicit operator T4(Union<T1, T2, T3, T4, T5> union) => (T4)union.value;
        public static explicit operator T5(Union<T1, T2, T3, T4, T5> union) => (T5)union.value;

        public override string? ToString() => value.ToString();
    }

    public sealed class Union<T1, T2, T3, T4, T5, T6>
        where T1 : notnull
        where T2 : notnull
        where T3 : notnull
        where T4 : notnull
        where T5 : notnull
        where T6 : notnull
    {
        private readonly object value;

        public Union(T1 value) => this.value = value;
        public Union(T2 value) => this.value = value;
        public Union(T3 value) => this.value = value;
        public Union(T4 value) => this.value = value;
        public Union(T5 value) => this.value = value;
        public Union(T6 value) => this.value = value;

        public void Match(
            Action<T1> case1, Action<T2> case2, Action<T3> case3, Action<T4> case4,
            Action<T5> case5, Action<T6> case6)
        {
            switch (value)
            {
                case T1 v1: case1(v1); break;
                case T2 v2: case2(v2); break;
                case T3 v3: case3(v3); break;
                case T4 v4: case4(v4); break;
                case T5 v5: case5(v5); break;
                case T6 v6: case6(v6); break;
            }
        }


        public TResult Match<TResult>(
            Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3, Func<T4, TResult> case4,
            Func<T5, TResult> case5, Func<T6, TResult> case6)
            => value switch
            {
                T1 v1 => case1(v1),
                T2 v2 => case2(v2),
                T3 v3 => case3(v3),
                T4 v4 => case4(v4),
                T5 v5 => case5(v5),
                T6 v6 => case6(v6),
                _ => throw new InvalidOperationException($"Unexpected type in union: {value.TypeName()}")
            };

        public static explicit operator T1(Union<T1, T2, T3, T4, T5, T6> union) => (T1)union.value;
        public static explicit operator T2(Union<T1, T2, T3, T4, T5, T6> union) => (T2)union.value;
        public static explicit operator T3(Union<T1, T2, T3, T4, T5, T6> union) => (T3)union.value;
        public static explicit operator T4(Union<T1, T2, T3, T4, T5, T6> union) => (T4)union.value;
        public static explicit operator T5(Union<T1, T2, T3, T4, T5, T6> union) => (T5)union.value;
        public static explicit operator T6(Union<T1, T2, T3, T4, T5, T6> union) => (T6)union.value;

        public override string? ToString() => value.ToString();
    }

    public sealed class Union<T1, T2, T3, T4, T5, T6, T7>
        where T1 : notnull
        where T2 : notnull
        where T3 : notnull
        where T4 : notnull
        where T5 : notnull
        where T6 : notnull
        where T7 : notnull
    {
        private readonly object value;

        public Union(T1 value) => this.value = value;
        public Union(T2 value) => this.value = value;
        public Union(T3 value) => this.value = value;
        public Union(T4 value) => this.value = value;
        public Union(T5 value) => this.value = value;
        public Union(T6 value) => this.value = value;
        public Union(T7 value) => this.value = value;

        public void Match(
            Action<T1> case1, Action<T2> case2, Action<T3> case3, Action<T4> case4,
            Action<T5> case5, Action<T6> case6, Action<T7> case7)
        {
            switch (value)
            {
                case T1 v1: case1(v1); break;
                case T2 v2: case2(v2); break;
                case T3 v3: case3(v3); break;
                case T4 v4: case4(v4); break;
                case T5 v5: case5(v5); break;
                case T6 v6: case6(v6); break;
                case T7 v7: case7(v7); break;
            }
        }


        public TResult Match<TResult>(
            Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3, Func<T4, TResult> case4,
            Func<T5, TResult> case5, Func<T6, TResult> case6, Func<T7, TResult> case7)
            => value switch
            {
                T1 v1 => case1(v1),
                T2 v2 => case2(v2),
                T3 v3 => case3(v3),
                T4 v4 => case4(v4),
                T5 v5 => case5(v5),
                T6 v6 => case6(v6),
                T7 v7 => case7(v7),
                _ => throw new InvalidOperationException($"Unexpected type in union: {value.TypeName()}")
            };

        public static explicit operator T1(Union<T1, T2, T3, T4, T5, T6, T7> union) => (T1)union.value;
        public static explicit operator T2(Union<T1, T2, T3, T4, T5, T6, T7> union) => (T2)union.value;
        public static explicit operator T3(Union<T1, T2, T3, T4, T5, T6, T7> union) => (T3)union.value;
        public static explicit operator T4(Union<T1, T2, T3, T4, T5, T6, T7> union) => (T4)union.value;
        public static explicit operator T5(Union<T1, T2, T3, T4, T5, T6, T7> union) => (T5)union.value;
        public static explicit operator T6(Union<T1, T2, T3, T4, T5, T6, T7> union) => (T6)union.value;
        public static explicit operator T7(Union<T1, T2, T3, T4, T5, T6, T7> union) => (T7)union.value;

        public override string? ToString() => value.ToString();
    }

    public sealed class Union<T1, T2, T3, T4, T5, T6, T7, T8>
        where T1 : notnull
        where T2 : notnull
        where T3 : notnull
        where T4 : notnull
        where T5 : notnull
        where T6 : notnull
        where T7 : notnull
        where T8 : notnull
    {
        private readonly object value;

        public Union(T1 value) => this.value = value;
        public Union(T2 value) => this.value = value;
        public Union(T3 value) => this.value = value;
        public Union(T4 value) => this.value = value;
        public Union(T5 value) => this.value = value;
        public Union(T6 value) => this.value = value;
        public Union(T7 value) => this.value = value;
        public Union(T8 value) => this.value = value;

        public void Match(
            Action<T1> case1, Action<T2> case2, Action<T3> case3, Action<T4> case4,
            Action<T5> case5, Action<T6> case6, Action<T7> case7, Action<T8> case8)
        {
            switch (value)
            {
                case T1 v1: case1(v1); break;
                case T2 v2: case2(v2); break;
                case T3 v3: case3(v3); break;
                case T4 v4: case4(v4); break;
                case T5 v5: case5(v5); break;
                case T6 v6: case6(v6); break;
                case T7 v7: case7(v7); break;
                case T8 v8: case8(v8); break;
            }
        }

        public TResult Match<TResult>(
            Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3, Func<T4, TResult> case4,
            Func<T5, TResult> case5, Func<T6, TResult> case6, Func<T7, TResult> case7, Func<T8, TResult> case8)
            => value switch
        {
            T1 v1 => case1(v1),
            T2 v2 => case2(v2),
            T3 v3 => case3(v3),
            T4 v4 => case4(v4),
            T5 v5 => case5(v5),
            T6 v6 => case6(v6),
            T7 v7 => case7(v7),
            T8 v8 => case8(v8),
            _ => throw new InvalidOperationException($"Unexpected type in union: {value.TypeName()}")
        };

        public static explicit operator T1(Union<T1, T2, T3, T4, T5, T6, T7, T8> union) => (T1)union.value;
        public static explicit operator T2(Union<T1, T2, T3, T4, T5, T6, T7, T8> union) => (T2)union.value;
        public static explicit operator T3(Union<T1, T2, T3, T4, T5, T6, T7, T8> union) => (T3)union.value;
        public static explicit operator T4(Union<T1, T2, T3, T4, T5, T6, T7, T8> union) => (T4)union.value;
        public static explicit operator T5(Union<T1, T2, T3, T4, T5, T6, T7, T8> union) => (T5)union.value;
        public static explicit operator T6(Union<T1, T2, T3, T4, T5, T6, T7, T8> union) => (T6)union.value;
        public static explicit operator T7(Union<T1, T2, T3, T4, T5, T6, T7, T8> union) => (T7)union.value;
        public static explicit operator T8(Union<T1, T2, T3, T4, T5, T6, T7, T8> union) => (T8)union.value;

        public override string? ToString() => value.ToString();
    }
}