﻿using Microsoft.Extensions.Configuration;

namespace PewPew.Configuration
{
    public class ConfigurationProvider<TConfig> : IConfigurationProvider<TConfig> where TConfig : new()
    {
        public TConfig Configuration { get; }

        public ConfigurationProvider(IConfiguration config, string section) => config
            .GetSection(section)
            .Bind(Configuration = new());
    }
}