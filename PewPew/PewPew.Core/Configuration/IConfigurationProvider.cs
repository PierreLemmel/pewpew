﻿namespace PewPew.Configuration
{
    public interface IConfigurationProvider<TConfig> where TConfig : new()
    {
        TConfig Configuration { get; }
    }
}