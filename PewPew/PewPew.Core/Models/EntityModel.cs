﻿using System;

namespace PewPew
{
    public abstract class EntityModel
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }

        protected EntityModel(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}