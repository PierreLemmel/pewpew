﻿using PewPew.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PewPew.Scanning
{
    public static class Warmup
    {
        private static bool warmedUp = false;

        public static void PreloadPewPewAssemblies()
        {
            if (warmedUp) return;

            string currentDirectory = Directory.GetCurrentDirectory();
            IEnumerable<string> pewPewAssemblies = Directory
                .EnumerateFiles(currentDirectory)
                .Where(file => file.GetExtension().IsOneOf(".dll", ".exe"))
                .Select(path => path.GetFileNameWithoutExtension())
                .Where(file => file.Contains("PewPew"))
                .Where(file => !file.Contains("Tests"));

            foreach (string assembly in pewPewAssemblies)
                AppDomain.CurrentDomain.Load(assembly);

            warmedUp = true;
        }
    }
}