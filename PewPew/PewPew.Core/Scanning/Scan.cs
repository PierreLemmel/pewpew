﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PewPew.Scanning
{
    public static class Scan
    {
        static Scan() => Warmup.PreloadPewPewAssemblies();

        public static IEnumerable<Assembly> PewPewAssemblies => AppDomain
            .CurrentDomain
            .GetAssemblies()
            .Where(assembly => assembly.FullName!.Contains("PewPew")
                && !assembly.FullName.Contains(".Tests"));

        public static IEnumerable<Assembly> PewPewUIAssemblies => PewPewAssemblies
            .Where(assembly => assembly.FullName!.Contains(".UI"));

        public static IEnumerable<Type> PewPewTypes => PewPewAssemblies
            .SelectMany(assembly => assembly.GetTypes());

        public static IEnumerable<Type> PewPewUITypes => PewPewUIAssemblies
            .SelectMany(assembly => assembly.GetTypes());

        public static IEnumerable<Type> PewPewClasses => PewPewTypes.Classes();

        public static IEnumerable<Type> PewPewUIClasses => GetPewPewUIClasses(includeAbstract: false);
        public static IEnumerable<Type> GetPewPewUIClasses(bool includeAbstract = false)
        {
            IEnumerable<Type> result = PewPewUITypes.Classes();

            if (!includeAbstract)
                result = result.ConcreteTypes();

            return result;
        }
    }
}