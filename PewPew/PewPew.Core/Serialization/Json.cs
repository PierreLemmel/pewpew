﻿using Newtonsoft.Json;

namespace PewPew
{
    public static class Json
    {
        public static string Serialize<T>(T value) => JsonConvert.SerializeObject(value);
        public static string Serialize<T>(T value, JsonSerializerSettings settings) => JsonConvert.SerializeObject(value, settings);
        public static T Deserialize<T>(string json) => JsonConvert.DeserializeObject<T>(json);
    }
}