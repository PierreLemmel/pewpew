using System;
using System.Globalization;

namespace PewPew
{
    public struct Percentage
    {
        public float Value { get; }

        public Percentage(float value) => Value = value;

        public Percentage Clamped0_100() => new(Value.Clamped0_100());

        public static Percentage Zero => new(0.0f);
        public static Percentage OneHundred => new(100.0f);

        public static bool operator ==(Percentage lhs, Percentage rhs) => lhs.Value == rhs.Value;
        public static bool operator !=(Percentage lhs, Percentage rhs) => lhs.Value != rhs.Value;

        public static implicit operator float(Percentage per) => per.Value / 100.0f;
        public static explicit operator Percentage(float val) => new(100.0f * val);

        public static Percentage operator +(Percentage lhs, Percentage rhs) => new(lhs.Value + rhs.Value);
        public static Percentage operator -(Percentage lhs, Percentage rhs) => new(lhs.Value - rhs.Value);

        public static Percentage operator +(Percentage per) => per;
        public static Percentage operator -(Percentage per) => new(-per.Value);

        public override bool Equals(object? obj) => (obj is Percentage per) ? per == this : false;
        public override int GetHashCode() => Value.GetHashCode();
        public override string ToString() => $"{Value:F}%";
    }
}