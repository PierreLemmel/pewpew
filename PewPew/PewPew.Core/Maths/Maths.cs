using System;

namespace PewPew
{
    public static class Maths
    {
        public static bool IsBetween(this int value, int min, int max, bool includeBounds = true) => includeBounds ?
            value.IsBetweenInclusive(min, max) :
            value.IsBetweenExclusive(min, max);
        public static bool IsBetweenInclusive(this int value, int min, int max) => value >= min && value <= max;
        public static bool IsBetweenExclusive(this int value, int min, int max) => value > min && value < max;

        public static float Clamped(this float value, float min, float max) => min <= max ? (value >= min ? (value <= max ? value : max) : min) : throw new ArgumentException($"Expected: min <= max, found: {min} > {max}");
        public static float Clamped01(this float value) => value.Clamped(0.0f, 1.0f);
        public static float Clamped0_100(this float value) => value.Clamped(0.0f, 100.0f);
    }
}