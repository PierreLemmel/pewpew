using System;

namespace PewPew
{
    public static class Rng
    {
        private static readonly Random rng = new();

        public static int Next() => rng.Next();

        /// <param name="max">Exclusive upper bound</param>
        public static int Next(int max) => rng.Next(max);

        /// <param name="min">Inclusive lower bound</param>
        /// <param name="max">Exclusive upper bound</param>
        public static int Next(int min, int max) => rng.Next(min, max);
    }
}