﻿namespace PewPew.Lifecycle
{
    public enum SourceState
    {
        Uninitialized = 0,

        Initialized,
        Started,
        Stopped,
        Terminated,

        Error
    }
}