﻿namespace PewPew.Lifecycle
{
    public interface IInitializable
    {
        void Initialize();
    }

    public interface IInitializable<TSettings>
    {
        void Initialize(TSettings settings);
    }
}