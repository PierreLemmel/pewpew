﻿namespace PewPew.Lifecycle
{
    public interface ITerminable
    {
        void Terminate();
    }
}