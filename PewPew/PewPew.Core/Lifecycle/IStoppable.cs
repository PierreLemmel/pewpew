﻿namespace PewPew.Lifecycle
{
    public interface IStoppable
    {
        void Stop();
    }
}