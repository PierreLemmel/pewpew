﻿namespace PewPew.Lifecycle
{
    public interface IStartable
    {
        void Start();
    }

    public interface IStartable<TSettings>
    {
        void Start(TSettings settings);
    }
}