﻿namespace PewPew.Lifecycle
{
    public enum StartStopState
    {
        Stopped = 0,
        Started = 1,
    }
}