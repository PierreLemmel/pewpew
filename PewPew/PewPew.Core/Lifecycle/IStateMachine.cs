﻿namespace PewPew.Lifecycle
{
    public interface IStateMachine<TState>
    {
        TState State { get; }
    }
}