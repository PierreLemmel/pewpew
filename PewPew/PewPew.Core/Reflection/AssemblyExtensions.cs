﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PewPew
{
    public static class AssemblyExtensions
    {
        public static IEnumerable<Type> GetImplementationsOf<TInterface>(this Assembly assembly) => assembly
            .GetTypes()
            .Where(type => !type.IsAbstract && type.Implements<TInterface>());

        public static IEnumerable<Type> GetSubclassesOf<TClass>(this Assembly assembly) => assembly
            .GetTypes()
            .Where(type => type.InheritsFrom<TClass>());
    }
}