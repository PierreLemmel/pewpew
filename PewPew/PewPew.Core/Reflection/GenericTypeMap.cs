using System;
using System.Collections.Generic;

namespace PewPew.Reflection
{
    public record GenericTypeMap(Type GenericDefinition, IEnumerable<Type> Types) { }
}