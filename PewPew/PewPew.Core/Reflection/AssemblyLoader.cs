﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace PewPew.Reflection
{
    public class AssemblyLoader : IAssemblyLoader
    {
        private readonly ISet<string> loadedAssemblies = new HashSet<string>();

        public void LoadAssemblyWithAllDependencies(string assemblyPath)
        {
            CacheAlreadyLoadedAssemblies();

            string absolutePath = Path.GetFullPath(assemblyPath);
            Assembly entryPoint = Assembly.LoadFile(absolutePath);

            LoadDependencies(entryPoint);
        }

        private void CacheAlreadyLoadedAssemblies()
        {
            loadedAssemblies.Clear();

            IEnumerable<string> loaded = AppDomain
                .CurrentDomain
                .GetAssemblies()
                .Select(asm => asm.FullName!);

            loadedAssemblies.AddRange(loaded);
        }

        private void LoadDependencies(Assembly assembly)
        {
            IEnumerable<AssemblyName> references = assembly.GetReferencedAssemblies();

            foreach (AssemblyName reference in references)
            {
                string fullname = reference.FullName;
                if (!loadedAssemblies.Add(fullname))
                    continue;

                Log.Debug($"Loading assembly '{fullname}'");
                try
                {
                    Assembly loaded = Assembly.Load(reference);
                    Log.Debug($"Assembly '{fullname}' loaded");
                    LoadDependencies(loaded);
                }
                catch (Exception ex)
                {
                    Log.Warn($"Error while loading assembly '{fullname}'");
                    Log.Warn(ex.Message);
                }
            }
        }
    }
}