﻿namespace PewPew
{
    public enum BinaryOperator
    {
        Addition,
        Subtraction,
        Multiply,
        Division,

        BitwiseAnd,
        BitwiseOr,
        ExclusiveOr,

        Equality,
        Inequality,

        LessThan,
        GreaterThan,
        LessThanOrEqual,
        GreaterThanOrEqual,

        LeftShift,
        RightShift,
        
        Modulus,
    }
}