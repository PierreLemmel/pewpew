﻿namespace PewPew
{
    public enum UnaryOperator
    {
        UnaryPlus,
        UnaryNegation,

        Increment,
        Decrement,

        LogicalNot,

        OnesComplement,

        True,
        False
    }
}
