﻿namespace PewPew.Reflection
{
    public interface IAssemblyLoader
    {
        void LoadAssemblyWithAllDependencies(string assemblyPath);
    }
}