using Autofac;
using PewPew.Lifecycle;
using System.Collections.Generic;

namespace PewPew.DI
{
    public static class ContainerExtensions
    {
        public static IEnumerable<IStaticComponent> LoadStaticComponents(this IContainer container) => container
            .Resolve<IEnumerable<IStaticComponent>>();
    }
}