﻿using Autofac;
using Autofac.Builder;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using PewPew.Configuration;
using PewPew.Lifecycle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PewPew
{
    public static class ContainerBuilderExtensions
    {
        public static IRegistrationBuilder<ConfigurationProvider<TOptions>, SimpleActivatorData, SingleRegistrationStyle> RegisterConfigProvider<TOptions>(
            this ContainerBuilder builder, string section)
            where TOptions : new()
            => builder.Register(c => new ConfigurationProvider<TOptions>(c.Resolve<IConfiguration>(), section)).As<IConfigurationProvider<TOptions>>();

        public static void RegisterSubtypesOf<TBaseClass>(this ContainerBuilder builder, Assembly inAssembly) => inAssembly
            .GetTypes()
            .InheritingFrom<TBaseClass>()
            .ConcreteTypes()
            .ForEach(builder.RegisterType);

        public static void RegisterSubtypesOf<TBaseClass>(this ContainerBuilder builder, IEnumerable<Assembly> assemblies) => assemblies
            .ForEach(assembly => builder.RegisterSubtypesOf<TBaseClass>(assembly));

        public static void RegisterSubtypesOf<TBaseClass>(this ContainerBuilder builder, params Assembly[] assemblies) => builder
            .RegisterSubtypesOf<TBaseClass>(assemblies.AsEnumerable());

        public static void RegisterImplementationsAsInterface<TInterface>(this ContainerBuilder builder, Assembly inAssembly) => inAssembly
            .GetTypes()
            .Implementing<TInterface>()
            .ConcreteTypes()
            .ExcludeGenericTypeDefinitions()
            .ForEach(t => builder.RegisterType(t).As(typeof(TInterface)));

        public static void RegisterImplementationsAsInterface<TInterface>(this ContainerBuilder builder, IEnumerable<Assembly> assemblies) => assemblies
            .ForEach(assembly => builder.RegisterImplementationsAsInterface<TInterface>(assembly));

        public static void RegisterImplementationsAsInterface<TInterface>(this ContainerBuilder builder, params Assembly[] assemblies) => builder
            .RegisterImplementationsAsInterface<TInterface>(assemblies.AsEnumerable());

        public static void RegisterImplementationsAsSelf<TInterface>(this ContainerBuilder builder, Assembly inAssembly) => inAssembly
            .GetTypes()
            .Implementing<TInterface>()
            .ConcreteTypes()
            .ForEach(builder.RegisterType);

        public static void RegisterImplementationsAsSelf<TInterface>(this ContainerBuilder builder, IEnumerable<Assembly> assemblies) => assemblies
            .ForEach(assembly => builder.RegisterImplementationsAsSelf<TInterface>(assembly));

        public static void RegisterImplementationsAsSelf<TInterface>(this ContainerBuilder builder, params Assembly[] assemblies) => builder
            .RegisterImplementationsAsSelf<TInterface>(assemblies.AsEnumerable());

        public static IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> As<TLimit, TActivatorData, TRegistrationStyle>(
            this IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> registration, IEnumerable<Type> services)
            => registration.As(services.ToArray());

        public static IRegistrationBuilder<TService, ConcreteReflectionActivatorData, SingleRegistrationStyle> AddStaticComponent<TService>(this ContainerBuilder builder) where TService : IStaticComponent
            => builder.RegisterType<TService>().As<IStaticComponent>().SingleInstance();
        }
}