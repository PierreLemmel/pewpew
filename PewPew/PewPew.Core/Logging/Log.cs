﻿using NLog;
using System;

namespace PewPew
{
    public static class Log
    {
        private static ILogger defaultLogger;
        static Log()
        {
            LogManager.ReconfigExistingLoggers();
            defaultLogger = LogManager.GetCurrentClassLogger();
        }
        public static void Trace(string message) => defaultLogger.Trace(message);
        public static void Trace(object obj) => Trace(obj.ToString()!);
        public static void Debug(string message) => defaultLogger.Debug(message);
        public static void Debug(object obj) => Debug(obj.ToString()!);
        public static void Info(string message) => defaultLogger.Info(message);
        public static void Info(object obj) => Info(obj.ToString()!);
        public static void Warn(string message) => defaultLogger.Warn(message);
        public static void Warn(object obj) => Warn(obj.ToString()!);
        public static void Error(string message) => defaultLogger.Error(message);
        public static void Error(object obj) => Error(obj.ToString()!);
        public static void Error(Exception exception) => defaultLogger.Error(exception);
        public static void Fatal(string message) => defaultLogger.Fatal(message);
        public static void Fatal(object obj) => Fatal(obj.ToString()!);
        public static void Fatal(Exception exception) => defaultLogger.Fatal(exception);
    }
}