﻿using System;

namespace PewPew
{
    public static class Convert
    {
        public static Guid ToGuid(this string input) => Guid.Parse(input);
    }
}