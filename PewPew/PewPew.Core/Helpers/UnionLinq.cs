using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PewPew
{
    public static class UnionLinq
    {
        public static IEnumerable<Union<T1, T2, T3, T4, T5, T6, T7, T8>> OfTypes<T1, T2, T3, T4, T5, T6, T7, T8>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            where T8 : notnull
            => sequence
            .Select(obj => obj.AsUnionOrDefault<T1, T2, T3, T4, T5, T6, T7, T8>())
            .NotNull();

        public static IEnumerable<Union<T1, T2, T3, T4, T5, T6, T7>> OfTypes<T1, T2, T3, T4, T5, T6, T7>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            => sequence
            .Select(obj => obj.AsUnionOrDefault<T1, T2, T3, T4, T5, T6, T7>())
            .NotNull();

        public static IEnumerable<Union<T1, T2, T3, T4, T5, T6>> OfTypes<T1, T2, T3, T4, T5, T6>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            => sequence
            .Select(obj => obj.AsUnionOrDefault<T1, T2, T3, T4, T5, T6>())
            .NotNull();

        public static IEnumerable<Union<T1, T2, T3, T4, T5>> OfTypes<T1, T2, T3, T4, T5>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            => sequence
            .Select(obj => obj.AsUnionOrDefault<T1, T2, T3, T4, T5>())
            .NotNull();

        public static IEnumerable<Union<T1, T2, T3, T4>> OfTypes<T1, T2, T3, T4>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            => sequence
            .Select(obj => obj.AsUnionOrDefault<T1, T2, T3, T4>())
            .NotNull();

        public static IEnumerable<Union<T1, T2, T3>> OfTypes<T1, T2, T3>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            => sequence
            .Select(obj => obj.AsUnionOrDefault<T1, T2, T3>())
            .NotNull();

        public static IEnumerable<Union<T1, T2>> OfTypes<T1, T2>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            => sequence
            .Select(obj => obj.AsUnionOrDefault<T1, T2>())
            .NotNull();


        public static Union<T1, T2, T3, T4, T5, T6, T7, T8> FirstOfTypes<T1, T2, T3, T4, T5, T6, T7, T8>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            where T8 : notnull
            => sequence.OfTypes<T1, T2, T3, T4, T5, T6, T7, T8>().First();

        public static Union<T1, T2, T3, T4, T5, T6, T7> FirstOfTypes<T1, T2, T3, T4, T5, T6, T7>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            => sequence.OfTypes<T1, T2, T3, T4, T5, T6, T7>().First();

        public static Union<T1, T2, T3, T4, T5, T6> FirstOfTypes<T1, T2, T3, T4, T5, T6>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            => sequence.OfTypes<T1, T2, T3, T4, T5, T6>().First();

        public static Union<T1, T2, T3, T4, T5> FirstOfTypes<T1, T2, T3, T4, T5>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            => sequence.OfTypes<T1, T2, T3, T4, T5>().First();

        public static Union<T1, T2, T3, T4> FirstOfTypes<T1, T2, T3, T4>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            => sequence.OfTypes<T1, T2, T3, T4>().First();

        public static Union<T1, T2, T3> FirstOfTypes<T1, T2, T3>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            => sequence.OfTypes<T1, T2, T3>().First();

        public static Union<T1, T2> FirstOfTypes<T1, T2>(this IEnumerable<object> sequence)
            where T1 : notnull
            where T2 : notnull
            => sequence.OfTypes<T1, T2>().First();


        public static void Match<T1, T2, T3, T4, T5, T6, T7, T8>(
            this IEnumerable<Union<T1, T2, T3, T4, T5, T6, T7, T8>> sequence,
            Action<T1> case1, Action<T2> case2, Action<T3> case3, Action<T4> case4,
            Action<T5> case5, Action<T6> case6, Action<T7> case7, Action<T8> case8)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            where T8 : notnull
            => sequence.ForEach(union => union.Match(case1, case2, case3, case4, case5, case6, case7, case8));

        public static void Match<T1, T2, T3, T4, T5, T6, T7>(
            this IEnumerable<Union<T1, T2, T3, T4, T5, T6, T7>> sequence,
            Action<T1> case1, Action<T2> case2, Action<T3> case3, Action<T4> case4,
            Action<T5> case5, Action<T6> case6, Action<T7> case7)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            => sequence.ForEach(union => union.Match(case1, case2, case3, case4, case5, case6, case7));

        public static void Match<T1, T2, T3, T4, T5, T6>(
            this IEnumerable<Union<T1, T2, T3, T4, T5, T6>> sequence, Action<T1> case1, Action<T2> case2,
            Action<T3> case3, Action<T4> case4, Action<T5> case5, Action<T6> case6)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            => sequence.ForEach(union => union.Match(case1, case2, case3, case4, case5, case6));

        public static void Match<T1, T2, T3, T4, T5>(
            this IEnumerable<Union<T1, T2, T3, T4, T5>> sequence, Action<T1> case1, Action<T2> case2,
            Action<T3> case3, Action<T4> case4, Action<T5> case5)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            => sequence.ForEach(union => union.Match(case1, case2, case3, case4, case5));

        public static void Match<T1, T2, T3, T4>(
            this IEnumerable<Union<T1, T2, T3, T4>> sequence, Action<T1> case1, Action<T2> case2,
            Action<T3> case3, Action<T4> case4)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            => sequence.ForEach(union => union.Match(case1, case2, case3, case4));

        public static void Match<T1, T2, T3>(this IEnumerable<Union<T1, T2, T3>> sequence, Action<T1> case1, Action<T2> case2, Action<T3> case3)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            => sequence.ForEach(union => union.Match(case1, case2, case3));

        public static void Match<T1, T2>(this IEnumerable<Union<T1, T2>> sequence, Action<T1> case1, Action<T2> case2)
            where T1 : notnull
            where T2 : notnull
            => sequence.ForEach(union => union.Match(case1, case2));


        public static IEnumerable<TResult> Match<T1, T2, T3, T4, T5, T6, T7, T8, TResult>(
           this IEnumerable<Union<T1, T2, T3, T4, T5, T6, T7, T8>> sequence,
           Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3, Func<T4, TResult> case4,
           Func<T5, TResult> case5, Func<T6, TResult> case6, Func<T7, TResult> case7, Func<T8, TResult> case8)
           where T1 : notnull
           where T2 : notnull
           where T3 : notnull
           where T4 : notnull
           where T5 : notnull
           where T6 : notnull
           where T7 : notnull
           where T8 : notnull
           => sequence.Select(union => union.Match(case1, case2, case3, case4, case5, case6, case7, case8));

        public static IEnumerable<TResult> Match<T1, T2, T3, T4, T5, T6, T7, TResult>(
            this IEnumerable<Union<T1, T2, T3, T4, T5, T6, T7>> sequence,
            Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3, Func<T4, TResult> case4,
            Func<T5, TResult> case5, Func<T6, TResult> case6, Func<T7, TResult> case7)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            where T7 : notnull
            => sequence.Select(union => union.Match(case1, case2, case3, case4, case5, case6, case7));

        public static IEnumerable<TResult> Match<T1, T2, T3, T4, T5, T6, TResult>(
            this IEnumerable<Union<T1, T2, T3, T4, T5, T6>> sequence, Func<T1, TResult> case1, Func<T2, TResult> case2,
            Func<T3, TResult> case3, Func<T4, TResult> case4, Func<T5, TResult> case5, Func<T6, TResult> case6)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            where T6 : notnull
            => sequence.Select(union => union.Match(case1, case2, case3, case4, case5, case6));

        public static IEnumerable<TResult> Match<T1, T2, T3, T4, T5, TResult>(
            this IEnumerable<Union<T1, T2, T3, T4, T5>> sequence, Func<T1, TResult> case1, Func<T2, TResult> case2,
            Func<T3, TResult> case3, Func<T4, TResult> case4, Func<T5, TResult> case5)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            where T5 : notnull
            => sequence.Select(union => union.Match(case1, case2, case3, case4, case5));

        public static IEnumerable<TResult> Match<T1, T2, T3, T4, TResult>(
            this IEnumerable<Union<T1, T2, T3, T4>> sequence, Func<T1, TResult> case1, Func<T2, TResult> case2,
            Func<T3, TResult> case3, Func<T4, TResult> case4)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            where T4 : notnull
            => sequence.Select(union => union.Match(case1, case2, case3, case4));

        public static IEnumerable<TResult> Match<T1, T2, T3, TResult>(this IEnumerable<Union<T1, T2, T3>> sequence, Func<T1, TResult> case1, Func<T2, TResult> case2, Func<T3, TResult> case3)
            where T1 : notnull
            where T2 : notnull
            where T3 : notnull
            => sequence.Select(union => union.Match(case1, case2, case3));

        public static IEnumerable<TResult> Match<T1, T2, TResult>(this IEnumerable<Union<T1, T2>> sequence, Func<T1, TResult> case1, Func<T2, TResult> case2)
            where T1 : notnull
            where T2 : notnull
            => sequence.Select(union => union.Match(case1, case2));
    }
}