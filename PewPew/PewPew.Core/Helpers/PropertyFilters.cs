using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PewPew
{
    public static class PropertyFilters
    {
        public static IEnumerable<PropertyInfo> GetSetProperties(this IEnumerable<PropertyInfo> properties)
            => properties.Where(prop => prop.HasGetter() && prop.HasSetter());
        public static IEnumerable<PropertyInfo> NotImplementing<TInterface>(this IEnumerable<PropertyInfo> properties)
            => properties.Where(prop => !prop.PropertyType.Implements<TInterface>());
        public static IEnumerable<PropertyInfo> NotOfType<TType>(this IEnumerable<PropertyInfo> properties)
            => properties.Where(prop => prop.PropertyType != typeof(TType));
    }
}