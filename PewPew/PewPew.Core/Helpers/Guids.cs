﻿using System;

namespace PewPew
{
    public static class Guids
    {
        public static bool IsEmpty(this Guid guid) => guid == Guid.Empty;
    }
}