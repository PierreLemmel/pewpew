using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew
{
    public static class Tuples
    {
        public static (T1[], T2[]) Deconstruct<T1, T2>(this (T1, T2)[] tupleArray)
        {
            T1[] array1 = tupleArray.Select(((T1 t1, T2 t2) t) => t.t1);
            T2[] array2 = tupleArray.Select(((T1 t1, T2 t2) t) => t.t2);

            return (array1, array2);
        }
        
    }
}