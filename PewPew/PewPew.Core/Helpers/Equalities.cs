﻿using System;

namespace PewPew
{
    public static class Equalities
    {
		public static bool IsOneOf(this string input, params string[] others)
		{
			foreach (string other in others)
			{
				if (input == other) return true;
			}
			return false;
		}

		public static bool IsOneOf(this int input, params int[] others)
		{
			foreach (int other in others)
			{
				if (input == other) return true;
			}
			return false;
		}

		public static bool IsOneOf(this uint input, params uint[] others)
		{
			foreach (uint other in others)
			{
				if (input == other) return true;
			}
			return false;
		}

		public static bool IsOneOf(this long input, params long[] others)
		{
			foreach (long other in others)
			{
				if (input == other) return true;
			}
			return false;
		}

		public static bool IsOneOf(this ulong input, params ulong[] others)
		{
			foreach (ulong other in others)
			{
				if (input == other) return true;
			}
			return false;
		}

		public static bool IsOneOf(this float input, params float[] others)
		{
			foreach (float other in others)
			{
				if (input == other) return true;
			}
			return false;
		}

		public static bool IsOneOf(this double input, params double[] others)
		{
			foreach (double other in others)
			{
				if (input == other) return true;
			}
			return false;
		}

		public static bool IsOneOf<TEnum>(this TEnum input, params TEnum[] others) where TEnum : Enum
		{
			foreach (TEnum other in others)
			{
				if (input.Equals(other)) return true;
			}
			return false;
		}
	}
}