using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew
{
    public static class Stacks
    {
        public static Stack<T> Create<T>(params T[] elts) => new(elts);
    }
}