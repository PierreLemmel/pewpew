﻿using System.IO;

namespace PewPew.IO
{
    public static class IOExtensions
    {
        /// <summary>
        /// Gets extension (including the dot)
        /// </summary>
        public static string GetExtension(this string path) => Path.GetExtension(path);

        public static string GetFileName(this string path) => Path.GetFileName(path);
        public static string GetDirectoryName(this string path) => Path.GetDirectoryName(path)!;
        public static string GetFileNameWithoutExtension(this string path) => Path.GetFileNameWithoutExtension(path);
        public static string GetFullPath(this string path) => Path.GetFullPath(path);
    }
}