﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew
{
    public static class Collections
    {
        public static void AddRange<T>(this ICollection<T> collection, params T[] elts) => collection.AddRange(elts.AsEnumerable());
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> elts)
        {
            foreach (T item in elts)
                collection.Add(item);
        }
        public static void AddRange<T>(this ICollection<T> collection, int count, Func<int, T> factoryMethod) => collection
            .AddRange(Enumerables.Create(count, factoryMethod));
        public static void AddRange<T>(this ICollection<T> collection, int count, Func<T> factoryMethod) => collection
            .AddRange(Enumerables.Create(count, factoryMethod));

        public static void ReplaceWith<T>(this ICollection<T> collection, params T[] elts) => collection.ReplaceWith(elts.AsEnumerable());
        public static void ReplaceWith<T>(this ICollection<T> collection, IEnumerable<T> elts)
        {
            collection.Clear();
            collection.AddRange(elts);
        }
    }
}