namespace PewPew.Helpers
{
    public static class OutVars
    {
        public static T OutVar<T>(this T input, out T asVar)
        {
            asVar = input;
            return input;
        }
    }
}