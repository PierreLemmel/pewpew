using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew
{
    public static class Booleans
    {
        public static bool Or(params bool[] input) => input.Any(b => b) || input.IsEmpty();
        public static bool And(params bool[] input) => input.All(b => b);
    }
}