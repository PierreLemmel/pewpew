using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace PewPew
{
    public static class Filters
    {
        public static T FindByType<T>(this IEnumerable<T> sequence, Type type) => sequence.First(IsOfType<T>(type));
        public static bool TryFindByType<T>(this IEnumerable<T> sequence, Type type, [NotNullWhen(true)] out T? result) => sequence.TryFind(IsOfType<T>(type), out result);

        private static Func<T, bool> IsOfType<T>(Type type) => elt => elt?.GetType() == type;
    }
}