﻿using System;
using System.Collections.Generic;

namespace PewPew
{
    public static class Dictionaries
    {
        public static TResult Get<TResult>(this IDictionary<string, object?> dic, string key) => (TResult)dic[key]!;

        public static TValue GetOrInitialize<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> initializer) where TKey : notnull
        {
            if (!dictionary.TryGetValue(key, out TValue value))
            {
                value = initializer(key);
                dictionary[key] = value;
            }

            return value;
        }

        public static TValue GetOrInitialize<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TValue> initializer) where TKey : notnull 
            => dictionary.GetOrInitialize(key, _ => initializer());
    }
}