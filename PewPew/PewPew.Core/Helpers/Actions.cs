using System;

namespace PewPew
{
    public class Actions
    {
        public static Action Empty() => () => { };
        public static Action<T> Empty<T>() => (t => { });
        public static Action<T1, T2> Empty<T1, T2>() => ((t1, t2) => { });
        public static Action<T1, T2, T3> Empty<T1, T2, T3>() => ((t1, t2, t3) => { });
        public static Action<T1, T2, T3, T4> Empty<T1, T2, T3, T4>() => ((t1, t2, t3, t4) => { });
        public static Action<T1, T2, T3, T4, T5> Empty<T1, T2, T3, T4, T5>() => ((t1, t2, t3, t4, t5) => { });
        public static Action<T1, T2, T3, T4, T5, T6> Empty<T1, T2, T3, T4, T5, T6>() => ((t1, t2, t3, t4, t5, t6) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7> Empty<T1, T2, T3, T4, T5, T6, T7>() => ((t1, t2, t3, t4, t5, t6, t7) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8> Empty<T1, T2, T3, T4, T5, T6, T7, T8>() => ((t1, t2, t3, t4, t5, t6, t7, t8) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> Empty<T1, T2, T3, T4, T5, T6, T7, T8, T9>() => ((t1, t2, t3, t4, t5, t6, t7, t8, t9) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Empty<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>() => ((t1, t2, t3, t4, t5, t6, t7, t8, t9, t10) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Empty<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>() => ((t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Empty<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>() => ((t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> Empty<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>() => ((t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> Empty<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>() => ((t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> Empty<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>() => ((t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15) => { });
        public static Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> Empty<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>() => ((t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16) => { });
    }
}