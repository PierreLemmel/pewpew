﻿using FluentValidation;
using FluentValidation.Results;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew
{
    public static class MoreValidation
    {
        public static string GetErrorMessages(this ValidationResult result) => string.Join(
            Environment.NewLine + "\t",
            result.Errors.Select(failure => $"{failure.PropertyName}: {failure.ErrorMessage}")
        );

        public static IRuleBuilderOptions<T, IEnumerable<TElt>> AreAllDistincts<T, TElt>(this IRuleBuilder<T, IEnumerable<TElt>> rule) => rule
            .Must(seq => seq.AreAllDistinct())
            .WithMessage("Elements in sequence must all be uniques");

        public static IRuleBuilderOptions<T, bool> True<T>(this IRuleBuilder<T, bool> rule) => rule
            .Must(b => b)
            .WithMessage("Value must be true");

        public static IRuleBuilderOptions<T, bool> False<T>(this IRuleBuilder<T, bool> rule) => rule
            .Must(b => !b)
            .WithMessage("Value must be false");
    }
}
