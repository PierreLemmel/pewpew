using System;

namespace PewPew
{
    public static class Objects
    {
        public static string TypeName(this object? obj) => obj?.GetType().Name ?? "NULL";
    }
}