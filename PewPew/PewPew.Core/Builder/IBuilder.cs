﻿using System;

namespace PewPew
{
    public interface IBuilder<T>
    {
        T Build();
    }
}