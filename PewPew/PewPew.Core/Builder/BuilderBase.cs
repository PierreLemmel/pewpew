namespace PewPew
{
    public abstract class BuilderBase<T> : IBuilder<T>
    {
        public abstract T Build();

        public static implicit operator T(BuilderBase<T> builder) => builder.Build();
    }
}