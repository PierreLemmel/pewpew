using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Layout
{
    public abstract class SplittedPanel : GridControl
    {
        public static readonly DependencyProperty SplitterSizeProperty;

        static SplittedPanel()
        {
            SplitterSizeProperty = DependencyProperty.Register(nameof(SplitterSize), typeof(float), typeof(SplittedPanel), new(5.0f, OnSplitterSizeChanged));
        }

        public float SplitterSize
        {
            get => (float)GetValue(SplitterSizeProperty);
            set => SetValue(SplitterSizeProperty, value);
        }

        protected virtual void OnSplitterSizeChanged() { }


        private static void OnSplitterSizeChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            SplittedPanel splitted = (SplittedPanel)sender;
            splitted.OnSplitterSizeChanged();
        }
    }
}