using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Layout
{
    public class HorizontallySplittedPanel : SplittedPanel
    {
        public static readonly DependencyProperty TopControlProperty;
        public static readonly DependencyProperty BottomControlProperty;

        static HorizontallySplittedPanel()
        {
            TopControlProperty = DependencyProperty.Register(nameof(TopControl), typeof(Control), typeof(HorizontallySplittedPanel), new(OnTopControlChanged));
            BottomControlProperty = DependencyProperty.Register(nameof(BottomControl), typeof(Control), typeof(HorizontallySplittedPanel), new(OnBottomControlChanged));
        }

        public Control TopControl
        {
            get => (Control)GetValue(TopControlProperty);
            set => SetValue(TopControlProperty, value);
        }

        public Control BottomControl
        {
            get => (Control)GetValue(BottomControlProperty);
            set => SetValue(BottomControlProperty, value);
        }

        protected override void OnSplitterSizeChanged()
        {
            GridSplitter splitter = mainGrid.Children.OfType<GridSplitter>().Single();
            splitter.Width = SplitterSize;
        }

        protected override void Setup()
        {
            mainGrid.SetupRows(
                Grids.Rows.Star(1.0),
                Grids.Rows.Pixels(5.0),
                Grids.Rows.Star(1.0)
            );

            mainGrid.AddControl(new GridSplitter()
            {
                Height = SplitterSize,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch,
            }, row: 1);
        }

        public static HorizontallySplittedPanel Create(Control top, Control bottom) => new() { TopControl = top, BottomControl = bottom };

        private void OnTopControlChanged() => mainGrid.SetControl(TopControl, row: 0);
        private void OnBottomControlChanged() => mainGrid.SetControl(BottomControl, row: 2);

        private static void OnTopControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            HorizontallySplittedPanel panel = (HorizontallySplittedPanel)d;
            panel.OnTopControlChanged();
        }

        private static void OnBottomControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            HorizontallySplittedPanel panel = (HorizontallySplittedPanel)d;
            panel.OnBottomControlChanged();
        }
    }
}