using System.Windows.Controls;

namespace PewPew.UI.Layout
{
    public abstract class GridControl : ContentControl
    {
        protected virtual void Setup() { }

        protected readonly Grid mainGrid;

        public GridControl()
        {
            mainGrid = new();
            Content = mainGrid;
            Setup();
        }
    }
}