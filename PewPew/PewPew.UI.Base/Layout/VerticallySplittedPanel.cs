using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Layout
{
    public class VerticallySplittedPanel : SplittedPanel
    {
        public static readonly DependencyProperty LeftControlProperty;
        public static readonly DependencyProperty RightControlProperty;

        static VerticallySplittedPanel()
        {
            LeftControlProperty = DependencyProperty.Register(nameof(LeftControl), typeof(Control), typeof(VerticallySplittedPanel), new(OnLeftControlChanged));
            RightControlProperty = DependencyProperty.Register(nameof(RightControl), typeof(Control), typeof(VerticallySplittedPanel), new(OnRightControlChanged));
        }

        public Control LeftControl
        {
            get => (Control)GetValue(LeftControlProperty);
            set => SetValue(LeftControlProperty, value);
        }

        public Control RightControl
        {
            get => (Control)GetValue(RightControlProperty);
            set => SetValue(RightControlProperty, value);
        }

        protected override void OnSplitterSizeChanged()
        {
            GridSplitter splitter = mainGrid.Children.OfType<GridSplitter>().Single();
            splitter.Width = SplitterSize;
        }

        protected override void Setup()
        {
            mainGrid.SetupColumns(
                Grids.Columns.Star(1.0),
                Grids.Columns.Pixels(5.0),
                Grids.Columns.Star(1.0)
            );

            mainGrid.AddControl(new GridSplitter()
            {
                Width = SplitterSize,
                HorizontalAlignment = HorizontalAlignment.Stretch,
            }, column: 1);
        }

        public static VerticallySplittedPanel Create(Control left, Control right) => new() { LeftControl = left, RightControl = right };

        private void OnLeftControlChanged() => mainGrid.SetControl(LeftControl, col: 0);
        private void OnRightControlChanged() => mainGrid.SetControl(RightControl, col: 2);

        private static void OnLeftControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            VerticallySplittedPanel panel = (VerticallySplittedPanel)d;
            panel.OnLeftControlChanged();
        }

        private static void OnRightControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            VerticallySplittedPanel panel = (VerticallySplittedPanel)d;
            panel.OnRightControlChanged();
        }
    }
}