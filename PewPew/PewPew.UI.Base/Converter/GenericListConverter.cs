using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace PewPew.UI.Converter
{
    public class GenericListConverter<T> : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IList ilist)
            {
                if (ilist is not IEnumerable<T> result)
                    result = ilist.Cast<T>();

                return result;
            }
            else
                throw new InvalidOperationException($"Can't convert {value?.ToString() ?? "null"} to List<{typeof(T).Name}>");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => ((IEnumerable<T>)value).ToList();
    }
}