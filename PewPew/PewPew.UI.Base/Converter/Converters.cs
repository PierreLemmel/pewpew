using System;
using System.Windows.Data;

namespace PewPew.UI.Converter
{
    public static class Converters
    {
        public static ILazyCache<Type, IValueConverter> GenericListConverters { get; } = new LazyCache<Type, IValueConverter>(CreateConverter);

        private static IValueConverter CreateConverter(Type itemType)
        {
            Type closed = typeof(GenericListConverter<>).MakeGenericType(itemType);
            object? instance = Activator.CreateInstance(closed);

            return (IValueConverter)instance!;
        }
    }
}