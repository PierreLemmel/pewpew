using System;
using System.Globalization;
using System.Windows.Data;

namespace PewPew.UI.Converter
{
    public class HasSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int intVal)
                return intVal >= 0;
            else
                throw new InvalidOperationException($"Can't convert value of type '{value?.GetType().Name ?? "null"}' to boolean. Expected: item selection.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool boolVal)
                return boolVal ? 0 : -1;
            else
                throw new InvalidOperationException($"Can't convert value of type '{value?.GetType().Name ?? "null"}' to selection. Expected: boolean.");
        }
    }
}