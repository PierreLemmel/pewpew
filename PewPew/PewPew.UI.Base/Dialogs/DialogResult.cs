using System;

namespace PewPew.UI
{
    public sealed class DialogResult : IEquatable<DialogResult>
    {
        private readonly DialogResultEnum value;
        private readonly string? Name = null;

        private DialogResult(DialogResultEnum value) => this.value = value;
        private DialogResult(DialogResultEnum value, string name) : this(value) => Name = name;

        public bool Equals(DialogResult? other) => other is not null && value == other.value;

        public bool IsDefined => value != DialogResultEnum.Undefined;

        public bool IsSuccess => value.IsOneOf(DialogResultEnum.Ok, DialogResultEnum.Yes);
        public bool IsCanceled => value == DialogResultEnum.Canceled;
        public bool IsDenied => value == DialogResultEnum.No;

        public bool IsFailure => IsCanceled || IsDenied;

        public static bool operator ==(DialogResult lhs, DialogResult rhs) => lhs.value == rhs.value;
        public static bool operator !=(DialogResult lhs, DialogResult rhs) => lhs.value != rhs.value;

        public static implicit operator bool(DialogResult result) => result.IsSuccess;

        public override bool Equals(object? obj) => Equals(obj as DialogResult);
        public override string ToString() => Name ?? value.ToString();
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 13;
                hash = hash * 7 + value.GetHashCode();

                return hash;
            }
        }

        public static DialogResult Undefined { get; } = new DialogResult(DialogResultEnum.Undefined);
        public static DialogResult Ok { get; } = new DialogResult(DialogResultEnum.Ok, nameof(Ok));
        public static DialogResult Yes { get; } = new DialogResult(DialogResultEnum.Yes, nameof(Yes));
        public static DialogResult No { get; } = new DialogResult(DialogResultEnum.No);
        public static DialogResult Canceled { get; } = new DialogResult(DialogResultEnum.Canceled);

        private enum DialogResultEnum
        {
            Undefined = 0,

            Ok = 1,

            Yes = 1,
            No = 2,

            Canceled = 3
        }
    }
}