namespace PewPew.UI
{
    public abstract class DialogViewModel<TThisViewModel> : ViewModel<TThisViewModel>, IDialogViewModel
        where TThisViewModel : DialogViewModel<TThisViewModel>
    {
        public DialogResult Result { get; set; } = DialogResult.Undefined;

        public Command<Dialog> OkCommand { get; }
        public Command<Dialog> YesCommand { get; }
        public Command<Dialog> NoCommand { get; }
        public Command<Dialog> CancelCommand { get; }

        protected DialogViewModel()
        {
            OkCommand = CreateResultCommand(DialogResult.Ok);
            YesCommand = CreateResultCommand(DialogResult.Yes);
            NoCommand = CreateResultCommand(DialogResult.No);
            CancelCommand = CreateResultCommand(DialogResult.Canceled);
        }

        private Command<Dialog> CreateResultCommand(DialogResult result) => Command.Create<Dialog>(dialog =>
        {
            Result = result;
            dialog.Close();
        });
    }
}