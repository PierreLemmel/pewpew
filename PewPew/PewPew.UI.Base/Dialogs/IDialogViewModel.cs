﻿namespace PewPew.UI
{
    public interface IDialogViewModel
    {
        DialogResult Result { get; set; }

        Command<Dialog> OkCommand { get; }
        Command<Dialog> YesCommand { get; }
        Command<Dialog> NoCommand { get; }
        Command<Dialog> CancelCommand { get; }
    }
}
