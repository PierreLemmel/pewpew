using System;
using System.ComponentModel;
using System.Windows;

namespace PewPew.UI
{
    public abstract class Dialog : Window
    {
        public IDialogViewModel ViewModel { get; }
        public new DialogResult DialogResult
        {
            get => ViewModel.Result;
            set => ViewModel.Result = value;
        }

        public Dialog(IDialogViewModel viewModel)
        {
            ViewModel = viewModel;
            DataContext = ViewModel;

            WindowStartupLocation = WindowStartupLocation.CenterOwner;
        }

        public override void EndInit()
        {
            if (Style.IsEmpty())
            {
                if (Resources[typeof(Dialog)] is Style dialogStyle)
                    Style = dialogStyle;
                else
                    throw new InvalidOperationException($"Missing DialogStyle for dialogs of type {GetType().Name}");
            }
            else
            {
                if (!Style.HasTargetTypeInHierarchy(typeof(Dialog)))
                    throw new InvalidOperationException($"Style for dialog of type '{GetType().Name}' is not based on dialog style");
            }

            Width = Resources.Get<double>("defaultDialogWidth");
            Height = Resources.Get<double>("defaultDialogHeight");

            base.EndInit();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!DialogResult.IsDefined)
                DialogResult = DialogResult.Canceled;
        }

        public new DialogResult ShowDialog() => ShowDialog(null, null, null);
        public DialogResult ShowDialog(Action? onSuccess) => ShowDialog(onSuccess, null);
        public DialogResult ShowDialog(Action? onSuccess, Action? onFailure) => ShowDialog(onSuccess, onFailure, onFailure);

        public DialogResult ShowDialog(Action? onSuccess, Action? onDenied, Action? onCanceled)
        {
            base.ShowDialog();

            DialogResult result = ViewModel.Result;

            if (!result.IsDefined) throw new InvalidOperationException("DialogResult has not been defined before exiting Dialog");
            else if (result.IsSuccess) onSuccess?.Invoke();
            else if (result.IsDenied) onDenied?.Invoke();
            else if (result.IsCanceled) onCanceled?.Invoke();

            return result;
        }
    }
}