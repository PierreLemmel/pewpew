using PewPew.UI.Events;
using System;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI
{
    public abstract class PewPewControl : UserControl
    {
        public IViewModel ViewModel { get; }
        private readonly IEventSender<ControlEvent> sender;

        public PewPewControl(IViewModel viewModel, IEventSender<ControlEvent> sender)
        {
            ViewModel = viewModel;
            DataContext = ViewModel;

            this.sender = sender;

            Unloaded += (_, _) => sender.SendEvent<ControlClosed>(new(GetType()));
        }

        protected override void OnInitialized(EventArgs e) => sender.SendEvent<ControlCreated>(new(GetType()));
        protected override void OnGotFocus(RoutedEventArgs e) => sender.SendEvent<ControlFocused>(new(GetType()));

        public override void EndInit()
        {
            if (Resources[typeof(PewPewControl)] is Style ppcStyle)
                Style = ppcStyle;
            else
                throw new InvalidOperationException($"Missing DialogStyle for dialogs of type {GetType().Name}");

            base.EndInit();
        }
    }
}