﻿using System.Windows.Input;

namespace PewPew.UI
{
    public interface ICommand<TParam> : ICommand
    {
        bool CanExecute(TParam parameter);
        void Execute(TParam parameter);
    }
}