﻿using System.ComponentModel;

namespace PewPew.UI
{
    public interface IViewModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
    }
}