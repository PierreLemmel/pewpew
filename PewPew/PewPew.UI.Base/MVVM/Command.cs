﻿using System;
using System.Windows.Input;

namespace PewPew.UI
{
    public abstract class Command<TParam> : ICommand<TParam>, ICommand
    {
        private EventHandler? canExecuteChangedEventHandler;
        event EventHandler? ICommand.CanExecuteChanged
        {
            add => canExecuteChangedEventHandler += value;
            remove => canExecuteChangedEventHandler -= value;
        }
        
        public virtual bool CanExecute(TParam parameter) => true;
        public abstract void Execute(TParam parameter);

        protected void RaiseCanExecuteChanged() => canExecuteChangedEventHandler?.Invoke(this, EventArgs.Empty);

        bool ICommand.CanExecute(object? parameter) => CanExecute((TParam)parameter!);
        void ICommand.Execute(object? parameter) => Execute((TParam)parameter!);
    }

    public abstract class Command : ICommand
    {
        private EventHandler? canExecuteChangedEventHandler;
        event EventHandler? ICommand.CanExecuteChanged
        {
            add => canExecuteChangedEventHandler += value;
            remove => canExecuteChangedEventHandler -= value;
        }

        protected void RaiseCanExecuteChanged() => canExecuteChangedEventHandler?.Invoke(this, EventArgs.Empty);

        public virtual bool CanExecute() => true;
        public abstract void Execute();

        bool ICommand.CanExecute(object? parameter) => CanExecute();
        void ICommand.Execute(object? parameter) => Execute();

        public static Command<TParam> Create<TParam>(Action<TParam> execute) => new CommandImpl<TParam>(execute);
        public static Command<TParam> Create<TParam>(Action<TParam> execute, Func<TParam, bool> canExecute) => new CommandImpl<TParam>(execute, canExecute);

        public static Command Create(Action execute) => new CommandImpl(execute);
        public static Command Create(Action execute, Func<bool> canExecute) => new CommandImpl(execute, canExecute);

        private class CommandImpl<TParam> : Command<TParam>
        {
            private readonly Action<TParam> execute;
            private readonly Func<TParam, bool> canExecute;

            public CommandImpl(Action<TParam> execute, Func<TParam, bool>? canExecute = null)
            {
                this.execute = execute;
                this.canExecute = canExecute ?? (_ => true);
            }

            public override void Execute(TParam parameter) => execute.Invoke(parameter);
            public override bool CanExecute(TParam parameter) => canExecute.Invoke(parameter);
        }

        private class CommandImpl : Command
        {
            private readonly Action execute;
            private readonly Func<bool> canExecute;

            public CommandImpl(Action execute, Func<bool>? canExecute = null)
            {
                this.execute = execute;
                this.canExecute = canExecute ?? (() => true);
            }

            public override void Execute() => execute.Invoke();
            public override bool CanExecute() => canExecute.Invoke();
        }
    }
}