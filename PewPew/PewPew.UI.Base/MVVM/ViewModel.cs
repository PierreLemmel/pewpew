using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using FluentValidation;
using FluentValidation.Results;

namespace PewPew.UI
{
    public abstract class ViewModel<TThisViewModel> : IViewModel
        where TThisViewModel : ViewModel<TThisViewModel>
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        protected internal void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private ValidationResult? lastResult;

        public ViewModel()
        {
            // Validator is not injected cause injecting an empty class in every ViewModelConstructor is boring
            // and I want to keep validation configuration inside ViewModel
            AbstractValidator<TThisViewModel> validator = new ViewModelValidator();
            SetupValidator(validator);

            this.PropertyChanged += (sender, ea) =>
            {
                string propertyName = ea.PropertyName!;

                lastResult = validator.Validate((TThisViewModel)this);
                ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));

                if (!propertyName.IsOneOf(nameof(HasErrors), nameof(IsValid)))
                {
                    OnPropertyChanged(nameof(HasErrors));
                    OnPropertyChanged(nameof(IsValid));
                }
            };
        }

        protected virtual void SetupValidator(AbstractValidator<TThisViewModel> validator) { }

        public bool HasErrors => !(lastResult?.IsValid) ?? false;
        public bool IsValid => lastResult?.IsValid ?? false;

        public event EventHandler<DataErrorsChangedEventArgs>? ErrorsChanged;

        public IEnumerable GetErrors(string? propertyName)
        {
            if (lastResult is null) return Enumerable.Empty<string>();

            return lastResult.Errors
                .Where(error => error.PropertyName == propertyName)
                .Select(error => error.ErrorMessage);
        }

        private class ViewModelValidator : AbstractValidator<TThisViewModel> { }
    }
}