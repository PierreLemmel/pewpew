using PewPew.DataAccess;
using PewPew.DataAccess.Events;
using PewPew.Events.Handlers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace PewPew.UI.Observable
{
    internal class ObservableDataSetImpl<TModel> : ObservableDataSet<TModel>,
        IEventHandler,
        IEventHandler<DataSetCleared<TModel>>,
        IEventHandler<EntityCreated<TModel>>,
        IEventHandler<EntityDeleted<TModel>>,
        IEventHandler<EntityUpdated<TModel>>,
        IEventHandler<EntitiesCreated<TModel>>,
        IEventHandler<EntitiesDeleted<TModel>>,
        IEventHandler<EntitiesUpdated<TModel>>,
        IDisposable
        where TModel : EntityModel
    {
        protected readonly List<TModel> items;
        private readonly IHandlerRegistrar registrar;

        public override int Count => items.Count;
        public override TModel this[int index] => items[index];

        public ObservableDataSetImpl(IHandlerRegistrar registrar, IModelReader<TModel> reader)
        {
            items = reader.GetAll().ToList();

            this.registrar = registrar;
            
            registrar.AddHandler(this);
        }

        public void Handle(DataSetCleared<TModel> @event)
        {
            items.Clear();
            RaiseCollectionChanged(NotifyCollectionChangedAction.Reset);
        }

        public void Handle(EntityCreated<TModel> @event)
        {
            TModel createdEntity = @event.Entity;
            items.Add(createdEntity);

            RaiseCollectionChanged(NotifyCollectionChangedAction.Add, createdEntity);
        }

        public void Handle(EntitiesCreated<TModel> @event)
        {
            IReadOnlyCollection<TModel> createdEntities = @event.Entities;
            items.AddRange(createdEntities);

            RaiseCollectionChanged(NotifyCollectionChangedAction.Add, createdEntities.ToList());
        }

        public void Handle(EntityUpdated<TModel> @event)
        {
            TModel updatedEntity = @event.Entity;

            int updateIndex = items.FindIndex(m => m.Id == updatedEntity.Id);

            TModel oldItem = items[updateIndex];
            items[updateIndex] = updatedEntity;

            RaiseCollectionChanged(NotifyCollectionChangedAction.Replace, oldItem, updatedEntity);
        }

        public void Handle(EntitiesUpdated<TModel> @event)
        {
            IReadOnlyCollection<TModel> newItems = @event.Entities;

            List<TModel> oldItems = new List<TModel>(newItems.Count);

            foreach (TModel updatedEntity in newItems)
            {
                int updateIndex = items.FindIndex(m => m.Id == updatedEntity.Id);

                oldItems.Add(items[updateIndex]);
                items[updateIndex] = updatedEntity;
            }

            RaiseCollectionChanged(NotifyCollectionChangedAction.Replace, oldItems, newItems.ToList());
        }

        public void Handle(EntityDeleted<TModel> @event)
        {
            int removedIndex = items.FindIndex(m => m.Id == @event.EntityId);
            TModel oldItem = items[removedIndex];
            
            items.RemoveAt(removedIndex);

            RaiseCollectionChanged(NotifyCollectionChangedAction.Remove, oldItem, removedIndex);
        }

        public void Handle(EntitiesDeleted<TModel> @event)
        {
            HashSet<Guid> removedIds = @event.Entities.ToHashSet();
            
            List<TModel> removedItems = new List<TModel>(removedIds.Count);
            List<TModel> preservedItems = new List<TModel>(items.Count - removedIds.Count);

            int? startingIndex = null;
            for (int i = 0; i < items.Count; i++)
            {
                TModel item = items[i];

                if (removedIds.Contains(item.Id))
                {
                    removedItems.Add(item);
                    startingIndex ??= i;
                }
                else
                    preservedItems.Add(item);
            }

            items.Clear();
            items.AddRange(preservedItems);

            RaiseCollectionChanged(NotifyCollectionChangedAction.Reset);
        }


        public void Dispose() => registrar.RemoveHandler(this);

        public override IEnumerator<TModel> GetEnumerator() => items.GetEnumerator();
    }
}