using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace PewPew.UI.Observable
{
    public abstract class ObservableDataSet<TModel> : IReadOnlyList<TModel>, INotifyCollectionChanged where TModel : EntityModel
    {
        public event NotifyCollectionChangedEventHandler? CollectionChanged;

        protected void RaiseCollectionChanged(NotifyCollectionChangedAction action) => CollectionChanged?.Invoke(this, new(action));
        protected void RaiseCollectionChanged(NotifyCollectionChangedAction action, TModel changedItem) => CollectionChanged?.Invoke(this, new(action, changedItem));
        protected void RaiseCollectionChanged(NotifyCollectionChangedAction action, TModel changedItem, int index) => CollectionChanged?.Invoke(this, new(action, changedItem, index));
        protected void RaiseCollectionChanged(NotifyCollectionChangedAction action, List<TModel> changedItems) => CollectionChanged?.Invoke(this, new(action, changedItems));
        protected void RaiseCollectionChanged(NotifyCollectionChangedAction action, List<TModel> changedItems, int index) => CollectionChanged?.Invoke(this, new(action, changedItems, index));
        protected void RaiseCollectionChanged(NotifyCollectionChangedAction action, TModel oldItem, TModel newItem) => CollectionChanged?.Invoke(this, new(action, oldItem, newItem));
        protected void RaiseCollectionChanged(NotifyCollectionChangedAction action, List<TModel> oldItems, List<TModel> newItems) => CollectionChanged?.Invoke(this, new(action, oldItems, newItems));

        public abstract TModel this[int index] { get; }
        public abstract int Count { get; }

        public abstract IEnumerator<TModel> GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}