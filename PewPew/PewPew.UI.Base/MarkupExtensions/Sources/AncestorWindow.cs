using System.Windows;
using System.Windows.Data;

namespace PewPew.UI
{
    public sealed class AncestorWindow : RelativeSource
    {
        public AncestorWindow() : base() => AncestorType = typeof(Window);
    }
}