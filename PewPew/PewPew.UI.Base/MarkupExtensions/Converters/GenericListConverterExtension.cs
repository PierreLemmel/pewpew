using PewPew.UI.Converter;
using System;
using System.Windows.Data;
using System.Windows.Markup;

namespace PewPew.UI
{
    [MarkupExtensionReturnType(typeof(IValueConverter))]
    public class GenericListConverterExtension : MarkupExtension
    {
        public Type ItemType { get; set; }

        public GenericListConverterExtension(Type itemType) => ItemType = itemType;

        public override object ProvideValue(IServiceProvider serviceProvider) => Converters.GenericListConverters[ItemType];
    }
}