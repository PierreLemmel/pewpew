using Autofac;
using PewPew.UI.Menus;
using System;

namespace PewPew.UI
{
    public static class MenuContainerBuilderExtensions
    {
        public static ContainerBuilder AddViewMenu<TViewMenu>(this ContainerBuilder containerBuilder, int priority) where TViewMenu : PewPewViewMenu
        {
            containerBuilder.RegisterType<TViewMenu>()
                .As<PewPewViewMenu>()
                .OnActivating(activating => activating.Instance.Priority = priority);

            return containerBuilder;
        }

        public static ContainerBuilder AddViewMenu<TViewMenu>(this ContainerBuilder containerBuilder) where TViewMenu : PewPewViewMenu
        {
            containerBuilder.RegisterType<TViewMenu>()
                .As<PewPewViewMenu>();

            return containerBuilder;
        }
    }
}