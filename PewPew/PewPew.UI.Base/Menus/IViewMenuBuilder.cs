﻿using System;
using System.Windows.Controls;

namespace PewPew.UI.Menus
{
    public interface IViewMenuBuilder
    {
        void BuildViewMenu(MenuItem viewMenu);
    }
}