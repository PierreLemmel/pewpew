﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace PewPew.UI.Menus
{
    internal class ViewMenuBuilder : IViewMenuBuilder
    {
        private readonly IReadOnlyCollection<PewPewViewMenu> menus;

        public ViewMenuBuilder(IEnumerable<PewPewViewMenu> viewItems)
        {
            this.menus = viewItems
                .OrderByDescending(item => item.Priority)
                .ThenBy(item => item.Header.ToString())
                .ToList();
        }

        public void BuildViewMenu(MenuItem viewMenu)
        {
            viewMenu.Items.Clear();

            foreach(PewPewViewMenu ppvm in menus)
            {
                viewMenu.Items.Add(ppvm);
            }
        }
    }
}