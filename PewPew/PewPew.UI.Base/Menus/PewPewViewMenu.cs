using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Menus
{
    public abstract class PewPewViewMenu : PewPewMenu
    {
        protected PewPewViewMenu(IViewModel viewModel) : base(viewModel)
        {
        }
    }
}