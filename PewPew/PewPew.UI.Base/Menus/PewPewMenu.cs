using System;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Menus
{
    public abstract class PewPewMenu : MenuItem
    {
        public IViewModel ViewModel { get; }

        public static readonly DependencyProperty PriorityProperty;

        static PewPewMenu()
        {
            PriorityProperty = DependencyProperty.Register(nameof(Priority), typeof(int), typeof(PewPewViewMenu), new PropertyMetadata(defaultValue: 0));
        }

        public PewPewMenu(IViewModel viewModel)
        {
            ViewModel = viewModel;
            DataContext = ViewModel;
        }

        public int Priority
        {
            get => (int)GetValue(PriorityProperty);
            set => SetValue(PriorityProperty, value);
        }
    }
}