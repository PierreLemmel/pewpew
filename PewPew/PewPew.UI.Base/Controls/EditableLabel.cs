using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PewPew.UI.Controls
{
    public class EditableLabel : TextBox
    {
        private static readonly DependencyPropertyKey isEditingPropertyKey;
        public static readonly DependencyProperty IsEditingProperty;

        static EditableLabel()
        {
            isEditingPropertyKey = DependencyProperty.RegisterReadOnly(
                nameof(IsEditing), typeof(bool), typeof(EditableLabel), new PropertyMetadata(defaultValue: false));
            IsEditingProperty = isEditingPropertyKey.DependencyProperty;
        }

        private string oldValue = "";

        private RoutedEventHandler? startEditingEventHandler;
        public event RoutedEventHandler StartEditing
        {
            add => startEditingEventHandler += value;
            remove => startEditingEventHandler -= value;
        }

        private RoutedEventHandler? cancelEditingEventHandler;
        public event RoutedEventHandler CancelEditing
        {
            add => cancelEditingEventHandler += value;
            remove => cancelEditingEventHandler -= value;
        }

        private RoutedEventHandler? commitEditingEventHandler;
        public event RoutedEventHandler CommitEditing
        {
            add => commitEditingEventHandler += value;
            remove => commitEditingEventHandler -= value;
        }

        public bool IsEditing
        {
            get => (bool)GetValue(IsEditingProperty);
            private set => SetValue(isEditingPropertyKey, value);
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            IsReadOnly = true;
        }

        // Don't block click events to bubble up when parent is a ListBox for instance
        protected override void OnMouseDown(MouseButtonEventArgs e) => e.Handled = false;

        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            if (!IsEditing)
                StartEdition();
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);

            if (IsEditing)
                CancelEdition();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (!IsEditing) return;

            if (e.Key == Key.Enter)
            {
                CommitEdition();
            }
            else if (e.Key == Key.Escape)
            {
                CancelEdition();
            }
            else
                base.OnKeyDown(e);
        }

        private void StartEdition()
        {
            oldValue = Text;
            IsReadOnly = false;
            IsEditing = true;
            startEditingEventHandler?.Invoke(this, new RoutedEventArgs());
        }

        private void CancelEdition()
        {
            IsReadOnly = true;
            IsEditing = false;
            Text = oldValue;
            cancelEditingEventHandler?.Invoke(this, new RoutedEventArgs());
            Select(0, 0);
        }

        private void CommitEdition()
        {
            oldValue = Text;
            IsReadOnly = true;
            commitEditingEventHandler?.Invoke(this, new RoutedEventArgs());
        }
    }
}