using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PewPew.UI.Controls
{
    public class PewPewButton : Button
    {
        public static readonly DependencyProperty TextProperty;

        private static readonly DependencyPropertyKey hasTextPropertyKey;
        public static readonly DependencyProperty HasTextProperty;

        public static readonly DependencyProperty IconProperty;

        public static readonly DependencyProperty IconColorProperty;

        private static readonly DependencyPropertyKey hasIconPropertyKey;
        public static readonly DependencyProperty HasIconProperty;

        static PewPewButton()
        {
            TextProperty = DependencyProperty.Register(nameof(Text), typeof(string), typeof(PewPewButton), new(OnTextChanged));

            hasTextPropertyKey = DependencyProperty.RegisterReadOnly(nameof(HasText), typeof(bool), typeof(PewPewButton), new());
            HasTextProperty = hasTextPropertyKey.DependencyProperty;

            IconProperty = DependencyProperty.Register(nameof(Icon), typeof(Brush), typeof(PewPewButton), new(OnIconChanged));

            IconColorProperty = DependencyProperty.Register(nameof(IconColor), typeof(Brush), typeof(PewPewButton), new(OnIconColorChanged));

            hasIconPropertyKey = DependencyProperty.RegisterReadOnly(nameof(HasIcon), typeof(bool), typeof(PewPewButton), new());
            HasIconProperty = hasIconPropertyKey.DependencyProperty;
        }

        public PewPewButton() : base() { }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }
        
        public bool HasText
        {
            get => (bool)GetValue(HasTextProperty);
            private set => SetValue(hasTextPropertyKey, value);
        }

        public Brush Icon
        {
            get => (Brush)GetValue(IconProperty);
            set => SetValue(IconProperty, value);
        }

        public Brush IconColor
        {
            get => (Brush)GetValue(IconColorProperty);
            set => SetValue(IconColorProperty, value);
        }

        public bool HasIcon
        {
            get => (bool)GetValue(HasIconProperty);
            private set => SetValue(hasIconPropertyKey, value);
        }

        private void UpdateIconColor()
        {
            if (Icon is not null and DrawingBrush drawingBrush)
            {
                drawingBrush.Drawing.SetAllBrushesInDrawing(IconColor ?? Foreground);
            }
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == ForegroundProperty)
                UpdateIconColor();
        }

        private static void OnTextChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PewPewButton ppb = (PewPewButton)sender;
            ppb.HasText = !ppb.Text.IsNullOrEmpty();
        }

        private static void OnIconChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PewPewButton ppb = (PewPewButton)sender;
            ppb.HasIcon = ppb.Icon is not null and DrawingBrush;

            ppb.UpdateIconColor();
        }

        private static void OnIconColorChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PewPewButton ppb = (PewPewButton)sender;
            ppb.UpdateIconColor();
        }
    }
}