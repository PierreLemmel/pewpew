using System.Windows.Media;

namespace PewPew.UI
{
    public static class DrawingHelpers
    {
        public static void SetAllBrushesInDrawing(this Drawing drawing, Brush brush)
        {
            switch (drawing)
            {
                case DrawingGroup group:
                    foreach (Drawing child in group.Children)
                        child.SetAllBrushesInDrawing(brush);
                    break;

                case GeometryDrawing geometry:
                    geometry.Brush = brush;
                    break;
            }
        }
    }
}