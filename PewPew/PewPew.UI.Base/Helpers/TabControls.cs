using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace PewPew.UI
{
    public static class TabControls
    {
        public static void SelectLastItem(this TabControl tab) => tab.SelectedIndex = tab.Items.Count - 1;

        public static TabControl Create(IEnumerable<TabItem> items)
        {
            TabControl tabCtrl = new();

            foreach (TabItem item in items)
                tabCtrl.Items.Add(item);

            return tabCtrl;
        }

        public static TabControl Create(IEnumerable<(string header, Control control)> items) => Create(items.Select(item => new TabItem()
        {
            Header = item.header,
            Content = item.control,
        }));
    }
}