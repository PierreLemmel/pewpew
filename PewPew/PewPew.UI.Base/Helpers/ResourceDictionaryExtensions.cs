using System.Windows;

namespace PewPew.UI
{
    public static class ResourceDictionaryExtensions
    {
        public static T Get<T>(this ResourceDictionary resources, string key) => (T)resources[key];
    }
}