using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI
{
    public static class Grids
    {
        public static Control? GetControl(this Grid grid, int? row = null, int? column = null)
        {
            IEnumerable<Control> children = grid
                .Children
                .OfType<Control>();

            if (row is not null) children = children.Where(child => Grid.GetRow(child) == row);
            if (column is not null) children = children.Where(child => Grid.GetColumn(child) == column);

            return children.FirstOrDefault();
        }

        public static void AddControl(this Grid grid, Control control, int? row = null, int? column = null)
        {
            if (control is null) return;

            grid.Children.Add(control);

            if (row is not null) Grid.SetRow(control, row.Value);
            if (column is not null) Grid.SetColumn(control, column.Value);
        }

        public static void AddControl(this Grid grid, Control control, int row, int col)
        {
            grid.Children.Add(control);

            Grid.SetRow(control, row);
            Grid.SetColumn(control, col);
        }

        public static void Clear(this Grid grid) => grid.Children.Clear();

        public static void SetupRows(this Grid grid, params RowDefinition[] rows) => grid.RowDefinitions.ReplaceWith(rows);
        public static void SetupColumns(this Grid grid, params ColumnDefinition[] columns) => grid.ColumnDefinitions.ReplaceWith(columns);

        public static void SetupRow(this Grid grid, Control[] controls, int row = 0)
        {
            for (int i = 0; i < controls.Length; i++)
            {
                grid.AddControl(controls[i], row: row, col: i);
            }
        }

        public static void SetupColumn(this Grid grid, Control[] controls, int column = 0)
        {
            for (int i = 0; i < controls.Length; i++)
            {
                grid.AddControl(controls[i], row: i, col: column);
            }
        }

        public static void SetControl(this Grid grid, Control control, int? row = 0, int? col = 0)
        {
            Control? removed = grid.GetControl(row, col);
            if (removed is not null)
                grid.Children.Remove(removed);

            grid.AddControl(control, row, col);
        }

        public static void SetupControls(this Grid grid, Control[,] controls)
        {
            int nbOfRows = controls.GetLength(0);
            int nbOfCols = controls.GetLength(1);

            for (int row = 0; row < nbOfRows; row++)
            {
                for (int col = 0; col < nbOfCols; col++)
                {
                    Control control = controls[row, col];
                    grid.AddControl(control, row, col);
                }
            }
        }

        public static Grid Create(
            Control[,]? controls,
            RowDefinition[]? rows = null,
            ColumnDefinition[]? columns = null
        )
        {
            Grid grid = new();

            if (rows is not null) grid.SetupRows(rows);
            if (columns is not null) grid.SetupColumns(columns);
            if (controls is not null) grid.SetupControls(controls);

            return grid;
        }

        public static Grid Vertical(Control[] controls, RowDefinition[]? rows = null)
        {
            Grid grid = new();

            if (rows is not null) grid.SetupRows(rows);

            for (int row = 0; row < controls.Length; row++)
                grid.AddControl(controls[row], row, 0);

            return grid;
        }

        public static Grid Vertical(params (Control control, RowDefinition row)[] elts)
        {
            (Control[] controls, RowDefinition[] rows) = elts.Deconstruct();
            return Vertical(controls, rows);
        }

        public static Grid Horizontal(Control[] controls, ColumnDefinition[]? columns = null)
        {
            Grid grid = new();

            if (columns is not null) grid.SetupColumns(columns);

            for (int col = 0; col < controls.Length; col++)
                grid.AddControl(controls[col], 0, col);

            return grid;
        }

        public static Grid Horizontal(params (Control control, ColumnDefinition col)[] elts)
        {
            (Control[] controls, ColumnDefinition[] columns) = elts.Deconstruct();
            return Horizontal(controls, columns);
        }

        public static class Rows
        {
            public static RowDefinition Auto => new() { Height = GridLength.Auto };
            public static RowDefinition Pixels(double pixels) => new() { Height = new(pixels, GridUnitType.Pixel) };
            public static RowDefinition Star(double value = 1.0) => new() { Height = new(value, GridUnitType.Star) };
        }

        public static class Columns
        {
            public static ColumnDefinition Auto => new() { Width = GridLength.Auto };
            public static ColumnDefinition Pixels(double pixels) => new() { Width = new(pixels, GridUnitType.Pixel) };
            public static ColumnDefinition Star(double value = 1.0) => new() { Width = new(value, GridUnitType.Star) };
        }
    }
}