using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PewPew.UI
{
    public static class ControlTreeHelper
    {
        public static IEnumerable<TDependency> EnumerateChildren<TDependency>(this DependencyObject reference)
            where TDependency : DependencyObject
            => reference.EnumerateChildren().OfType<TDependency>();

        public static IEnumerable<DependencyObject> EnumerateChildren(this DependencyObject reference) => reference
            .EnumerateFirstLevelChildren()
            .SelectMany(EnumerateThisAndChildren);

        private static IEnumerable<DependencyObject> EnumerateThisAndChildren(this DependencyObject reference) => reference.EnumerateChildren().Prepend(reference);

        public static IEnumerable<TDependency> EnumerateParents<TDependency>(this DependencyObject reference)
            where TDependency : DependencyObject
            => reference.EnumerateParents().OfType<TDependency>();

        public static IEnumerable<DependencyObject> EnumerateParents(this DependencyObject reference)
        {
            DependencyObject? current = (reference as FrameworkElement)?.Parent;

            while(current is not null and FrameworkElement fe)
            {
                yield return current;
                current = fe.Parent;
            }
        }

        private static IEnumerable<DependencyObject> EnumerateFirstLevelChildren(this DependencyObject reference) => reference.HasDependencyContent(out DependencyObject? dependencyContent) ?
            Enumerables.Create(dependencyContent) :
            Enumerables.Create(
                VisualTreeHelper.GetChildrenCount(reference),
                index => VisualTreeHelper.GetChild(reference, index)
            );

        private static bool HasDependencyContent(this DependencyObject reference, [NotNullWhen(true)] out DependencyObject? dependencyContent)
        {
            if (reference is ContentControl cc && cc.Content is DependencyObject content)
            {
                dependencyContent = content;
                return true;
            }
            else
            {
                dependencyContent = null;
                return false;
            }
        }


        public static DependencyObject FindInChildren(this DependencyObject reference, Func<DependencyObject, bool> predicate) => reference.EnumerateChildren().First(predicate);
        public static TChild FindInChildren<TChild>(this DependencyObject reference)
            where TChild : DependencyObject
            => reference.EnumerateChildren<TChild>().First();
        public static TChild FindInChildren<TChild>(this DependencyObject reference, Func<TChild, bool> predicate)
            where TChild : DependencyObject
            => reference.EnumerateChildren<TChild>().First(predicate);

        public static bool TryFindInChildren(this DependencyObject reference, Func<DependencyObject, bool> predicate, [NotNullWhen(true)] out DependencyObject? result) => reference
            .EnumerateChildren()
            .TryFind(predicate, out result);

        public static bool TryFindInChildren<TChild>(this DependencyObject reference, Func<TChild, bool> predicate, [NotNullWhen(true)] out TChild? result)
            where TChild : DependencyObject => reference
            .EnumerateChildren<TChild>()
            .TryFind(predicate, out result);
        public static bool TryFindInChildren<TChild>(this DependencyObject reference, [NotNullWhen(true)] out TChild? result)
            where TChild : DependencyObject => reference
            .EnumerateChildren<TChild>()
            .TryFind(out result);


        public static DependencyObject FindInParents(this DependencyObject reference, Func<DependencyObject, bool> predicate) => reference.EnumerateParents().First(predicate);
        public static TChild FindInParents<TChild>(this DependencyObject reference)
            where TChild : DependencyObject
            => reference.EnumerateParents<TChild>().First();
        public static TParent FindInParents<TParent>(this DependencyObject reference, Func<TParent, bool> predicate)
            where TParent : DependencyObject
            => reference.EnumerateParents<TParent>().First(predicate);

        public static bool TryFindInParents(this DependencyObject reference, Func<DependencyObject, bool> predicate, [NotNullWhen(true)] out DependencyObject? result) => reference
            .EnumerateParents()
            .TryFind(predicate, out result);

        public static bool TryFindInParents<TParent>(this DependencyObject reference, Func<TParent, bool> predicate, [NotNullWhen(true)] out TParent? result)
            where TParent : DependencyObject => reference
            .EnumerateParents<TParent>()
            .TryFind(predicate, out result);

        public static bool TryFindInParents<TParent>(this DependencyObject reference, [NotNullWhen(true)] out TParent? result)
            where TParent : DependencyObject => reference
            .EnumerateParents<TParent>()
            .TryFind(out result);
    }
}