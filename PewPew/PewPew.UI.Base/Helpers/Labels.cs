using System.Windows.Controls;

namespace PewPew.UI
{
    public static class Labels
    {
        public static Label Create(string label) => new()
        {
            Content = label,
        };
    }
}