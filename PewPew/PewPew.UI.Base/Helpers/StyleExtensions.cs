using System;
using System.Windows;

namespace PewPew.UI
{
    public static class StyleExtensions
    {
        public static bool IsEmpty(this Style style) => style.Setters.IsEmpty();

        public static bool HasTargetTypeInHierarchy(this Style style, Type targetType)
            => style.TargetType == targetType || (style.BasedOn?.HasTargetTypeInHierarchy(targetType) ?? false);
    }
}