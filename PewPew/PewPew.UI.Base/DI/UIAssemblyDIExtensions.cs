using Autofac;
using FluentValidation;
using PewPew.UI.Factories;
using PewPew.UI.Menus;
using PewPew.UI.Observable;
using PewPew.UI.Orchestration;
using System.Reflection;

namespace PewPew.UI
{
    public static class UIAssemblyDIExtensions
    {
        public static void RegisterBaseUITypes(this ContainerBuilder builder, Assembly uiAssembly)
        {
            builder.RegisterSubtypesOf<Dialog>(uiAssembly);
            builder.RegisterSubtypesOf<PewPewControl>(uiAssembly);
            builder.RegisterSubtypesOf<PewPewMenu>(uiAssembly);
            builder.RegisterImplementationsAsSelf<IViewModel>(uiAssembly);
        }

        public static void AddObservableDatasets(this ContainerBuilder builder) => builder.RegisterGeneric(typeof(ObservableDataSetImpl<>)).As(typeof(ObservableDataSet<>));

        public static void AddMenuBuilders(this ContainerBuilder builder) => builder.RegisterType<ViewMenuBuilder>().As<IViewMenuBuilder>();

        public static void AddControlFactory(this ContainerBuilder builder) => builder.RegisterType<DefaultPewPewControlFactory>().As<IPewPewControlFactory>();
        public static void AddDialogFactory(this ContainerBuilder builder) => builder.RegisterGeneric(typeof(DefaultDialogFactory<>)).As(typeof(IDialogFactory<>));

        public static void AddControlOrchestraction(this ContainerBuilder builder)
        {
            builder.RegisterType<TabbedMDIControlOrchestrator>()
                .As<IControlOrchestrator>()
                .As<IEventHandler>()
                .SingleInstance();
        }
    }
}