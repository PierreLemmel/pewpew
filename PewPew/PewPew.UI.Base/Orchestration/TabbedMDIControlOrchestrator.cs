﻿using PewPew.Events.Handlers;
using PewPew.UI.Controls;
using PewPew.UI.Events;
using PewPew.UI.Events.User;
using PewPew.UI.Layout;
using PewPew.UI.Orchestration.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Orchestration
{
    internal class TabbedMDIControlOrchestrator : IControlOrchestrator, IEventHandler, IEventHandler<ControlRequested>
    {
        private Window? mainWindow;

        private bool IsInitialized => mainWindow is not null;

        private readonly IPewPewControlFactory factory;
        private readonly IHandlerRegistrar registrar;


        public TabbedMDIControlOrchestrator(
            IPewPewControlFactory factory,
            IHandlerRegistrar handlerRegistrar)
        {
            this.factory = factory;
            this.registrar = handlerRegistrar;

            handlerRegistrar.AddHandler(this);
        }

        public void StartOrchestration(Window mainWindow)
        {
            if (IsInitialized)
                throw new InvalidOperationException("Orchestration already started");

            this.mainWindow = mainWindow;
        }

        public void Handle(ControlRequested @event)
        {
            if (!IsInitialized) return;

            HandleRequestControl(@event.ControlType, @event.ControlName);
        }

        private void HandleRequestControl(Type controlType, string controlName)
        {
            if (EnumerateControls().TryFindByType(controlType, out PewPewControl? control))
            {
                FocusControl(control);
            }
            else
            {
                AddControl(controlType, controlName);
            }
        }

        private void FocusControl(PewPewControl control)
        {
            control.Focus();
        }

        private void AddControl(Type controlType, string controlName)
        {
            PewPewControl control = factory.CreateControl(controlType);

            (ContentControl target, TabbedMDIAnchor anchor) = GetNewControlLocation(control);
            AddControlOnTarget(control, controlName, target, anchor);
        }

        private (ContentControl target, TabbedMDIAnchor anchor) GetNewControlLocation(PewPewControl control)
        {
            ContentControl target = mainWindow!.TryFindInChildren(out PewPewControlContainer? result) ? 
                result : mainWindow.FindInChildren<RootContainer>();

            TabbedMDIAnchor anchor = TabbedMDIAnchor.Right;

            return (target, anchor);
        }

        private void AddControlOnTarget(PewPewControl control, string controlName, ContentControl target, TabbedMDIAnchor anchor)
        {
            PewPewControlContainer newContainer = PewPewControlContainer.Create(controlName, control);
            switch (target)
            {
                case RootContainer root:
                    root.Content = newContainer;
                    break;
                case PewPewControlContainer targetContainer:
                    Union<VerticallySplittedPanel, HorizontallySplittedPanel> parent = targetContainer.EnumerateParents().FirstOfTypes<VerticallySplittedPanel, HorizontallySplittedPanel>();

                    Control created = CreateControlWithAddedElement(targetContainer, newContainer, anchor);
                    break;
                default:
                    throw new InvalidOperationException($"Unexpected type: {control.TypeName()}");
            }
        }

        private Control CreateControlWithAddedElement(PewPewControlContainer targetContainer, PewPewControlContainer newContainer, TabbedMDIAnchor anchor) => anchor switch
        {
            TabbedMDIAnchor.Left => VerticallySplittedPanel.Create(left: newContainer, right: targetContainer),
            TabbedMDIAnchor.Right => VerticallySplittedPanel.Create(left: targetContainer, right: newContainer),
            TabbedMDIAnchor.Top => HorizontallySplittedPanel.Create(top: newContainer, bottom: targetContainer),
            TabbedMDIAnchor.Bottom => HorizontallySplittedPanel.Create(top: targetContainer, bottom: newContainer),
            _ => throw new InvalidOperationException($"Unexpected anchor: {anchor}")
        };

        private Control ReplaceContent(ContentControl target)
        {
            throw new NotImplementedException();
        }

        public void Dispose() => registrar.RemoveHandler(this);

        private IEnumerable<PewPewControl> EnumerateControls() => mainWindow!
            .EnumerateChildren()
            .OfType<PewPewControl>();
    }
}