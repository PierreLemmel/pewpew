using PewPew.UI.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Orchestration.Controls
{
    public class PewPewControlContainer : GridControl
    {
        public static readonly DependencyProperty ContainerTitleProperty;
        public static readonly DependencyProperty ContainedControlProperty;

        static PewPewControlContainer()
        {
            ContainerTitleProperty = DependencyProperty.Register(nameof(ContainerTitle), typeof(string), typeof(PewPewControlContainer), new(OnContainerTitleChanged));
            ContainedControlProperty = DependencyProperty.Register(nameof(ContainedControl), typeof(PewPewControl), typeof(PewPewControlContainer), new(OnContainedControlChanged));
        }

        public string ContainerTitle
        {
            get => (string)GetValue(ContainerTitleProperty);
            set => SetValue(ContainerTitleProperty, value);
        }

        public PewPewControl ContainedControl
        {
            get => (PewPewControl)GetValue(ContainedControlProperty);
            set => SetValue(ContainedControlProperty, value);
        }


        protected override void Setup()
        {
            mainGrid.SetupRows(
                Grids.Rows.Auto,
                Grids.Rows.Star(1.0)
            );
        }

        private void OnContainedControlChanged() => mainGrid.SetControl(ContainedControl, row: 1);
        private static void OnContainedControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PewPewControlContainer ppcc = (PewPewControlContainer)d;
            ppcc.OnContainedControlChanged();
        }

        private void OnContainerTitleChanged() => mainGrid.SetControl(Labels.Create(ContainerTitle), row: 0);
        private static void OnContainerTitleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PewPewControlContainer ppcc = (PewPewControlContainer)d;
            ppcc.OnContainerTitleChanged();
        }

        public static PewPewControlContainer Create(string title, PewPewControl control) => new()
        { 
            ContainerTitle = title,
            ContainedControl = control
        };
    }
}