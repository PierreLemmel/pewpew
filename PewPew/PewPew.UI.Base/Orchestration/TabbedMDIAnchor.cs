namespace PewPew.UI.Orchestration
{
    internal enum TabbedMDIAnchor
    {
        Fit,

        Left,
        Right,
        Top,
        Bottom,

        Floating,
    }
}