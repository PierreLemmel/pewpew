using System.Windows;

namespace PewPew.UI.Orchestration
{
    public interface IControlOrchestrator
    {
        void StartOrchestration(Window mainWindow);
    }
}