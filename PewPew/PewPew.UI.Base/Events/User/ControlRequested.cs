using System;

namespace PewPew.UI.Events.User
{
    public class ControlRequested : UserControlRequestEvent
    {
        public override string Name => $"{ControlName}Requested";

        protected ControlRequested(Type controlType) : base(controlType) { }
    }

    public class ControlRequested<TControl> : ControlRequested where TControl : PewPewControl
    {
        public ControlRequested() : base(typeof(TControl)) { }
    }
}