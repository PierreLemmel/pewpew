using Newtonsoft.Json;
using System;

namespace PewPew.UI.Events.User
{
    public abstract class UserControlRequestEvent : UserRequestEvent
    {
        [JsonIgnore]
        public Type ControlType { get; }

        public string ControlName => ControlType.Name;

        protected UserControlRequestEvent(Type controlType)
        {
            ControlType = controlType;
        }
    }
}