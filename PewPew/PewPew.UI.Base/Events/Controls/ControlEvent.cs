using Newtonsoft.Json;
using System;

namespace PewPew.UI.Events
{
    public abstract class ControlEvent : Event
    {
        [JsonIgnore]
        public Type ControlType { get; }

        public string ControlName => ControlType.Name;

        protected ControlEvent(Type controlType)
        {
            ControlType = controlType;
        }
    }
}