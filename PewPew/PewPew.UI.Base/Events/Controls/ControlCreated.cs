using System;

namespace PewPew.UI.Events
{
    public class ControlCreated : ControlEvent
    {
        public override string Name => $"{ControlName}Created";

        public ControlCreated(Type controlType) : base(controlType) { }
    }
}