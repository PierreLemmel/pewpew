using System;

namespace PewPew.UI.Events
{
    public class ControlClosed : ControlEvent
    {
        public override string Name => $"{ControlName}Closed";

        public ControlClosed(Type controlType) : base(controlType) { }
    }
}