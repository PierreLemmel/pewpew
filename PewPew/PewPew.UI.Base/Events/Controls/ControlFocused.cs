using System;

namespace PewPew.UI.Events
{
    public class ControlFocused : ControlEvent
    {
        public override string Name => $"{ControlName}Focused";

        public ControlFocused(Type controlType) : base(controlType) { }
    }
}