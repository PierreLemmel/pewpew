namespace PewPew.UI
{
    public static class PewPewControlFactoryExtensions
    {
        public static TPewPewControl CreateControl<TPewPewControl>(this IPewPewControlFactory factory)
            where TPewPewControl : PewPewControl
            => (TPewPewControl)factory.CreateControl(typeof(TPewPewControl));
    }
}