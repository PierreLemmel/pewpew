﻿using System;

namespace PewPew.UI
{
    public interface IPewPewControlFactory
    {
        PewPewControl CreateControl(Type controlType);
    }
}