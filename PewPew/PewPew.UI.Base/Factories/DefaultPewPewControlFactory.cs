﻿using Autofac;
using System;

namespace PewPew.UI.Factories
{
    internal class DefaultPewPewControlFactory : IPewPewControlFactory
    {
        // Yes: that's anti-pattern to inject container, but it is only one dependency to avoid a billion factories 
        // and I won't do it again, I swear !!!
        private readonly ILifetimeScope scope;

        public DefaultPewPewControlFactory(ILifetimeScope scope) => this.scope = scope;

        public PewPewControl CreateControl(Type controlType) => (PewPewControl)scope.Resolve(controlType);
    }
}