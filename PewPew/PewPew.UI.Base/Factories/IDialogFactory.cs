using System.Windows;

namespace PewPew.UI
{
    public interface IDialogFactory<TDialog> where TDialog : Dialog
    {
        TDialog CreateDialog(Window? owner);
    }
}