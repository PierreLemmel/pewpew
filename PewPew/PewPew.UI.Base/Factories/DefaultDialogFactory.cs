using Autofac;
using System.Windows;

namespace PewPew.UI.Factories
{
    public class DefaultDialogFactory<TDialog> : IDialogFactory<TDialog> where TDialog : Dialog
    {
        // Yes: that's anti-pattern to inject container, but it is only one dependency to avoid a billion factories 
        // and I won't do it again, I swear !!!
        private readonly ILifetimeScope scope;

        public DefaultDialogFactory(ILifetimeScope scope) => this.scope = scope;

        public TDialog CreateDialog(Window? owner)
        {
            TDialog dialog = scope.Resolve<TDialog>();

            if (owner is not null)
                dialog.Owner = owner;

            return dialog;
        }
    }
}