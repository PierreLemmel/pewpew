﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("PewPew.UI.Base.Tests")]
[assembly: InternalsVisibleTo("PewPew.UI.Application.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")] //Moq dynamic assembly