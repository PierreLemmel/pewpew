using Moq;
using NFluent;
using NUnit.Framework;
using PewPew.Configuration;
using PewPew.DataAccess.Models;
using PewPew.DataAccess.Services;
using PewPew.Events.Handlers;
using PewPew.Fakes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.DataAccess.Tests.Tests
{
    public class EventSourcerShould
    {
        [Test]
        public void Be_Registered_After_Construction()
        {
            (IHandlerRegistrar registrar, EventSourcer sut) = CreateSut();

            Check.That(sut).IsRegisteredIn(registrar);
        }

        [Test]
        public void Be_Unregistered_After_Disposing()
        {
            (IHandlerRegistrar registrar, EventSourcer sut) = CreateSut();
            
            ((IDisposable)sut).Dispose();
            Check.That(sut).IsNotRegisteredIn(registrar);
        }

        private (IHandlerRegistrar registrar, EventSourcer sut) CreateSut()
        {
            IHandlerRegistrar registrar = new FakeRegistrar();
            IConfigurationProvider<DataAccessOptions> config = new FakeConfigurationProvider<DataAccessOptions>(new() { DbPath = "/" });
            
            EventSourcer sut = new(config, registrar);

            return (registrar, sut);
        }
    }
}