﻿using System.Collections.Generic;

namespace PewPew.Dmx.Models
{
    public class FixtureMode
    {
        public FixtureMode(string name, IReadOnlyCollection<ChanType> channels)
        {
            Name = name;
            Channels = channels;
        }

        public string Name { get; set; }
        public IReadOnlyCollection<ChanType> Channels { get; }
    }
}