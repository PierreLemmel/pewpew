﻿namespace PewPew.Dmx.Models
{
    public enum ChanType
    {
        Dimmer,
        
        Red,
        Green,
        Blue,
        UV,

        White,
        Yellow,
        Amber,

        Strobe,
        Unused
    }
}