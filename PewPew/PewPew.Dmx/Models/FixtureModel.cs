﻿using System;
using System.Collections.Generic;

namespace PewPew.Dmx.Models
{
    public class FixtureModel : EntityModel
    {
        public FixtureModel(Guid id, string name, IReadOnlyList<FixtureMode> modes, Guid manufacturerId) : base(id, name)
        {
            Modes = modes;
            ManufacturerId = manufacturerId;
        }

        public IReadOnlyList<FixtureMode> Modes { get; }
        public Guid ManufacturerId { get; }
    }
}