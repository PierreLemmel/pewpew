﻿using System;

namespace PewPew.Dmx.Models
{
    public class Manufacturer : EntityModel
    {
        public Manufacturer(Guid id, string name) : base(id, name)
        {

        }
    }
}