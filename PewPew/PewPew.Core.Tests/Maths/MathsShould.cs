using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace PewPew.Tests
{
    public class MathsShould
    {
        [Test]
        [TestCaseSource(nameof(IsBetweenTestCaseData))]
        public void IsBetween_Returns_Expected_Result(int value, int min, int max, bool includeBounds, bool expected)
            => Check.That(value.IsBetween(min, max, includeBounds)).IsEqualTo(expected);

        public static IEnumerable<object[]> IsBetweenTestCaseData => new object [][]
        {
            // Include bounds
            new object[] { 0, -1, 1, true, true },
            new object[] { -1, -1, 1, true, true },
            new object[] { 1, -1, 1, true, true },
            new object[] { 0, 2, 5, true, false },
            new object[] { 0, -2, -18, true, false },
            new object[] { 14, 14, 14, true, true },

            // Exclude bounds
            new object[] { 0, -1, 1, false, true },
            new object[] { -1, -1, 1, false, false },
            new object[] { 1, -1, 1, false, false },
            new object[] { 0, 2, 5, false, false },
            new object[] { 0, -2, -18, false, false },
            new object[] { 14, 14, 14, false, false },

            // Reversed bounds
            new object[] { 0, 1, -1, true, false },
            new object[] { -1, 1, -1, true, false },
            new object[] { 1, 1, -1, true, false },
            new object[] { 0, 5, 2, true, false },
            new object[] { 0, -18, -2, true, false },
        };

        [Test]
        [TestCase(100.0f, 0.0f)]
        public void Clamped_Throws_ArgumentException_When_Max_Is_Lower_Than_Min(float min, float max)
            => Check.ThatCode(() => 18.0f.Clamped(min, max)).Throws<ArgumentException>();

        [Test]
        [TestCaseSource(nameof(ClampedTestCaseData))]
        public void Clamped_Returns_Expected_Result(float value, float min, float max, float expected)
            => Check.That(value.Clamped(min, max)).IsEqualTo(expected);

        public static IEnumerable<object[]> ClampedTestCaseData => new object[][]
        {
            new object[] { 0.0f, 0.0f, 100.0f, 0.0f },
            new object[] { 100.0f, 0.0f, 100.0f, 100.0f },
            new object[] { 50.0f, 0.0f, 100.0f, 50.0f },
            new object[] { -50.0f, 0.0f, 100.0f, 0.0f },
            new object[] { 150.0f, 0.0f, 100.0f, 100.0f },
        };
    }
}