using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.Tests
{
    public class PercentageShould
    {
        [Test]
        [TestCase(0.0f)]
        [TestCase(12.3f)]
        [TestCase(66.66f)]
        [TestCase(100.0f)]
        public void Accept_Values_Between_0_And_100(float value) => Check.ThatCode(() => new Percentage(value)).DoesNotThrow();

        [Test]
        [TestCase(-10.0f)]
        [TestCase(-666.666f)]
        public void Accept_Negative_Values(float value) => Check.ThatCode(() => new Percentage(value)).DoesNotThrow();

        [Test]
        [TestCase(101.0f)]
        [TestCase(6666.666f)]
        public void Accept_Values_Greater_Than_100(float value) => Check.ThatCode(() => new Percentage(value)).DoesNotThrow();

        [Test]
        [TestCase(66.6f)]
        public void Equality_Operator_Returns_True_When_Equal(float value) => Check.That(new Percentage(value) == new Percentage(value)).IsTrue();

        [Test]
        [TestCase(66.6f, 33.3f)]
        public void Equality_Operator_Returns_False_When_Not_Equal(float value, float otherValue) => Check.That(new Percentage(value) != new Percentage(otherValue)).IsTrue();

        [Test]
        [TestCaseSource(nameof(EqualsTestCaseData))]
        public void Equals_Returns_Expected(Percentage per, object? obj, bool expected) => Check.That(per.Equals(obj)).IsEqualTo(expected);

        public static IEnumerable<object?[]> EqualsTestCaseData => new object?[][]
        {
            new object?[] { new Percentage(35.0f), new Percentage(35.0f), true },
            new object ?[] { new Percentage(35.0f), new Percentage(-35.0f), false },
            new object ?[] { new Percentage(35.0f), null, false },
            new object ?[] { new Percentage(35.0f), 35.0f, false },
            new object ?[] { new Percentage(35.0f), "35%", false },
        };

        [Test]
        [TestCaseSource(nameof(Clamped0_100TestCaseData))]
        public void Clamped0_100_Returns_Expected(Percentage per, Percentage expected) => Check.That(per.Clamped0_100()).IsEqualTo(expected);

        public static IEnumerable<object[]> Clamped0_100TestCaseData => new object[][]
        {
            new object[] { Percentage.Zero, Percentage.Zero },
            new object[] { new Percentage(50.0f), new Percentage(50.0f) },
            new object[] { Percentage.OneHundred, Percentage.OneHundred },
            new object[] { new Percentage(150.0f), Percentage.OneHundred },
            new object[] { new Percentage(-50.0f), Percentage.Zero },
        };

        [Test]
        [TestCaseSource(nameof(ImplicitToFloatTestCaseData))]
        public void Implicit_To_Float_Returns_Expected(Percentage per, float expected) => Check.That((float)per).IsEqualTo(expected);

        public static IEnumerable<object[]> ImplicitToFloatTestCaseData => new object[][]
        {
            new object[] { Percentage.Zero, 0.0f },
            new object[] { Percentage.OneHundred, 1.0f },
            new object[] { new Percentage(-550.0f), -5.5f },
            new object[] { new Percentage(150.0f), 1.5f },
        };

        [Test]
        [TestCaseSource(nameof(ExplicitFromFloatTestCaseData))]
        public void Explicit_From_Float_Returns_Expected(float value, Percentage expected) => Check.That((Percentage)value).IsEqualTo(expected);

        public static IEnumerable<object[]> ExplicitFromFloatTestCaseData => new object[][]
        {
            new object[] { 0.0f, Percentage.Zero },
            new object[] { 1.0f, Percentage.OneHundred },
            new object[] { -5.5f, new Percentage(-550.0f)},
            new object[] { 1.5f, new Percentage(150.0f) },
        };

        [Test]
        [TestCaseSource(nameof(MultiplicationOperatorTestCaseData))]
        public void Multiplication_Operator_Returns_Expected(float value, Percentage per, float expected) => Check
            .That(value * per)
            .IsEqualTo(per * value)
            .And.IsEqualTo(expected);

        public static IEnumerable<object[]> MultiplicationOperatorTestCaseData => new object[][]
        {
            new object[] { 510.0f, Percentage.Zero, 0.0f },
            new object[] { 510.0f, Percentage.OneHundred, 510.0f },
            new object[] { 12.5f, new Percentage(1000.0f), 125.0f},
            new object[] { 1.5f, new Percentage(-150.0f), -2.25f },
        };

        [Test]
        [TestCaseSource(nameof(AdditionOperatorTestCaseData))]
        public void Addition_Operator_Returns_Expected(Percentage lhs, Percentage rhs, Percentage expected) => Check.That(lhs + rhs).IsEqualTo<Percentage>(expected);

        public static IEnumerable<object[]> AdditionOperatorTestCaseData => new object[][]
        {
            new object[] { new Percentage(25.0f), new Percentage(25.0f), new Percentage(50.0f) },
            new object[] { new Percentage(25.0f), new Percentage(-25.0f), Percentage.Zero },
        };

        [Test]
        [TestCaseSource(nameof(SubstractionOperatorTestCaseData))]
        public void Substraction_Operator_Returns_Expected(Percentage lhs, Percentage rhs, Percentage expected) => Check.That(lhs - rhs).IsEqualTo<Percentage>(expected);

        public static IEnumerable<object[]> SubstractionOperatorTestCaseData => new object[][]
        {
            new object[] { new Percentage(25.0f), new Percentage(25.0f), Percentage.Zero },
            new object[] { new Percentage(25.0f), new Percentage(-25.0f), new Percentage(50.0f) },
        };

        [Test]
        [TestCase(14.0f)]
        public void Unary_Plus_Behaves_As_Expected(float value) => Check.That(+(new Percentage(value))).IsEqualTo<Percentage>(new Percentage(value));

        [Test]
        [TestCase(14.0f)]
        public void Unary_Minus_Behaves_As_Expected(float value) => Check.That(-(new Percentage(value))).IsEqualTo<Percentage>(new Percentage(-value));
    }
}