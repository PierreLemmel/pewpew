﻿using NFluent;
using NUnit.Framework;

namespace PewPew.Tests.Helpers
{
    public class StringsShould
    {
        [Test]
        [TestCase("Hello", "hello")]
        [TestCase("world", "world")]
        public void UncapitalizeFirstLetter_Should_Uncapitalize_First_Letter(string input, string expected)
        {
            string result = input.UncapitalizeFirstLetter();

            Check.That(result).IsEqualTo(expected);
        }

        [Test]
        [TestCase("hello world", "hello world")]
        [TestCase("HeLlO W0rld!!", "HeLlO W0rld!!")]
        [TestCase("Crème brûlée", "Creme brulee")]
        [TestCase("Les Écorcés", "Les Ecorces")]
        [TestCase("Ça va ?", "Ca va ?")]
        public void RemoveDiacritics_Should_Remove_Diacritics(string input, string expected)
        {
            string result = input.RemoveDiacritics();

            Check.That(result).IsEqualTo(expected);
        }

        [Test]
        [TestCase("hello world", "hello world")]
        [TestCase("HeLlO W0rld!!", "HeLlO W0rld")]
        [TestCase("!!Coucou", "Coucou")]
        [TestCase("!!Coucou!!??!!", "Coucou")]
        [TestCase("Ça va ?", "Ça va")]
        [TestCase("? Ça va ?", "Ça va")]
        public void TrimPunctuation_Should_Trim_Punctuation(string input, string expected)
        {
            string result = input.TrimPunctuation();

            Check.That(result).IsEqualTo(expected);
        }

        [Test]
        [TestCase("Hello world!", " ", "Hello")]
        [TestCase("Hello world!", "!", "Hello world")]
        [TestCase("Hello world!", "woups", "Hello world!")]
        [TestCase("Hello world!", "world", "Hello ")]
        public void RemoveAfter_Should_Return_Expected_Result(string input, string separator, string expected)
        {
            string result = input.RemoveAfter(separator);

            Check.That(result).IsEqualTo(expected);
        }

        [Test]
        [TestCase("HelloWorld", "World", "Hello")]
        [TestCase("HelloWorld", "world", "HelloWorld")]
        [TestCase("HelloWorld", "potato", "HelloWorld")]
        [TestCase("HelloWorldWorld", "World", "HelloWorld")]
        [TestCase("HelloWorlds", "World", "HelloWorlds")]
        public void RemoveEnd_With_Case_Should_Return_Expected_Results(string input, string toRemove, string expected) => Check.That(input.RemoveEnd(toRemove)).IsEqualTo(expected);

        [Test]
        [TestCase("", "")]
        [TestCase("Il y a de l'écho", "ohcé'l ed a y lI")]
        public void Reverse_Should_Return_Expected(string input, string expected) => Check.That(input.Reverse()).IsEqualTo(expected);
    }
}