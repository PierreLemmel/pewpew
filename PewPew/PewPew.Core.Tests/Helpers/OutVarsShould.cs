using NFluent;
using NUnit.Framework;
using PewPew.Helpers;
using PewPew.Tests.SampleClasses;

namespace PewPew.Tests.Helpers
{
    public class OutVarsShould
    {
        [Test]
        public void Return_Input_And_Output_Input_Int()
        {
            int input = 18;

            int result = input.OutVar(out int asVar);

            Check.That(result).IsEqualTo(input);
            Check.That(asVar).IsEqualTo(input);
        }

        [Test]
        public void Return_Input_And_Output_Input_SomeClass()
        {
            SomeClass input = new();

            SomeClass result = input.OutVar(out SomeClass asVar);

            Check.That(result).IsEqualTo(input);
            Check.That(asVar).IsEqualTo(input);
        }
    }
}