using NFluent;
using NUnit.Framework;
using PewPew.Tests.SampleClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.Tests.Helpers
{
    public class FiltersShould
    {
        [Test]
        public void FindByType_Returns_Expected()
        {
            SomeDerivedClass derived = new();

            IEnumerable<SomeClass> sequence = Enumerables.Create(
                new SomeClass(),
                new SomeClass(),
                new SomeClass(),
                new SomeClass(),
                new SomeClass(),
                derived,
                new SomeClass(),
                new SomeClass(),
                new SomeClass()
            );

            SomeClass result = sequence.FindByType(typeof(SomeDerivedClass));
            Check.That(result).IsEqualTo(derived);
        }

        [Test]
        public void TryFindByType_Returns_Expected_When_Sequence_Contains_Element()
        {
            SomeDerivedClass derived = new();

            IEnumerable<SomeClass> sequence = Enumerables.Create(
                new SomeClass(),
                new SomeClass(),
                new SomeClass(),
                new SomeClass(),
                new SomeClass(),
                derived,
                new SomeClass(),
                new SomeClass(),
                new SomeClass()
            );

            bool result = sequence.TryFindByType(typeof(SomeDerivedClass), out SomeClass? elt);

            Check.That(result).IsTrue();
            Check.That(elt).IsEqualTo(derived);
        }

        [Test]
        public void TryFindByType_Returns_Expected_When_Sequence_Does_Not_Contain_Element()
        {
            SomeDerivedClass derived = new();

            IEnumerable<SomeClass> sequence = Enumerables.Create(
                new SomeClass(),
                new SomeClass(),
                new SomeClass()
            );

            bool result = sequence.TryFindByType(typeof(SomeDerivedClass), out SomeClass? elt);

            Check.That(result).IsFalse();
            Check.That(elt).IsNull();
        }
    }
}