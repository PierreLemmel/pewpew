using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.Tests.Helpers
{
    public class DictionariesShould
    {
        private const string CreateByInitializer = "CreatedInInitializer";

        [Test]
        public void GetOrInitialize_Should_Return_Value_And_Not_Call_Initializer_When_Key_Already_Exists()
        {
            string key = "key";
            string value = "value";

            IDictionary<string, string> dictionary = new Dictionary<string, string>()
            {
                [key] = value
            };

            bool triggered = false;
            string result = dictionary.GetOrInitialize(key, _ =>
            {
                triggered = true;
                
                return CreateByInitializer;
            });

            Check.That(triggered).IsFalse();
            Check.That(result).IsEqualTo(value);
        }

        [Test]
        public void GetOrInitialize_Should_Initialize_Value_Call_Initializer_And_Set_Dic_Value()
        {
            string key = "key";

            IDictionary<string, string> dictionary = new Dictionary<string, string>();

            bool triggered = false;
            string result = dictionary.GetOrInitialize(key, _ =>
            {
                triggered = true;
                return CreateByInitializer;
            });

            Check.That(triggered).IsTrue();
            Check.That(result).IsEqualTo(CreateByInitializer);
            Check.WithCustomMessage("Dictionary should contain key")
                .That(dictionary.ContainsKey(key)).IsTrue();
        }
    }
}