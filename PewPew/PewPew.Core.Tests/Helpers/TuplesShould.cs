using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.Tests.Helpers
{
    public class TuplesShould
    {
        [Test]
        public void Deconstruct_Deconstruct_Tuples_Into_Separated_Arrays()
        {
            (string, int)[] input = new (string, int)[]
            {
                ("Hello", 1),
                ("World", 2),
                ("!!!!", 4),
                ("olleH", 8),
                ("dlroW", 666),
            };

            string[] expectedArray1 = new string[]
            {
                "Hello",
                "World",
                "!!!!",
                "olleH",
                "dlroW",
            };

            int[] expectedArray2 = new int[]
            {
                1,
                2,
                4,
                8,
                666,
            };

            (string[] array1, int[] array2) = input.Deconstruct();

            Check.That(array1).IsEquivalentTo(expectedArray1);
            Check.That(array2).IsEquivalentTo(expectedArray2);
        }
    }
}