﻿using NFluent;
using NUnit.Framework;
using PewPew.Tests.SampleClasses;

namespace PewPew.Tests.Helpers
{
    public class EqualitiesShould
    {
        private const string StringYeah = "yeah";
        private const string StringNope = "nope";

        [Test]
        [TestCase(StringYeah, StringYeah)]
        [TestCase(StringYeah, StringYeah, StringNope)]
        [TestCase(StringYeah, StringNope, StringYeah, StringNope)]
        [TestCase(StringYeah, StringNope, StringNope, StringNope, StringYeah)]
        public void Return_True_When_String_Input_Is_One_Of_Arguments(string input, params string[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsTrue();
        }

        [Test]
        [TestCase(StringYeah, StringNope)]
        [TestCase(StringYeah, StringNope, StringNope)]
        [TestCase(StringYeah, StringNope, StringNope, StringNope)]
        [TestCase(StringYeah, StringNope, StringNope, StringNope, StringNope)]
        public void Return_False_When_String_Input_Is_Not_One_Of_Arguments(string input, params string[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsFalse();
        }

        private const int IntYeah = 14;
        private const int IntNope = 666;

        [Test]
        [TestCase(IntYeah, IntYeah)]
        [TestCase(IntYeah, IntYeah, IntNope)]
        [TestCase(IntYeah, IntNope, IntYeah, IntNope)]
        [TestCase(IntYeah, IntNope, IntNope, IntNope, IntYeah)]
        public void Return_True_When_Int_Input_Is_One_Of_Arguments(int input, params int[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsTrue();
        }

        [Test]
        [TestCase(IntYeah, IntNope)]
        [TestCase(IntYeah, IntNope, IntNope)]
        [TestCase(IntYeah, IntNope, IntNope, IntNope)]
        [TestCase(IntYeah, IntNope, IntNope, IntNope, IntNope)]
        public void Return_False_When_Int_Input_Is_Not_One_Of_Arguments(int input, params int[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsFalse();
        }

        private const uint UIntYeah = 14;
        private const uint UIntNope = 666;

        [Test]
        [TestCase(UIntYeah, UIntYeah)]
        [TestCase(UIntYeah, UIntYeah, UIntNope)]
        [TestCase(UIntYeah, UIntNope, UIntYeah, UIntNope)]
        [TestCase(UIntYeah, UIntNope, UIntNope, UIntNope, UIntYeah)]
        public void Return_True_When_UInt_Input_Is_One_Of_Arguments(uint input, params uint[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsTrue();
        }

        [Test]
        [TestCase(UIntYeah, UIntNope)]
        [TestCase(UIntYeah, UIntNope, UIntNope)]
        [TestCase(UIntYeah, UIntNope, UIntNope, UIntNope)]
        [TestCase(UIntYeah, UIntNope, UIntNope, UIntNope, UIntNope)]
        public void Return_False_When_UInt_Input_Is_Not_One_Of_Arguments(uint input, params uint[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsFalse();
        }

        private const long LongYeah = 14;
        private const long LongNope = 666;

        [Test]
        [TestCase(LongYeah, LongYeah)]
        [TestCase(LongYeah, LongYeah, LongNope)]
        [TestCase(LongYeah, LongNope, LongYeah, LongNope)]
        [TestCase(LongYeah, LongNope, LongNope, LongNope, LongYeah)]
        public void Return_True_When_Long_Input_Is_One_Of_Arguments(long input, params long[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsTrue();
        }

        [Test]
        [TestCase(LongYeah, LongNope)]
        [TestCase(LongYeah, LongNope, LongNope)]
        [TestCase(LongYeah, LongNope, LongNope, LongNope)]
        [TestCase(LongYeah, LongNope, LongNope, LongNope, LongNope)]
        public void Return_False_When_Long_Input_Is_Not_One_Of_Arguments(long input, params long[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsFalse();
        }

        private const ulong ULongYeah = 14;
        private const ulong ULongNope = 666;

        [Test]
        [TestCase(ULongYeah, ULongYeah)]
        [TestCase(ULongYeah, ULongYeah, ULongNope)]
        [TestCase(ULongYeah, ULongNope, ULongYeah, ULongNope)]
        [TestCase(ULongYeah, ULongNope, ULongNope, ULongNope, ULongYeah)]
        public void Return_True_When_ULong_Input_Is_One_Of_Arguments(ulong input, params ulong[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsTrue();
        }

        [Test]
        [TestCase(ULongYeah, ULongNope)]
        [TestCase(ULongYeah, ULongNope, ULongNope)]
        [TestCase(ULongYeah, ULongNope, ULongNope, ULongNope)]
        [TestCase(ULongYeah, ULongNope, ULongNope, ULongNope, ULongNope)]
        public void Return_False_When_ULong_Input_Is_Not_One_Of_Arguments(ulong input, params ulong[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsFalse();
        }

        private const float FloatYeah = 14;
        private const float FloatNope = 666;

        [Test]
        [TestCase(FloatYeah, FloatYeah)]
        [TestCase(FloatYeah, FloatYeah, FloatNope)]
        [TestCase(FloatYeah, FloatNope, FloatYeah, FloatNope)]
        [TestCase(FloatYeah, FloatNope, FloatNope, FloatNope, FloatYeah)]
        public void Return_True_When_Float_Input_Is_One_Of_Arguments(float input, params float[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsTrue();
        }

        [Test]
        [TestCase(FloatYeah, FloatNope)]
        [TestCase(FloatYeah, FloatNope, FloatNope)]
        [TestCase(FloatYeah, FloatNope, FloatNope, FloatNope)]
        [TestCase(FloatYeah, FloatNope, FloatNope, FloatNope, FloatNope)]
        public void Return_False_When_Float_Input_Is_Not_One_Of_Arguments(float input, params float[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsFalse();
        }

        private const double DoubleYeah = 14;
        private const double DoubleNope = 666;

        [Test]
        [TestCase(DoubleYeah, DoubleYeah)]
        [TestCase(DoubleYeah, DoubleYeah, DoubleNope)]
        [TestCase(DoubleYeah, DoubleNope, DoubleYeah, DoubleNope)]
        [TestCase(DoubleYeah, DoubleNope, DoubleNope, DoubleNope, DoubleYeah)]
        public void Return_True_When_Double_Input_Is_One_Of_Arguments(double input, params double[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsTrue();
        }

        [Test]
        [TestCase(DoubleYeah, DoubleNope)]
        [TestCase(DoubleYeah, DoubleNope, DoubleNope)]
        [TestCase(DoubleYeah, DoubleNope, DoubleNope, DoubleNope)]
        [TestCase(DoubleYeah, DoubleNope, DoubleNope, DoubleNope, DoubleNope)]
        public void Return_False_When_Double_Input_Is_Not_One_Of_Arguments(double input, params double[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsFalse();
        }

        private const SomeEnum SomeEnumYeah = SomeEnum.Yeah;
        private const SomeEnum SomeEnumNope = SomeEnum.Nope;

        [Test]
        [TestCase(SomeEnumYeah, SomeEnumYeah)]
        [TestCase(SomeEnumYeah, SomeEnumYeah, SomeEnumNope)]
        [TestCase(SomeEnumYeah, SomeEnumNope, SomeEnumYeah, SomeEnumNope)]
        [TestCase(SomeEnumYeah, SomeEnumNope, SomeEnumNope, SomeEnumNope, SomeEnumYeah)]
        public void Return_True_When_SomeEnum_Input_Is_One_Of_Arguments(SomeEnum input, params SomeEnum[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsTrue();
        }

        [Test]
        [TestCase(SomeEnumYeah, SomeEnumNope)]
        [TestCase(SomeEnumYeah, SomeEnumNope, SomeEnumNope)]
        [TestCase(SomeEnumYeah, SomeEnumNope, SomeEnumNope, SomeEnumNope)]
        [TestCase(SomeEnumYeah, SomeEnumNope, SomeEnumNope, SomeEnumNope, SomeEnumNope)]
        public void Return_False_When_SomeEnum_Input_Is_Not_One_Of_Arguments(SomeEnum input, params SomeEnum[] others)
        {
            bool result = input.IsOneOf(others);
            Check.That(result).IsFalse();
        }
    }
}
