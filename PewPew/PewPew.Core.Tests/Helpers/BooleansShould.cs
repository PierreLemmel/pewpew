using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace PewPew.Tests.Helpers
{
    public class BooleansShould
    {

        [Test]
        [TestCaseSource(nameof(OrTestCases))]
        public void Or_Returns_Expected_Result(bool[] input, bool expected) => Check.That(Booleans.Or(input)).IsEqualTo(expected);

        [Test]
        [TestCaseSource(nameof(AndTestCases))]
        public void And_Returns_Expected_Result(bool[] input, bool expected) => Check.That(Booleans.And(input)).IsEqualTo(expected);

        public static IEnumerable<object[]> OrTestCases => new object[][]
        {
            new object[] { new bool[] { }, true },

            new object[] { new bool[] { true }, true },
            new object[] { new bool[] { false }, false },

            new object[] { new bool[] { true, true }, true },
            new object[] { new bool[] { true, false }, true },
            new object[] { new bool[] { false, true }, true },
            new object[] { new bool[] { false, false }, false },

            new object[] { new bool[] { true, true, true }, true },
            new object[] { new bool[] { false, true, false }, true },
            new object[] { new bool[] { false, true, true }, true },
            new object[] { new bool[] { false, false, false }, false },

            new object[] { new bool[] { true, false, false, false, false, false, false }, true },
            new object[] { new bool[] { false, false, false, false, false, true, false }, true },
            new object[] { new bool[] { false, false, false, false, false, false, false }, false },
            new object[] { new bool[] { true, true, true, true, true, true, true, true }, true },
        };

        public static IEnumerable<object[]> AndTestCases => new object[][]
        {
            new object[] { new bool[] { }, true },

            new object[] { new bool[] { true }, true },
            new object[] { new bool[] { false }, false },

            new object[] { new bool[] { true, true }, true },
            new object[] { new bool[] { true, false }, false },
            new object[] { new bool[] { false, true }, false },
            new object[] { new bool[] { false, false }, false },

            new object[] { new bool[] { true, true, true }, true },
            new object[] { new bool[] { false, true, false }, false },
            new object[] { new bool[] { false, true, true }, false },
            new object[] { new bool[] { false, false, false }, false },

            new object[] { new bool[] { false, false, false, false, false, false, false }, false },
            new object[] { new bool[] { true, true, true, false, true, true, true, true }, false },
            new object[] { new bool[] { false, true, false, true, false, true, false }, false },
            new object[] { new bool[] { true, true, true, true, true, true, true, true }, true },
        };
    }
}