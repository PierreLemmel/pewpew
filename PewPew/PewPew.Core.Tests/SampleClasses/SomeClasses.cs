using System;

namespace PewPew.Tests.SampleClasses
{
    public class SomeClass { }
    public class SomeDerivedClass : SomeClass { }
}