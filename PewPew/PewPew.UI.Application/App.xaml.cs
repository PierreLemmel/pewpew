﻿using Autofac;
using PewPew.DI;
using PewPew.UI.DI;
using PewPew.UI.Events.Lifecycle;
using PewPew.UI.Orchestration;
using System.Windows;

namespace PewPew.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IContainer container;

        public App()
        {
            container = Bootstrap.BuildContainer();
            container.LoadStaticComponents();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            IEventSender<StartupEvent> startupSender = container.Resolve<IEventSender<StartupEvent>>();
            startupSender.SendEvent();

            MainWindow mainWindow = container.Resolve<MainWindow>();

            IControlOrchestrator orchestrator = container.Resolve<IControlOrchestrator>();
            orchestrator.StartOrchestration(mainWindow);

            mainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            IEventSender<ExitEvent> exitSender = container.Resolve<IEventSender<ExitEvent>>();
            exitSender.SendEvent();
        }

        // TEST visual components
        //protected override void OnStartup(StartupEventArgs e)
        //{
        //    base.OnStartup(e);

        //    Window window = new();

        //    Button left = new Button() { Background = Brushes.Blue };
        //    left.SizeChanged += (sender, e) => Log.Trace($"Left: {e.NewSize.Width}");
        //    Button right = new Button() { Background = Brushes.Red };
        //    right.SizeChanged += (sender, e) => Log.Trace($"Right: {e.NewSize.Width}");

        //    VerticallySplittedPanel vPanel = VerticallySplittedPanel.Create(left, right);

        //    window.Content = vPanel;

        //    window.Show();
        //}
    }
}