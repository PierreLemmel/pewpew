﻿using PewPew.Dmx.UI.Controls;
using PewPew.UI.Events;
using PewPew.UI.Menus;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI
{
    public partial class MainWindow : Window
    {
        private readonly IPewPewControlFactory controlFactory;

        public MainWindow(IViewMenuBuilder viewMenuBuilder, IPewPewControlFactory controlFactory)
        {
            InitializeComponent();

            viewMenuBuilder.BuildViewMenu(viewMenu);
            this.controlFactory = controlFactory;
        }
    }
}