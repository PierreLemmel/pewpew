using PewPew.DataAccess.Services;
using PewPew.Events.Handlers;
using PewPew.Lifecycle;
using PewPew.UI.Events.Lifecycle;

namespace PewPew.UI.EventHandlers
{
    internal class MigrateDbOnStartup : IEventHandler<StartupEvent>, IStaticComponent
    {
        private readonly IDbMigrator migrator;
        private readonly IHandlerRegistrar handlerRegistrar;

        public MigrateDbOnStartup(
            IDbMigrator migrator,
            IHandlerRegistrar handlerRegistrar)
        {
            this.migrator = migrator;
            this.handlerRegistrar = handlerRegistrar;

            handlerRegistrar.AddHandler(this);
        }

        public void Handle(StartupEvent @event)
        {
            Log.Info("Beginning of StartupDbMigration");

            Log.Trace("Checking for Db Creation");
            migrator.CreateDatabaseIfNeeded();
            Log.Trace("Db Creation checked");

            Log.Trace("Checking if database schema is up to date");
            migrator.CreateSchemaIfNeeded();
            Log.Trace("Database schema checked");

            Log.Info("End of StartupDbMigration");
        }

        public void Dispose() => handlerRegistrar.RemoveHandler(this);
    }
}