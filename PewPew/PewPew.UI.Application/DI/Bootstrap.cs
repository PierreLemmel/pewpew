﻿using Autofac;
using PewPew.Dmx.OpenDmx.DI;
using PewPew.Dmx.UI.DI;

namespace PewPew.UI.DI
{
    public static class Bootstrap
    {
        public static IContainer BuildContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

#if DEBUG
            string environment = "debug";
#elif RELEASE
            string environment = "release";
#else
#error Unexpected environment
#endif

            builder.Properties.Add("env", environment);

            builder.RegisterModule<OpenDmxModule>();
            builder.RegisterModule<DmxUIModule>();
            builder.RegisterModule<ApplicationModule>();

            IContainer container = builder.Build();

            return container;
        }
    }
}