﻿using Autofac;
using Autofac.Core;
using Autofac.Core.Registration;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using PewPew.DataAccess;
using PewPew.DataAccess.Events;
using PewPew.DataAccess.Models;
using PewPew.Events;
using PewPew.Lifecycle;
using PewPew.Reflection;
using PewPew.Scanning;
using PewPew.UI.EventHandlers;
using PewPew.UI.Events;
using PewPew.UI.Events.User;
using PewPew.UI.Factories;
using PewPew.UI.Orchestration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace PewPew.UI.DI
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            ValidatorOptions.Global.LanguageManager.Culture = CultureInfo.InvariantCulture;

            builder.AddDataAccess();
            builder.AddEvents();

            builder.AddStaticEventHandler<MigrateDbOnStartup>();

            builder.RegisterType<MainWindow>().SingleInstance();
            builder.AddControlOrchestraction();

            RegisterEventSenders(builder);

            builder.AddObservableDatasets();
            builder.AddMenuBuilders();
            builder.AddDialogFactory();
            builder.AddControlFactory();

            SetupConfiguration(builder);
        }

        private void RegisterEventSenders(ContainerBuilder builder)
        {
            IEnumerable<Type> entityModels = Scan
                .PewPewClasses
                .InheritingFrom<EntityModel>();

            IEnumerable<GenericTypeMap> dataSetChangedEventsTypeMaps = Scan
                .PewPewClasses
                .InheritingFrom(typeof(DataSetChanged<>), canBeEqualToClassType: true)
                .GenericTypeDefinitions()
                .Select(typeDef => new GenericTypeMap(
                    typeDef,
                    entityModels)
                );


            IEnumerable<Type> pewPewControls = Scan
                .PewPewUIClasses
                .InheritingFrom<PewPewControl>();

            IEnumerable<GenericTypeMap> userControlRequestEvents = Scan
                .PewPewUIClasses
                .InheritingFrom<UserControlRequestEvent>()
                .GenericTypeDefinitions()
                .Select(typeDef => new GenericTypeMap(
                    typeDef,
                    pewPewControls)
                );


            IEnumerable<GenericTypeMap> eventTypeMaps = Enumerables.Merge(
                dataSetChangedEventsTypeMaps,
                userControlRequestEvents
            );

            

            IReadOnlyCollection<Type> eventTypes = Scan
                .PewPewClasses
                .InheritingFrom<Event>()
                .MapGenericDefinitions(eventTypeMaps)
                .ToList();

            IEnumerable<Type> eventSenders = eventTypes
                .MakeGenericsFor(typeof(IEventSender<>));

            builder.Register(ctx => ctx.Resolve<IEventSender<Event>>())
                .As(eventSenders);
        }

        private void SetupConfiguration(ContainerBuilder builder)
        {
            string environment = builder.Properties.Get<string>("env");

            IConfigurationBuilder cfgBuilder = new ConfigurationBuilder();

            cfgBuilder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            cfgBuilder.AddJsonFile($"appsettings.{environment}.json", optional: false, reloadOnChange: true);

            IConfiguration config = cfgBuilder.Build();

            builder.RegisterInstance(config);

            builder.RegisterConfigProvider<DataAccessOptions>("DataAccess");
        }
    }
}