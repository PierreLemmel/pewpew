﻿using Autofac;
using Microsoft.Extensions.Configuration;
using NFluent;
using NUnit.Framework;
using PewPew.Configuration;
using PewPew.DataAccess.Models;
using PewPew.DataAccess.Services;
using PewPew.DataMigration;
using System;
using System.Collections.Generic;

namespace PewPew.UI.Application.Tests.DI
{
    public class ContainerShould
    {
        [Test]
        public void Build_Without_Throwing()
        {
            IContainer? container = default;
            Check.ThatCode(() => container = Bootstrap.BuildContainer())
                .DoesNotThrow();

            Check.That(container).IsNotNull();
        }

        [Test]
        [GenericTestCaseSource(nameof(DataMigrationTypes))]
        public void Resolve_Service<TService>() where TService : class
        {
            IContainer container = Bootstrap.BuildContainer();

            TService service = container.Resolve<TService>();

            Check.That(service).IsNotNull();
        }

        public static IEnumerable<Type> DataMigrationTypes => new Type[]
        {
            typeof(IConfiguration),
            typeof(IConfigurationProvider<DataAccessOptions>),
            typeof(IDbMigrator),
        };

    }
}