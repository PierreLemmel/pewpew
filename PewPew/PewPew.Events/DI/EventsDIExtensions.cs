using Autofac;
using Autofac.Builder;
using PewPew.Events.Dispatchers;
using PewPew.Events.Handlers;
using PewPew.Events.Senders;
using PewPew.Lifecycle;

namespace PewPew.Events
{
    public static class EventsDIExtensions
    {
        public static void AddEvents(this ContainerBuilder builder)
        {
            builder.RegisterType<PewPewEventDispatcher>()
                .As<IEventDispatcher>()
                .As<IHandlerRegistrar>()
                .SingleInstance();

            builder.RegisterType<PewPewEventSender>().As<IEventSender<Event>>();
        }

        public static IRegistrationBuilder<THandler, ConcreteReflectionActivatorData, SingleRegistrationStyle> AddStaticEventHandler<THandler>(this ContainerBuilder builder)
            where THandler : IEventHandler, IStaticComponent
            => builder.AddStaticComponent<THandler>().As<IEventHandler>();
    }
}