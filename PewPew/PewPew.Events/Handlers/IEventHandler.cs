﻿using System;

namespace PewPew
{
    public interface IEventHandler : IDisposable
    {
        
    }
    public interface IEventHandler<in TEvent> : IEventHandler where TEvent : Event
    {
        void Handle(TEvent @event);
    }
}