using System;

namespace PewPew.Events.Handlers
{
    public interface IHandlerRegistrar
    {
        void AddHandler(IEventHandler handler);
        void RemoveHandler(IEventHandler handler);

        bool HasRegistered(IEventHandler handler);
    }
}