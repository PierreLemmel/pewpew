namespace PewPew
{
    public interface IEventSender<in TEvent> where TEvent : Event
    {
        void SendEvent<TSent>(TSent @event) where TSent : TEvent;
    }
}