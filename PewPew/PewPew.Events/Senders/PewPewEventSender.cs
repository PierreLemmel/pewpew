using PewPew.Events.Dispatchers;

namespace PewPew.Events.Senders
{
    internal class PewPewEventSender : IEventSender<Event>
    {
        private readonly IEventDispatcher dispatcher;

        public PewPewEventSender(IEventDispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
        }

        public void SendEvent<TSent>(TSent @event) where TSent : Event => dispatcher.DispatchEvent(@event);
    }
}