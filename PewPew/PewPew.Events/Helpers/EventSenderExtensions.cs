namespace PewPew
{
    public static class EventSenderExtensions
    {
        public static void SendEvent<TEvent>(this IEventSender<TEvent> sender)
            where TEvent : Event, new() => sender.SendEvent<TEvent>(new ());
    }
}