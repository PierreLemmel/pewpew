using Newtonsoft.Json;
using System;

namespace PewPew
{
    public abstract class Event
    {
        [JsonIgnore]
        public DateTime DateTime { get; } = DateTime.Now;

        [JsonIgnore]
        public virtual string Name => GetType().GetGenericName().RemoveEnd("Event");

        [JsonIgnore]
        public Guid SessionId => sessionId;

        private static readonly Guid sessionId = Guid.NewGuid();
    }
}