using PewPew.Events.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.Events.Dispatchers
{
    internal sealed class PewPewEventDispatcher : IEventDispatcher, IHandlerRegistrar
    {
        private readonly HashSet<IEventHandler> registeredHandlers;
        private readonly IDictionary<Type, ICollection<IEventHandler>> handlersCache;

        public PewPewEventDispatcher()
        {
            registeredHandlers = new();
            handlersCache = new Dictionary<Type, ICollection<IEventHandler>>();

            BuildCache();
        }

        private void RebuildCache()
        {
            handlersCache.Clear();
            BuildCache();
        }

        private void BuildCache()
        {
            foreach (IEventHandler eventHandler in registeredHandlers)
            {
                AddHandlerToCache(eventHandler);
            }
        }

        private void AddHandlerToCache(IEventHandler eventHandler)
        {
            IEnumerable<Type> handlersTypes = eventHandler
                                .GetType()
                                .GetGenericVersionsOfInterface(typeof(IEventHandler<>));

            foreach (Type handlerType in handlersTypes)
            {
                Type handledType = handlerType
                    .GetGenericArguments()
                    .Single();

                handlersCache
                    .GetOrInitialize(handledType, () => new List<IEventHandler>())
                    .Add(eventHandler);
            }
        }

        public void DispatchEvent<TEvent>(TEvent @event) where TEvent : Event
        {
            Type eventType = typeof(TEvent);

            do
            {
                if (handlersCache.TryGetValue(eventType, out ICollection<IEventHandler>? handlers))
                {
                    IEnumerable<IEventHandler<TEvent>> typedHandlers = handlers.Cast<IEventHandler<TEvent>>();

                    foreach (IEventHandler<TEvent> typedHandler in typedHandlers)
                        typedHandler.Handle(@event);
                }

                eventType = eventType.BaseType!;
            }
            while (eventType != typeof(object));
        }

        public void AddHandler(IEventHandler handler)
        {
            if (registeredHandlers.Add(handler))
                AddHandlerToCache(handler);
            else
                throw new InvalidOperationException("Impossible to add handler: this handler already exists in dispatcher");
        }

        public void RemoveHandler(IEventHandler handler)
        {
            if (registeredHandlers.Remove(handler))
                RebuildCache();
            else
                throw new InvalidOperationException("Impossible to remove handler: this handler does not exist in dispatcher");
        }

        public bool HasRegistered(IEventHandler handler) => registeredHandlers.Contains(handler);
    }
}