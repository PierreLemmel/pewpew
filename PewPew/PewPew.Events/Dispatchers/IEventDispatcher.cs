namespace PewPew.Events.Dispatchers
{
    public interface IEventDispatcher
    {
        void DispatchEvent<TEvent>(TEvent @event) where TEvent : Event;
    }
}