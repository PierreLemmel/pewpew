using NFluent;
using NUnit.Framework;
using PewPew.Events.Handlers;
using PewPew.Scanning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PewPew.Idiomatic.Tests.Events
{
    public class EventHandlersShould
    {
        public static IEnumerable<object[]> EventHandlerConstructors => Scan
            .PewPewClasses
            .Implementing<IEventHandler>()
            .SelectMany(handlerType => handlerType.GetConstructors())
            .Select(constructor => new object[] { constructor.DeclaringType!, constructor });

        [Test]
        [TestCaseSource(nameof(EventHandlerConstructors))]
        public void Include_An_Handler_Registrar_In_Constructor(Type handlerType, ConstructorInfo constructor)
        {
            bool hasRegistrarInConstructor = constructor
                .GetParameters()
                .Any(param => param.ParameterType == typeof(IHandlerRegistrar));

            Check
                .WithCustomMessage($"Constructor on type {handlerType.Name} is missing {nameof(IHandlerRegistrar)} in constructor")
                .That(hasRegistrarInConstructor)
                .IsTrue();
        }
    }
}