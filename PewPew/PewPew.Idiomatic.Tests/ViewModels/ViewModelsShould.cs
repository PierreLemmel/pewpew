﻿using AutoFixture;
using AutoFixture.Kernel;
using NFluent;
using NUnit.Framework;
using PewPew.Scanning;
using PewPew.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Input;

namespace PewPew.Idiomatic.Tests.ViewModels
{
    public class ViewModelsShould
    {
        #region Sources
        private static IEnumerable<Type> ViewModelTypes => Scan
            .PewPewTypes
            .ConcreteTypes()
            .Implementing<IViewModel>();

        private static IEnumerable<object[]> ViewModelProperties => ViewModelTypes
            .SelectMany(type => type.GetProperties())
            .GetSetProperties()
            .NotImplementing<ICommand>()
            .NotOfType<DialogResult>()
            .Select(prop => new object[] { prop.DeclaringType!, prop });
        #endregion

        private IFixture fixture;

        public ViewModelsShould()
        {
            fixture = new Fixture().Customize<OmitInterfacesInConstructor>();

            foreach (Type dialogViewModelType in ViewModelTypes.Implementing<IDialogViewModel>())
            {
                fixture.Customizations.Add(
                new TypeRelay(
                    typeof(DialogViewModel<>).MakeGenericType(dialogViewModelType),
                    dialogViewModelType));
            }
        }



        #region ViewModel Properties
        [Test]
        [TestCaseSource(nameof(ViewModelProperties))]
        public void ViewModelProperties_Should_Raise_PropertyChanged_Event_With_Correct_Property_Name_When_Modified(Type type, PropertyInfo property)
        {
            Type propType = property.PropertyType;

            IViewModel vm = (IViewModel)fixture.Create(type);

            bool triggered = false;
            string? propName = default;
            vm.PropertyChanged += (_, e) =>
            {
                triggered = true;
                propName = e.PropertyName;
            };

            object? propValue = property.GetValue(vm);

            object newValue = fixture.CreateDifferent(property.PropertyType, propValue);

            property.SetValue(vm, newValue);

            Check.That(triggered).IsTrue();
            Check.That(propName).IsEqualTo(property.Name);
        }

        [Test]
        [TestCaseSource(nameof(ViewModelProperties))]
        public void ViewModelProperties_Should_Not_Raise_PropertyChanged_Event_When_Assigned_Same_Value(Type type, PropertyInfo property)
        {
            Type propType = property.PropertyType;

            IViewModel vm = (IViewModel)fixture.Create(type);

            bool triggered = false;
            vm.PropertyChanged += (_, e) => triggered = true;

            object oldValue = property.GetValue(vm)!;
            property.SetValue(vm, oldValue);

            Check.That(triggered).IsFalse();
        } 
        #endregion
    }
}