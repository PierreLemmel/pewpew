﻿using AutoFixture;
using NFluent;
using NUnit.Framework;
using PewPew.Scanning;
using System;
using System.Collections.Generic;

namespace PewPew.Idiomatic.Tests.Models
{
    public class EntityModelsShould
    {
        public static IEnumerable<Type> EntityModelTypes => Scan
            .PewPewTypes
            .InheritingFrom<EntityModel>();

        private readonly Fixture fixture = new Fixture();

        [Test]
        [GenericTestCaseSource(nameof(EntityModelTypes))]
        public void Can_Be_Serialized_In_Json<TModel>() where TModel : EntityModel
        {
            TModel model = fixture.Create<TModel>();
            string json = Json.Serialize(model);

            Check.That(json).IsNotNullOrEmpty();
        }

        [Test]
        [GenericTestCaseSource(nameof(EntityModelTypes))]
        public void Can_Be_Deserialized_From_Json<TModel>() where TModel : EntityModel
        {
            string json = fixture.CreateJson<TModel>();
            TModel model = Json.Deserialize<TModel>(json);

            Check.That(model).IsNotNull();
        }
    }
}