using NFluent;
using NUnit.Framework;
using PewPew.Events.Dispatchers;
using PewPew.Events.Tests.SampleClasses.Events;
using PewPew.Events.Tests.SampleClasses.Handlers;
using System;
using System.Collections.Generic;

namespace PewPew.Events.Tests.Dispatchers
{
    public class PewPewEventDispatcherShould
    {
        [Test]
        public void Trigger_Handler_Of_Same_Type()
        {
            bool triggered = false;

            PewPewEventDispatcher dispatcher = new();
            dispatcher.AddHandler(new ActionEventHandler<SomeEvent>(se => triggered = true));

            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggered).IsTrue();
        }

        [Test]
        public void Does_Not_Trigger_Handler_Of_Others_Type()
        {
            bool triggered = false;

            PewPewEventDispatcher dispatcher = new();
            dispatcher.AddHandler(new ActionEventHandler<SomeEvent>(se => triggered = true));

            dispatcher.DispatchEvent(new SomeOtherEvent());

            Check.That(triggered).IsFalse();
        }

        [Test]
        public void Does_Not_Trigger_Other_Handler_Types()
        {
            bool triggeredSomeEvent = false;
            bool triggeredSomeOtherEvent = false;

            PewPewEventDispatcher dispatcher = new();
            dispatcher.AddHandler(new ActionEventHandler<SomeEvent>(se => triggeredSomeEvent = true));
            dispatcher.AddHandler(new ActionEventHandler<SomeOtherEvent>(se => triggeredSomeOtherEvent = true));

            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggeredSomeEvent).IsTrue();
            Check.That(triggeredSomeOtherEvent).IsFalse();
        }

        [Test]
        public void Trigger_Multiple_Handlers_Of_Same_Type()
        {
            bool triggered1 = false;
            bool triggered2 = false;
            bool triggered3 = false;

            PewPewEventDispatcher dispatcher = new();
            dispatcher.AddHandler(new ActionEventHandler<SomeEvent>(se => triggered1 = true));
            dispatcher.AddHandler(new ActionEventHandler<SomeEvent>(se => triggered2 = true));
            dispatcher.AddHandler(new ActionEventHandler<SomeEvent>(se => triggered3 = true));

            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggered1).IsTrue();
            Check.That(triggered2).IsTrue();
            Check.That(triggered3).IsTrue();
        }

        [Test]
        public void Trigger_Handler_Only_One_Time()
        {
            int triggerCount = 0;

            PewPewEventDispatcher dispatcher = new();
            dispatcher.AddHandler(new ActionEventHandler<SomeEvent>(se => triggerCount++));

            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggerCount).IsEqualTo(1);
        }

        [Test]
        public void Trigger_Base_Class_Handler()
        {
            bool triggered = false;

            PewPewEventDispatcher dispatcher = new();
            dispatcher.AddHandler(new ActionEventHandler<Event>(e => triggered = true));

            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggered).IsTrue();
        }

        [Test]
        public void Trigger_Base_Class_Only_Once()
        {
            int triggerCount = 0;

            PewPewEventDispatcher dispatcher = new();
            dispatcher.AddHandler(new ActionEventHandler<Event>(e => triggerCount++));

            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggerCount).IsEqualTo(1);
        }

        [Test]
        public void Trigger_Base_Class_With_All_Parent_Classes()
        {
            int triggerCount = 0;

            PewPewEventDispatcher dispatcher = new();
            dispatcher.AddHandler(new ActionEventHandler<Event>(e => triggerCount++));

            dispatcher.DispatchEvent(new SomeEvent());
            dispatcher.DispatchEvent(new SomeOtherEvent());

            Check.That(triggerCount).IsEqualTo(2);
        }

        [Test]
        public void Accept_Some_Composite_Handlers()
        {
            PewPewEventDispatcher dispatcher = new();

            CompositeHandler composite = new CompositeHandler(Actions.Empty<SomeEvent>(), Actions.Empty<SomeOtherEvent>());

            Check.ThatCode(() => dispatcher.AddHandler(composite))
                .DoesNotThrow();
        }

        [Test]
        public void Trigger_ChildHandlers_From_Composite_Handlers()
        {
            PewPewEventDispatcher dispatcher = new();

            bool triggered1 = false;
            bool triggered2 = false;

            dispatcher.AddHandler(new CompositeHandler(
                e => triggered1 = true,
                e => triggered2 = true
            ));

            Check.That(triggered1).IsFalse();
            Check.That(triggered2).IsFalse();

            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggered1).IsTrue();
            Check.That(triggered2).IsFalse();

            dispatcher.DispatchEvent(new SomeOtherEvent());

            Check.That(triggered1).IsTrue();
            Check.That(triggered2).IsTrue();
        }
         
        [Test]
        public void Does_Not_Trigger_Removed_Handlers()
        {
            PewPewEventDispatcher dispatcher = new();

            int triggerCount = 0;
            IEventHandler handler = new ActionEventHandler<SomeEvent>(e => triggerCount++);

            dispatcher.AddHandler(handler);

            dispatcher.DispatchEvent(new SomeEvent());
            Check.That(triggerCount).IsEqualTo(1);

            dispatcher.RemoveHandler(handler);
            dispatcher.DispatchEvent(new SomeEvent());
            Check.That(triggerCount).IsEqualTo(1);
        }

        [Test]
        public void Removing_A_Handler_Still_Triggers_Removed_Handlers()
        {
            PewPewEventDispatcher dispatcher = new();

            int triggerCount1 = 0;
            int triggerCount2 = 0;
            IEventHandler handler1 = new ActionEventHandler<SomeEvent>(e => triggerCount1++);
            IEventHandler handler2 = new ActionEventHandler<SomeEvent>(e => triggerCount2++);

            dispatcher.AddHandler(handler1);
            dispatcher.AddHandler(handler2);

            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggerCount1).IsEqualTo(1);
            Check.That(triggerCount2).IsEqualTo(1);

            dispatcher.RemoveHandler(handler1);
            dispatcher.DispatchEvent(new SomeEvent());

            Check.That(triggerCount1).IsEqualTo(1);
            Check.That(triggerCount2).IsEqualTo(2);
        }

        [Test]
        public void Removing_A_Composite_Handler_Remove_All_Child_Handlers()
        {
            PewPewEventDispatcher dispatcher = new();

            int triggerCount1 = 0;
            int triggerCount2 = 0;

            IEventHandler composite = new CompositeHandler(
                e => triggerCount1++,
                e => triggerCount2++);

            dispatcher.AddHandler(composite);

            dispatcher.DispatchEvent(new SomeEvent());
            dispatcher.DispatchEvent(new SomeEvent());
            dispatcher.DispatchEvent(new SomeOtherEvent());

            Check.That(triggerCount1).IsEqualTo(2);
            Check.That(triggerCount2).IsEqualTo(1);

            dispatcher.RemoveHandler(composite);

            dispatcher.DispatchEvent(new SomeEvent());
            dispatcher.DispatchEvent(new SomeEvent());
            dispatcher.DispatchEvent(new SomeOtherEvent());

            Check.That(triggerCount1).IsEqualTo(2);
            Check.That(triggerCount2).IsEqualTo(1);
        }

        [Test]
        public void Cant_Add_Multiple_Times_Same_Handler()
        {
            PewPewEventDispatcher dispatcher = new();
            IEventHandler handler = new ActionEventHandler<SomeEvent>(Actions.Empty<SomeEvent>());

            dispatcher.AddHandler(handler);

            Check.ThatCode(() => dispatcher.AddHandler(handler))
                .Throws<InvalidOperationException>();
        }

        [Test]
        public void Cant_Remove_Non_Existing_Handler()
        {
            PewPewEventDispatcher dispatcher = new();
            IEventHandler handler = new ActionEventHandler<SomeEvent>(Actions.Empty<SomeEvent>());

            Check.ThatCode(() => dispatcher.RemoveHandler(handler))
                .Throws<InvalidOperationException>();
        }
    }
}