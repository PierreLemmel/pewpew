using System;

namespace PewPew.Events.Tests.SampleClasses.Handlers
{
    public class ActionEventHandler<TEvent> : IEventHandler<TEvent> where TEvent : Event
    {
        private readonly Action<TEvent> action;

        public ActionEventHandler(Action<TEvent> action)
        {
            this.action = action;
        }

        public void Handle(TEvent @event) => action(@event);

        public void Dispose() { }
    }
}