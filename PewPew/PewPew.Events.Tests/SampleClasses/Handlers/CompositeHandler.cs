using PewPew.Events.Tests.SampleClasses.Events;
using System;

namespace PewPew.Events.Tests.SampleClasses.Handlers
{
    public class CompositeHandler : IEventHandler<SomeEvent>, IEventHandler<SomeOtherEvent>
    {
        private readonly Action<SomeEvent> someEventHandler;
        private readonly Action<SomeOtherEvent> someOtherEventHandler;

        public CompositeHandler(Action<SomeEvent> someEventHandler, Action<SomeOtherEvent> someOtherEventHandler)
        {
            this.someEventHandler = someEventHandler;
            this.someOtherEventHandler = someOtherEventHandler;
        }

        public void Handle(SomeEvent @event) => someEventHandler(@event);
        public void Handle(SomeOtherEvent @event) => someOtherEventHandler(@event);

        public void Dispose() { }
    }
}