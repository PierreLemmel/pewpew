using PewPew.Events.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.Fakes
{
    public class FakeRegistrar : IHandlerRegistrar
    {
        private readonly HashSet<IEventHandler> register = new();

        public void AddHandler(IEventHandler handler) => register.Add(handler);
        public bool HasRegistered(IEventHandler handler) => register.Contains(handler);
        public void RemoveHandler(IEventHandler handler) => register.Remove(handler);
    }
}