using PewPew.Configuration;
using System;

namespace PewPew.Fakes
{
    public class FakeConfigurationProvider<TConfig> : IConfigurationProvider<TConfig> where TConfig : new()
    {
        public TConfig Configuration { get; }

        public FakeConfigurationProvider(TConfig value) => Configuration = value;
        public FakeConfigurationProvider() : this(new()) { }
    }
}