﻿using FluentValidation;

namespace PewPew.Fakes
{
    public class FakeValidator<T> : AbstractValidator<T> { }
}