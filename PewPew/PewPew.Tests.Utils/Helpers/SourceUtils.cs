﻿using System.Collections.Generic;
using System.Linq;

namespace PewPew
{
    public static class SourceUtils
    {
        public static IEnumerable<object[]> ToObjectSource<T1, T2>(this IEnumerable<(T1 t1, T2 t2)> tupleSource) => tupleSource.Select(tuple => new object[] { tuple.t1, tuple.t2 });
        public static IEnumerable<object[]> ToObjectSource<T1, T2, T3>(this IEnumerable<(T1 t1, T2 t2, T3 t3)> tupleSource) => tupleSource.Select(tuple => new object[] { tuple.t1, tuple.t2, tuple.t3 });
        public static IEnumerable<object[]> ToObjectSource<T1, T2, T3, T4>(this IEnumerable<(T1 t1, T2 t2, T3 t3, T4 t4)> tupleSource) => tupleSource.Select(tuple => new object[] { tuple.t1, tuple.t2, tuple.t3, tuple.t4 });
        public static IEnumerable<object[]> ToObjectSource<T1, T2, T3, T4, T5>(this IEnumerable<(T1 t1, T2 t2, T3 t3, T4 t4, T5 t5)> tupleSource) => tupleSource.Select(tuple => new object[] { tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5 });
        public static IEnumerable<object[]> ToObjectSource<T1, T2, T3, T4, T5, T6>(this IEnumerable<(T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6)> tupleSource) => tupleSource.Select(tuple => new object[] { tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6 });
        public static IEnumerable<object[]> ToObjectSource<T1, T2, T3, T4, T5, T6, T7>(this IEnumerable<(T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7)> tupleSource) => tupleSource.Select(tuple => new object[] { tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7 });
        public static IEnumerable<object[]> ToObjectSource<T1, T2, T3, T4, T5, T6, T7, T8>(this IEnumerable<(T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8)> tupleSource) => tupleSource.Select(tuple => new object[] { tuple.t1, tuple.t2, tuple.t3, tuple.t4, tuple.t5, tuple.t6, tuple.t7, tuple.t8 });
    }
}