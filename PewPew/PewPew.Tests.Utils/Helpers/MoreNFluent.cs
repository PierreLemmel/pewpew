﻿using FluentValidation.Results;
using NFluent;
using NFluent.Extensibility;
using PewPew.Events.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew
{
    public static class MoreNFluent
    {
        public static ICheckLink<ICheck<string>> IsNotNullOrEmpty(this ICheck<string> checker) => checker.IsNotNull().And.IsNotEmpty();

        public static ICheck<RunTrace> DoesNotThrow<TException>(this ICheck<RunTrace> check) where TException : Exception
        {
            check.Not.Throws<TException>();

            return check;
        }

        public static ICheckLink<ICheck<ValidationResult>> IsValid(this ICheck<ValidationResult> check)
        {
            ExtensibilityHelper.BeginCheck(check)
                .FailWhen(result => !result.IsValid, "Validation result is invalid")
                .OnNegate("Validation result is valid")
                .EndCheck();

            return ExtensibilityHelper.BuildCheckLink(check);
        }

        public static ICheckLink<ICheck<ValidationResult>> IsInvalid(this ICheck<ValidationResult> check)
        {
            ExtensibilityHelper.BeginCheck(check)
                .FailWhen(result => result.IsValid, "Validation result is valid")
                .OnNegate("Validation result is invalid")
                .EndCheck();

            return ExtensibilityHelper.BuildCheckLink(check);
        }

        public static ICheckLink<ICheck<IEventHandler>> IsRegisteredIn(this ICheck<IEventHandler> check, IHandlerRegistrar registrar)
        {
            ExtensibilityHelper.BeginCheck(check)
                .FailWhen(handler => !registrar.HasRegistered(handler), "Handler has not been registered.")
                .OnNegate("Handler has been registered.")
                .EndCheck();

            return ExtensibilityHelper.BuildCheckLink(check);
        }

        public static ICheckLink<ICheck<IEventHandler>> IsNotRegisteredIn(this ICheck<IEventHandler> check, IHandlerRegistrar registrar)
        {
            ExtensibilityHelper.BeginCheck(check)
                .FailWhen(handler => registrar.HasRegistered(handler), "Handler has been registered.")
                .OnNegate("Handler has not been registered.")
                .EndCheck();

            return ExtensibilityHelper.BuildCheckLink(check);
        }

        public static ICheckLink<ICheck<IEventHandler>> UnregistersOnDispose(this ICheck<IEventHandler> check, IHandlerRegistrar registrar)
        {
            ExtensibilityHelper.BeginCheck(check)
                .FailWhen(handler => registrar.HasRegistered(handler), "Handler has been unregistered.")
                .OnNegate("Handler has not been registered.")
                .EndCheck();

            return ExtensibilityHelper.BuildCheckLink(check);
        }

        public static ICheckLink<ICheck<IEnumerable<T>>> All<T>(this ICheck<IEnumerable<T>> check, Func<T, bool> predicate)
        {
            ExtensibilityHelper.BeginCheck(check)
                .FailWhen(sequence => !sequence.All(predicate), "Some elements in sequence don't match predicate.")
                .OnNegate("Some elements match predicate while they shouldn't.")
                .EndCheck();

            return ExtensibilityHelper.BuildCheckLink(check);
        }
    }
}