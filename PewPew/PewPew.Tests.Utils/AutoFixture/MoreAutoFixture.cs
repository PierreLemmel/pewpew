﻿using AutoFixture;
using AutoFixture.Kernel;
using System;
using System.Text.Json;

namespace PewPew
{
    public static class MoreAutoFixture
    {
        public static object Create(this IFixture fixture, Type type) => new SpecimenContext(fixture).Resolve(type);

        public static object CreateDifferent(this IFixture fixture, Type type, object other)
        {
            SpecimenContext ctx = new SpecimenContext(fixture);

            object result;

            do
            {
                result = ctx.Resolve(type);
            }
            while (object.Equals(result, other));

            return result;
        }

        public static T CreateDifferent<T>(this IFixture fixture, T other)
        {
            T result;

            do
            {
                result = fixture.Create<T>();
            }
            while (object.Equals(result, other));

            return result;
        }

        public static string CreateJson<T>(this IFixture fixture)
        {
            T obj = fixture.Create<T>();
            string json = JsonSerializer.Serialize(obj);
            return json;
        }

        public static IFixture Customize<TSpecimen>(this IFixture fixture) where TSpecimen : ISpecimenBuilder, new()
            => fixture.Customize(new TSpecimen().ToCustomization());
    }
}