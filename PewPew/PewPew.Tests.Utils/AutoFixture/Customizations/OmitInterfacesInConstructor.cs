using AutoFixture.Kernel;
using System.Reflection;

namespace PewPew
{
    public class OmitInterfacesInConstructor : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            if (request is ParameterInfo parameter)
            {
                if (parameter.ParameterType.IsInterface)
                    return null;
            }

            return new NoSpecimen();
        }
    }
}