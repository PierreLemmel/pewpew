﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("PewPew.DataAccess.Tests")]
[assembly: InternalsVisibleTo("PewPew.UI.Tests")]
[assembly: InternalsVisibleTo("PewPew.Idiomatic.Tests")]