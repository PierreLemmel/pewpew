﻿using Autofac;
using PewPew.DataAccess.Services;
using PewPew.Events;

namespace PewPew.DataAccess
{
    public static class DataAccessDIExtensions
    {
        public static void AddDataAccess(this ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IModelReader<>))
                .As(typeof(IModelWriter<>));

            builder.RegisterType<DbMigrator>().As<IDbMigrator>();

            builder.AddStaticEventHandler<EventSourcer>();
        }
    }
}