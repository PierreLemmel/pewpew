using Dapper;
using PewPew.Configuration;
using PewPew.DataAccess.Contracts;
using PewPew.DataAccess.Models;
using PewPew.Events.Handlers;
using PewPew.Lifecycle;
using System.Data.SQLite;

namespace PewPew.DataAccess.Services
{
    internal class EventSourcer : DbConsummer, IEventHandler<Event>, IStaticComponent
    {
        private readonly IHandlerRegistrar handlerRegistrar;

        public EventSourcer(
            IConfigurationProvider<DataAccessOptions> optionsProvider,
            IHandlerRegistrar handlerRegistrar)
            : base(optionsProvider)
        {
            this.handlerRegistrar = handlerRegistrar;

            handlerRegistrar.AddHandler(this);
        }

        public void Handle(Event @event)
        {
            string dateTime = @event.DateTime.ToString(EventDbModel.DateFormat);
            string eventType = @event.Name;
            string data = Json.Serialize(@event);
            string sessionId = @event.SessionId.ToString();

            EventDbModel eventDbModel = new(dateTime, eventType, data, sessionId);

            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            connection.Execute(Queries.AddEvent, eventDbModel);
        }

        public void Dispose() => handlerRegistrar.RemoveHandler(this);

        private static class Queries
        {
            public static string AddEvent => "INSERT INTO Events (DateTime, EventType, Data, SessionId) VALUES (@DateTime, @EventType, @Data, @SessionId)";
        }
    }
}