﻿using System;
using System.Collections.Generic;

namespace PewPew.DataAccess
{
    public interface IModelReader<TModel> where TModel : EntityModel
    {
        TModel GetById(Guid id);
        TModel GetByName(string name);
        IReadOnlyCollection<TModel> GetRange(IEnumerable<Guid> ids);
        IReadOnlyCollection<TModel> GetAll();
    }
}