﻿using System;
using System.Collections.Generic;

namespace PewPew.DataAccess
{
    public interface IModelWriter<TModel> where TModel : EntityModel
    {
        void Delete(Guid id);
        void DeleteRange(IEnumerable<Guid> ids);

        void Add(TModel elt);
        void AddRange(IEnumerable<TModel> elts);

        void Update(TModel elt);
        void UpdateRange(IEnumerable<TModel> elts);

        void Clear();
    }
}