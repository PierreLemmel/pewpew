﻿using Dapper;
using PewPew.Configuration;
using PewPew.DataAccess.Models;
using System.Data.SQLite;
using System.IO;

namespace PewPew.DataAccess.Services
{
    internal class DbMigrator : DbConsummer, IDbMigrator
    {

        public DbMigrator(IConfigurationProvider<DataAccessOptions> optionsProvider) : base(optionsProvider) { }

        public bool CreateDatabaseIfNeeded()
        {
            bool createDb = !File.Exists(dbPath);

            if (createDb)
            {
                Log.Info($"Creating database '{dbPath}'");
                SQLiteConnection.CreateFile(dbPath);
                Log.Info($"Database '{dbPath}' created");
            }
            else
                Log.Trace($"Database found at path '{dbPath}'");

            return createDb;
        }

        private bool CreateTableIfNeeded(string tableName, string creationQuery)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            string checkQuery = Queries.CheckTableExistence(tableName);
            bool tableExists = connection.ExecuteScalar<bool>(checkQuery);

            if (tableExists)
            {
                Log.Trace($"Table '{tableName}' already exists");
                return false;
            }
            else
            {
                Log.Trace($"Creating table '{tableName}'");
                connection.Execute(creationQuery);
                Log.Trace($"Table '{tableName}' created");

                return true;
            }
        }

        public bool CreateSchemaIfNeeded()
        {
            bool createEntityTable = CreateTableIfNeeded("Entities", Queries.CreateEntitiesTable);
            bool createSettingsTable = CreateTableIfNeeded("Settings", Queries.CreateSettingsTable);
            bool createEventsTable = CreateTableIfNeeded("Events", Queries.CreateEventsTable);

            return Booleans.Or(createEntityTable, createSettingsTable, createEventsTable);
        }

        private static class Queries
        {
            public static string CreateEntitiesTable => "CREATE TABLE Entities (Id TEXT(36, 36) CONSTRAINT UUID PRIMARY KEY, Name TEXT, DataType TEXT (6, 64) NOT NULL, JsonData TEXT NOT NULL)";
            public static string CreateSettingsTable => "CREATE TABLE Settings ([Key] TEXT(8, 64) PRIMARY KEY NOT NULL, Value NOT NULL)";
            public static string CreateEventsTable => "CREATE TABLE Events (DateTime TEXT(23, 23) NOT NULL, EventType TEXT(4, 64)  NOT NULL, Data TEXT NOT NULL, SessionId TEXT(36, 36) NOT NULL)";

            public static string CheckTableExistence(string table) => $"SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='{table}'";
        }
    }
}