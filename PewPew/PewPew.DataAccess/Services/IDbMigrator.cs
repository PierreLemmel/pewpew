﻿namespace PewPew.DataAccess.Services
{
    public interface IDbMigrator
    {
        bool CreateDatabaseIfNeeded();
        bool CreateSchemaIfNeeded();
    }
}