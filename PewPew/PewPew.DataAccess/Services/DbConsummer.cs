﻿using PewPew.Configuration;
using PewPew.DataAccess.Models;
using PewPew.IO;
using System.Data;
using System.Data.SQLite;

namespace PewPew.DataAccess.Services
{
    internal abstract class DbConsummer
    {
        protected readonly string dbPath;
        private readonly string connectionString;
        private readonly IConfigurationProvider<DataAccessOptions> optionsProvider;

        protected DbConsummer(IConfigurationProvider<DataAccessOptions> optionsProvider)
        {
            DataAccessOptions options = optionsProvider.Configuration;
            dbPath = options.DbPath.GetFullPath();

            SQLiteConnectionStringBuilder csb = new SQLiteConnectionStringBuilder()
            {
                Uri = $"file:{dbPath}"
            };

            connectionString = csb.ToString();
            this.optionsProvider = optionsProvider;
        }

        protected SQLiteConnection CreateConnection()
        {
            SQLiteConnection sqliteConnection = new SQLiteConnection(connectionString);
            sqliteConnection.StateChange += OnStateChanged;

            return sqliteConnection;
        }

        private void OnStateChanged(object sender, StateChangeEventArgs e)
        {
            SQLiteConnection sqliteConnection = (SQLiteConnection)sender;

            if (e.CurrentState == ConnectionState.Open)
                sqliteConnection.Trace += OnTraceEvent;
            else if (e.CurrentState == ConnectionState.Closed)
                sqliteConnection.Trace -= OnTraceEvent;
        }

        private void OnTraceEvent(object sender, TraceEventArgs e) => Log.Trace($"Executing Query: \"{e.Statement}\"");
    }
}