﻿using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PewPew.Configuration;
using PewPew.DataAccess.Contracts;
using PewPew.DataAccess.Errors;
using PewPew.DataAccess.Events;
using PewPew.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Reflection;

namespace PewPew.DataAccess.Services
{
    internal class Repository<TModel> : DbConsummer, IModelReader<TModel>, IModelWriter<TModel> where TModel : EntityModel
    {
        private static readonly string DataType = typeof(TModel).Name;
        private readonly IEventSender<DataSetChanged<TModel>> eventSender;

        private static JsonSerializerSettings SerializerSettings { get; } = new JsonSerializerSettings() { ContractResolver = new EntityModelContractResolver() };

        public Repository(IConfigurationProvider<DataAccessOptions> optionsProvider, IEventSender<DataSetChanged<TModel>> eventSender) : base(optionsProvider)
        {
            this.eventSender = eventSender;
        }

        public void Add(TModel elt)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            connection.Execute(Queries.AddEntity, ModelToEntity(elt));

            eventSender.SendEvent(new EntityCreated<TModel>(elt));
        }

        public void AddRange(IEnumerable<TModel> elts)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            connection.Execute(Queries.AddRange, ModelsToEntities(elts));
            eventSender.SendEvent(new EntitiesCreated<TModel>(elts));
        }

        public void Delete(Guid id)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            connection.Execute(Queries.DeleteById, new { Id = id.ToString(), DataType });
            eventSender.SendEvent(new EntityDeleted<TModel>(id));
        }

        public void DeleteRange(IEnumerable<Guid> ids)
        {
            if (ids.TryGetSingle(out Guid id))
            {
                Delete(id);
                return;
            }

            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            connection.Execute(
                Queries.DeleteRange,
                new
                {
                    Ids = ids.Stringify().ToArray(),
                    DataType,
                });

            eventSender.SendEvent(new EntitiesDeleted<TModel>(ids));
        }

        public void Clear()
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            connection.Execute(Queries.Clear, new { DataType });

            eventSender.SendEvent(new DataSetCleared<TModel>());
        }

        public TModel GetById(Guid id)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            Entity entity = connection.QuerySingleOrDefault<Entity>(Queries.GetById, new { Id = id.ToString(), DataType })
                ?? throw new MissingEntityException(DataType, id);
            TModel result = EntityToModel(entity);

            return result;
        }

        public TModel GetByName(string name)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            Entity entity = connection.QuerySingleOrDefault<Entity>(Queries.GetByName, new { Name = name, DataType })
                ?? throw new MissingEntityException(DataType, name);
            TModel result = EntityToModel(entity);

            return result;
        }

        public IReadOnlyCollection<TModel> GetRange(IEnumerable<Guid> ids)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            IEnumerable<TModel> result = connection.Query<Entity>(
                Queries.GetRange,
                new
                {
                    Ids = ids.Stringify(),
                    DataType,
                })
                .Select(entity => EntityToModel(entity)
            );
            return result.ToList();
        }

        public IReadOnlyCollection<TModel> GetAll()
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            IEnumerable<TModel> result = connection
                .Query<Entity>(Queries.GetAll, new { DataType })
                .Select(entity => EntityToModel(entity)
            );
            return result.ToList();
        }

        public void Update(TModel elt)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            connection.Execute(Queries.UpdateEntity, ModelToEntity(elt));
            eventSender.SendEvent(new EntityUpdated<TModel>(elt));
        }

        public void UpdateRange(IEnumerable<TModel> elts)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            SQLiteTransaction transaction = connection.BeginTransaction();
            connection.Execute(Queries.UpdateRange, ModelsToEntities(elts), transaction);
            transaction.Commit();

            eventSender.SendEvent(new EntitiesUpdated<TModel>(elts));
        }

        private Entity ModelToEntity(TModel model) => new Entity(model.Id.ToString(), model.Name, DataType, Json.Serialize(model, SerializerSettings));

        private IEnumerable<Entity> ModelsToEntities(IEnumerable<TModel> models) => models.Select(model => ModelToEntity(model));

        private TModel EntityToModel(Entity entity)
        {
            TModel model = Json.Deserialize<TModel>(entity.JsonData);
            model.Id = entity.Id.ToGuid();
            model.Name = entity.Name;

            return model;
        }

        private static class Queries
        {
            public static string AddEntity => "INSERT INTO Entities (Id, Name, DataType, JsonData) VALUES (@Id, @Name, @DataType, @JsonData);";
            public static string AddRange => "INSERT INTO Entities (Id, Name, DataType, JsonData) VALUES (@Id, @Name, @DataType, @JsonData);";

            public static string UpdateEntity => "UPDATE Entities Set Name = @Name, JsonData = @JsonData WHERE Id = @Id AND DataType = @DataType";
            public static string UpdateRange => "UPDATE Entities Set Name = @Name, JsonData = @JsonData WHERE Id = @Id AND DataType = @DataType";

            public static string GetById => "SELECT * FROM Entities WHERE Id = @Id AND DataType = @DataType";
            public static string GetRange => "SELECT * FROM Entities WHERE DataType = @DataType AND Id IN @Ids";
            public static string GetByName => "SELECT * FROM Entities WHERE Name = @Name AND DataType = @DataType";
            public static string GetAll => "SELECT * FROM Entities WHERE DataType = @DataType";

            public static string DeleteById => "DELETE FROM Entities WHERE Id = @Id AND DataType = @DataType";
            public static string DeleteRange => "DELETE FROM Entities WHERE DataType = @DataType AND Id IN @Ids";

            public static string Clear => "DELETE FROM Entities WHERE DataType = @DataType";

        }

        private class EntityModelContractResolver : DefaultContractResolver
        {
            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty jsonProperty = base.CreateProperty(member, memberSerialization);

                if (member.Name.IsOneOf(nameof(EntityModel.Id), nameof(EntityModel.Name)))
                {
                    jsonProperty.Ignored = true;
                }

                return jsonProperty;
            }
        }
    }
}