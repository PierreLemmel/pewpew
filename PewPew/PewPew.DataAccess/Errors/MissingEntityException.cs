﻿using System;

namespace PewPew.DataAccess.Errors
{
    public class MissingEntityException : Exception
    {
        public Guid? Id { get; }
        public string DataType { get; }
        public string? Name { get; }

        public MissingEntityException(string dataType, Guid id) : base($"Missing entity of type '{dataType}' with id '{id}'")
        {
            DataType = dataType;
            Id = id;
        }

        public MissingEntityException(string dataType, string name) : base($"Missing entity of type '{dataType}' named '{name}'")
        {
            DataType = dataType;
            Name = name;
        }
    }
}