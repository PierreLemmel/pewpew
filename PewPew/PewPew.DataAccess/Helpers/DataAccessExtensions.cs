﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.DataAccess
{
    public static class DataAccessExtensions
    {
        public static void AddRange<TModel>(this IModelWriter<TModel> modelWriter, params TModel[] models) where TModel : EntityModel => modelWriter.AddRange(models);
        public static void UpdateRange<TModel>(this IModelWriter<TModel> modelWriter, params TModel[] models) where TModel : EntityModel => modelWriter.UpdateRange(models);
        public static void DeleteRange<TModel>(this IModelWriter<TModel> modelWriter, IEnumerable<TModel> models) where TModel : EntityModel => modelWriter.DeleteRange(models.Select(model => model.Id));
        public static void DeleteRange<TModel>(this IModelWriter<TModel> modelWriter, params Guid[] ids) where TModel : EntityModel => modelWriter.DeleteRange(ids);

        public static IReadOnlyCollection<TModel> GetRange<TModel>(this IModelReader<TModel> modelReader, params Guid[] ids) where TModel : EntityModel => modelReader.GetRange(ids);
    }
}