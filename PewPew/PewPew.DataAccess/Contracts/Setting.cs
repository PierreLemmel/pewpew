﻿namespace PewPew.DataAccess.Contracts
{
    public record Setting(string Key, string Value) { }
}