﻿namespace PewPew.DataAccess.Contracts
{
    internal record Entity(string Id, string? Name, string DataType, string JsonData){ }
}