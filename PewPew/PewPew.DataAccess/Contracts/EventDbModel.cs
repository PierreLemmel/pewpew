namespace PewPew.DataAccess.Contracts
{
    public record EventDbModel(string DateTime, string EventType, string Data, string SessionId)
    {
        public const string DateFormat = "yyyy-MM-dd HH:mm:ss.fff";
    }
}