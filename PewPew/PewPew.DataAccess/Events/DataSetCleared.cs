namespace PewPew.DataAccess.Events
{
    public class DataSetCleared<TEntity> : DataSetChanged<TEntity> where TEntity : EntityModel
    {
        public override string Name => $"{typeof(TEntity).Name}DataSetCleared";
    }
}