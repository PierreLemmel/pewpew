namespace PewPew.DataAccess.Events
{
    public class EntityCreated<TEntity> : DataSetChanged<TEntity> where TEntity : EntityModel
    {
        public override string Name => $"{typeof(TEntity).Name}Created";

        public TEntity Entity { get; }

        public EntityCreated(TEntity entity)
        {
            Entity = entity;
        }
    }
}