namespace PewPew.DataAccess.Events
{
    public abstract class DataSetChanged<TEntity> : Event where TEntity : EntityModel
    {
        public override string Name => typeof(TEntity).Name + "DataSetChanged";
    }
}