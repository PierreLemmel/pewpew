using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.DataAccess.Events
{
    public class EntitiesDeleted<TEntity> : DataSetChanged<TEntity> where TEntity : EntityModel
    {
        public override string Name => $"Multiple{typeof(TEntity).Name}Deleted";

        public IReadOnlyCollection<Guid> Entities { get; }

        public EntitiesDeleted(IEnumerable<Guid> entities) : this(entities.ToList()) { }
        public EntitiesDeleted(IReadOnlyCollection<Guid> entities)
        {
            Entities = entities;
        }
    }
}