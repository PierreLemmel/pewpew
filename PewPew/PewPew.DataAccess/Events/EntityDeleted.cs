using System;

namespace PewPew.DataAccess.Events
{
    public class EntityDeleted<TEntity> : DataSetChanged<TEntity> where TEntity : EntityModel
    {
        public override string Name => $"{typeof(TEntity).Name}Deleted";

        public Guid EntityId { get; }

        public EntityDeleted(Guid entityId)
        {
            EntityId = entityId;
        }
    }
}