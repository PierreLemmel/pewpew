namespace PewPew.DataAccess.Events
{
    public class EntityUpdated<TEntity> : DataSetChanged<TEntity> where TEntity : EntityModel
    {
        public override string Name => $"{typeof(TEntity).Name}Updated";

        public TEntity Entity { get; }

        public EntityUpdated(TEntity entity)
        {
            Entity = entity;
        }
    }
}