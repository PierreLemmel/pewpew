using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.DataAccess.Events
{
    public class EntitiesCreated<TEntity> : DataSetChanged<TEntity> where TEntity : EntityModel
    {
        public override string Name => $"Multiple{typeof(TEntity).Name}Created";

        public IReadOnlyCollection<TEntity> Entities { get; }

        public EntitiesCreated(IEnumerable<TEntity> entities) : this(entities.ToList()) { }
        public EntitiesCreated(IReadOnlyCollection<TEntity> entities)
        {
            Entities = entities;
        }
    }
}