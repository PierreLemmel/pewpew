using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.DataAccess.Events
{
    public class EntitiesUpdated<TEntity> : DataSetChanged<TEntity> where TEntity : EntityModel
    {
        public override string Name => $"Multiple{typeof(TEntity).Name}Updated";

        public IReadOnlyList<TEntity> Entities { get; }

        public EntitiesUpdated(IEnumerable<TEntity> entities) : this(entities.ToList()) { }
        public EntitiesUpdated(IReadOnlyList<TEntity> entities)
        {
            Entities = entities;
        }
    }
}