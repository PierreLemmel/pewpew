using Autofac;
using NFluent;
using NUnit.Framework;
using PewPew.Scanning;
using PewPew.UI.DI;
using PewPew.UI.Orchestration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace PewPew.UI.Application.Tests.DI
{
    /// <summary>
    /// This class contains sketchy container scenarios
    /// </summary>
    [Apartment(ApartmentState.STA)]
    public class DIIntegrationTests
    {
        static DIIntegrationTests() => Warmup.PreloadPewPewAssemblies();

        [Test]
        public void Orchestrator_Is_Always_Resolved_As_Same_Instance_As_IEventHandler_And_IControlOrchestrator()
        {
            IContainer container = Bootstrap.BuildContainer();

            IControlOrchestrator asOrchestrator = container.Resolve<IControlOrchestrator>();
            IControlOrchestrator asEventHandler = container
                .Resolve<IEnumerable<IEventHandler>>()
                .OfType<IControlOrchestrator>()
                .Single();

            Check.That(asEventHandler)
                .IsSameReferenceAs(asOrchestrator);
        }
    }
}