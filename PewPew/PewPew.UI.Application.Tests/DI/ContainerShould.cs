﻿using Autofac;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using NFluent;
using NUnit.Framework;
using PewPew.Configuration;
using PewPew.DataAccess;
using PewPew.DataAccess.Events;
using PewPew.DataAccess.Models;
using PewPew.DataAccess.Services;
using PewPew.DI;
using PewPew.Dmx.Models;
using PewPew.Dmx.OpenDmx;
using PewPew.Dmx.UI.Controls;
using PewPew.Events.Dispatchers;
using PewPew.Events.Handlers;
using PewPew.Lifecycle;
using PewPew.Scanning;
using PewPew.UI.DI;
using PewPew.UI.Events;
using PewPew.UI.Events.User;
using PewPew.UI.Menus;
using PewPew.UI.Observable;
using PewPew.UI.Orchestration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace PewPew.UI.Application.Tests.DI
{
    [Apartment(ApartmentState.STA)]
    public class ContainerShould
    {
        static ContainerShould() => Warmup.PreloadPewPewAssemblies();

        [Test]
        public void Build_Without_Throwing() => Check
            .ThatCode(() => Bootstrap.BuildContainer())
            .DoesNotThrow();

        [Test]
        public void Load_Static_Components_Without_Throwing()
        {
            IContainer container = Bootstrap.BuildContainer();

            Check.ThatCode(() => container.LoadStaticComponents())
                .DoesNotThrow();
        }

        [Test]
        [GenericTestCaseSource(nameof(StaticComponents))]
        public void Load_Static_Component<TComponent>() where TComponent : IStaticComponent
        {
            IContainer container = Bootstrap.BuildContainer();
            IEnumerable<IStaticComponent> loaded = container.LoadStaticComponents();

            bool componentLoaded = loaded.Any(staticComponent => staticComponent.GetType() == typeof(TComponent));

            Check
                .WithCustomMessage($"Component {typeof(TComponent).Name} is marked as a static component but is not registered in Container. Consider using {nameof(ContainerBuilderExtensions.AddStaticComponent)}<{typeof(TComponent).Name}>() method")
                .That(componentLoaded)
                .IsTrue();
        }

        [Test]
        [GenericTestCaseSource(nameof(ConfigurationTypes))]
        [GenericTestCaseSource(nameof(DataAccessTypes))]
        [GenericTestCaseSource(nameof(EventsTypes))]
        [GenericTestCaseSource(nameof(DmxTypes))]
        [GenericTestCaseSource(nameof(OpenDmxTypes))]
        [GenericTestCaseSource(nameof(UITypes))]
        [GenericTestCaseSource(nameof(DialogTypes))]
        [GenericTestCaseSource(nameof(PewPewControls))]
        [GenericTestCaseSource(nameof(PewPewMenus))]
        [GenericTestCaseSource(nameof(ViewModels))]
        [GenericTestCaseSource(nameof(EventSenders))]
        [GenericTestCaseSource(nameof(ManufacturerDataSetChangedEventSenders))]
        [GenericTestCaseSource(nameof(ObservableDataSets))]
        [GenericTestCaseSource(nameof(UIEventSenders))]
        public void Resolve_Service<TService>() where TService : class
        {
            IContainer container = Bootstrap.BuildContainer();

            TService service = container.Resolve<TService>();

            Check.That(service).IsNotNull();
        }

        [Test]
        [GenericTestCaseSource(nameof(MultiResolveTypes))]
        public void Resolve_Multiple_Services<TService>() where TService : class
        {
            IContainer container = Bootstrap.BuildContainer();

            IEnumerable<TService> services = container.Resolve<IEnumerable<TService>>();

            Check.That(services).IsNotNull();
        }


        [Test]
        [GenericTestCaseSource(nameof(SingleInstanceTypes))]
        public void Always_Refer_To_Same_Instance<TSingleton>() where TSingleton : class
        {
            IContainer container = Bootstrap.BuildContainer();

            TSingleton resolve1 = container.Resolve<TSingleton>();
            TSingleton resolve2 = container.Resolve<TSingleton>();

            Check.That(resolve1).IsSameReferenceAs(resolve2);
        }

        public static IEnumerable<Type> ConfigurationTypes => new Type[]
        {
            typeof(IConfiguration),
            typeof(IConfigurationProvider<DataAccessOptions>),
        };

        public static IEnumerable<Type> DataAccessTypes => new Type[]
        {
            typeof(IConfigurationProvider<DataAccessOptions>),

            typeof(IDbMigrator),
        };

        public static IEnumerable<Type> EventsTypes => new Type[]
        {
            typeof(IEventDispatcher),
            typeof(IHandlerRegistrar)
        };

        public static IEnumerable<Type> DmxTypes => new Type[]
        {
            typeof(IModelReader<Manufacturer>),
            typeof(IModelWriter<Manufacturer>),

            typeof(IModelReader<FixtureModel>),
            typeof(IModelWriter<FixtureModel>),
        };

        public static IEnumerable<Type> OpenDmxTypes => new Type[]
        {
            typeof(IOpenDmxInterface),
        };

        public static IEnumerable<Type> DialogTypes => Scan
            .PewPewUIClasses
            .InheritingFrom<Dialog>();

        public static IEnumerable<Type> PewPewControls => Scan
            .PewPewUIClasses
            .InheritingFrom<PewPewControl>();

        public static IEnumerable<Type> PewPewMenus => Scan
            .PewPewUIClasses
            .InheritingFrom<PewPewMenu>();

        public static IEnumerable<Type> ViewModels => Scan
            .PewPewUIClasses
            .Implementing<IViewModel>();

        public static IEnumerable<Type> EventSenders => Scan
            .PewPewClasses
            .InheritingFrom<Event>()
            .ExcludeGenericTypeDefinitions()
            .MakeGenericsFor(typeof(IEventSender<>));

        public static IEnumerable<Type> ObservableDataSets => Scan
            .PewPewClasses
            .InheritingFrom<EntityModel>()
            .ConcreteTypes()
            .MakeGenericsFor(typeof(ObservableDataSet<>));

        public static IEnumerable<Type> StaticComponents => Scan
            .PewPewClasses
            .Implementing<IStaticComponent>()
            .ConcreteTypes();

        public static IEnumerable<Type> ManufacturerDataSetChangedEventSenders => new Type[]
        {
            typeof(IEventSender<DataSetChanged<Manufacturer>>),
            typeof(IEventSender<DataSetCleared<Manufacturer>>),
            typeof(IEventSender<EntityCreated<Manufacturer>>),
            typeof(IEventSender<EntityUpdated<Manufacturer>>),
            typeof(IEventSender<EntityDeleted<Manufacturer>>),
            typeof(IEventSender<EntitiesCreated<Manufacturer>>),
            typeof(IEventSender<EntitiesUpdated<Manufacturer>>),
            typeof(IEventSender<EntitiesDeleted<Manufacturer>>),
        };

        public static IEnumerable<Type> UIEventSenders => new Type[]
        {
            typeof(IEventSender<ControlRequested<FixtureEditor>>),
            typeof(IEventSender<ControlRequested<ManufacturersEditor>>),
        };

        public static IEnumerable<Type> UITypes => new Type[]
        {
            typeof(MainWindow),
            typeof(IViewMenuBuilder),
            typeof(IControlOrchestrator),
            typeof(IEventSender<ControlEvent>),
        };


        public static IEnumerable<Type> MultiResolveTypes => new Type[]
        {
            typeof(IEventHandler),
            typeof(PewPewViewMenu),
        };

        public static IEnumerable<Type> SingleInstanceTypes => new Type[]
        {
            typeof(MainWindow),
            typeof(IEventDispatcher),
            typeof(IControlOrchestrator),
        };
    }
}