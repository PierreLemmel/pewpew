﻿using Autofac;
using NFluent;
using NUnit.Framework;
using PewPew.Scanning;
using PewPew.UI.DI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace PewPew.UI.Application.Tests.Factories
{
    public class PewPewControlFactoryShould
    {
        static PewPewControlFactoryShould() => Warmup.PreloadPewPewAssemblies();

        [Test]
        [Apartment(ApartmentState.STA)]
        [GenericTestCaseSource(nameof(UserControls))]
        public void CreateControlFromFactory<TPewPewControl>() where TPewPewControl : PewPewControl
        {
            IContainer container = Bootstrap.BuildContainer();
            IPewPewControlFactory factory = container.Resolve<IPewPewControlFactory>();
            
            TPewPewControl pewPewControl = factory.CreateControl<TPewPewControl>();
            Check.That(pewPewControl).IsNotNull();
        }

        public static IEnumerable<Type> UserControls => Scan
            .PewPewUIClasses
            .InheritingFrom<PewPewControl>();
    }
}