using Autofac;
using NFluent;
using NUnit.Framework;
using PewPew.Scanning;
using PewPew.UI.DI;
using System;
using System.Collections.Generic;
using System.Threading;

namespace PewPew.UI.Application.Tests.Factories
{
    public class DialogFactoryShould
    {
        static DialogFactoryShould() => Warmup.PreloadPewPewAssemblies();

        [Test]
        [Apartment(ApartmentState.STA)]
        [GenericTestCaseSource(nameof(Dialogs))]
        public void CreateDialogFromFactory<TDialog>() where TDialog : Dialog
        {
            IContainer container = Bootstrap.BuildContainer();
            IDialogFactory<TDialog> factory = container.Resolve<IDialogFactory<TDialog>>();

            TDialog dialgo = factory.CreateDialog(owner: null);
            Check.That(dialgo).IsNotNull();
        }

        public static IEnumerable<Type> Dialogs => Scan
            .PewPewUIClasses
            .InheritingFrom<Dialog>();
    }
}