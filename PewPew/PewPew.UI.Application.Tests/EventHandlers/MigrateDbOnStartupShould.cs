using Moq;
using NFluent;
using NUnit.Framework;
using PewPew.DataAccess.Services;
using PewPew.Events.Handlers;
using PewPew.Fakes;
using PewPew.UI.EventHandlers;

namespace PewPew.UI.Application.Tests.EventHandlers
{
    public class MigrateDbOnStartupShould
    {
        [Test]
        public void Be_Registered_After_Construction()
        {
            (IHandlerRegistrar registrar, MigrateDbOnStartup sut) = CreateSut();

            Check.That(sut).IsRegisteredIn(registrar);
        }

        [Test]
        public void Be_Unregistered_After_Disposing()
        {
            (IHandlerRegistrar registrar, MigrateDbOnStartup sut) = CreateSut();

            sut.Dispose();
            Check.That(sut).IsNotRegisteredIn(registrar);
        }

        private (IHandlerRegistrar registrar, MigrateDbOnStartup sut) CreateSut()
        {
            IHandlerRegistrar registrar = new FakeRegistrar();
            IDbMigrator migrator = Mock.Of<IDbMigrator>();

            MigrateDbOnStartup sut = new(migrator, registrar);

            return (registrar, sut);
        }
    }
}