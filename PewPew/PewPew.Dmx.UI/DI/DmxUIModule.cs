﻿using Autofac;
using PewPew.Dmx.UI.Menus;
using PewPew.UI;

namespace PewPew.Dmx.UI.DI
{
    public class DmxUIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterBaseUITypes(ThisAssembly);

            builder.AddViewMenu<DmxMenu>();
        }
    }
}