using FluentValidation;
using PewPew.UI;
using System;

namespace PewPew.Dmx.UI.ViewModels
{
    public class CreateManufacturerViewModel : DialogViewModel<CreateManufacturerViewModel>
    {
        public Guid Id { get; } = Guid.NewGuid();

        public string Name { get; set; } = "";

        protected override void SetupValidator(AbstractValidator<CreateManufacturerViewModel> validator)
        {
            validator.RuleFor(vm => vm.Name)
                .MinimumLength(6)
                .MaximumLength(36);
        }
    }
}