﻿using PewPew.DataAccess;
using PewPew.Dmx.Models;
using PewPew.UI;
using PewPew.UI.Observable;
using System;
using System.Collections.ObjectModel;

namespace PewPew.Dmx.UI.ViewModels
{
    public class FixtureEditorViewModel : ViewModel<FixtureEditorViewModel>
    {
        private readonly IModelReader<Manufacturer> manufacturerReader;
        private readonly IModelReader<FixtureModel> fixtureModelReader;
        private readonly IModelWriter<FixtureModel> fixtureModelWriter;
        
        public ObservableDataSet<Manufacturer> Manufacturers { get; }

        public FixtureEditorViewModel(
            ObservableDataSet<Manufacturer> manufacturers,
            IModelReader<Manufacturer> manufacturerReader,
            IModelReader<FixtureModel> fixtureModelReader,
            IModelWriter<FixtureModel> fixtureModelWriter)
        {
            Manufacturers = manufacturers;

            this.manufacturerReader = manufacturerReader;
            this.fixtureModelReader = fixtureModelReader;
            this.fixtureModelWriter = fixtureModelWriter;
        }
    }
}