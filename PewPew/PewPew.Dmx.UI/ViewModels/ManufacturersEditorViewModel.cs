using PewPew.DataAccess;
using PewPew.Dmx.Models;
using PewPew.Dmx.UI.Dialogs;
using PewPew.UI;
using PewPew.UI.Observable;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace PewPew.Dmx.UI.ViewModels
{
    public class ManufacturersEditorViewModel : ViewModel<ManufacturersEditorViewModel>
    {
        public ObservableDataSet<Manufacturer> Manufacturers { get; }

        public ManufacturersEditorViewModel(
            ObservableDataSet<Manufacturer> manufacturers,
            IModelReader<Manufacturer> manufacturerReader,
            IModelWriter<Manufacturer> manufacturerWriter,
            IDialogFactory<CreateManufacturerDialog> createManufacturerDialogFactory)
        {
            Manufacturers = manufacturers;

            DeleteItemsCommand = Command.Create<IEnumerable<Manufacturer>>(items => manufacturerWriter.DeleteRange(items));

            CreateItemDialogCommand = Command.Create<Window>(window =>
            {
                CreateManufacturerDialog dialog = createManufacturerDialogFactory.CreateDialog(window);
                DialogResult result = dialog.ShowDialog(
                    onSuccess: () =>
                    {
                        CreateManufacturerViewModel viewModel = dialog.ViewModel;
                        Manufacturer manufacturer = new Manufacturer(Guid.NewGuid(), viewModel.Name);

                        manufacturerWriter.Add(manufacturer);
                        
                        Log.Trace($"Manufacturer '{viewModel.Name}' created");
                    },
                    onFailure: () => Log.Error("CreateManufacturerDialog failed")
                );
            });
        }

        public Command<Window> CreateItemDialogCommand { get; }
        public Command<IEnumerable<Manufacturer>> DeleteItemsCommand { get; }
    }
}