﻿using PewPew.Dmx.UI.Controls;
using PewPew.UI.Events;
using PewPew.UI.Menus;
using System;

namespace PewPew.Dmx.UI.Menus
{
    /// <summary>
    /// Interaction logic for DmxMenu.xaml
    /// </summary>
    public partial class DmxMenu : PewPewViewMenu
    {
        public DmxMenu(DmxMenuViewModel viewModel) : base(viewModel)
        {
            InitializeComponent();
        }
    }
}