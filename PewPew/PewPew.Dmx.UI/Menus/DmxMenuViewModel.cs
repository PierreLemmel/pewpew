using PewPew.Dmx.UI.Controls;
using PewPew.UI;
using PewPew.UI.Events;
using PewPew.UI.Events.User;

namespace PewPew.Dmx.UI.Menus
{
    public class DmxMenuViewModel : ViewModel<DmxMenuViewModel>
    {
        public DmxMenuViewModel(
            IEventSender<ControlRequested<FixtureEditor>> fixtureEditorRequested,
            IEventSender<ControlRequested<ManufacturersEditor>> manufacturersEditorRequested)
        {
            RequestFixtureEditor = Command.Create(fixtureEditorRequested.SendEvent);
            RequestManufacturersEditor = Command.Create(manufacturersEditorRequested.SendEvent);
        }

        public Command RequestFixtureEditor { get; }
        public Command RequestManufacturersEditor { get; }
    }
}