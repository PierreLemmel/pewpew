﻿using PewPew.Dmx.UI.ViewModels;
using PewPew.UI;

namespace PewPew.Dmx.UI.Dialogs
{
    public partial class CreateManufacturerDialog : Dialog
    {
        public new CreateManufacturerViewModel ViewModel { get; }

        public CreateManufacturerDialog(CreateManufacturerViewModel viewModel) : base(viewModel)
        {
            ViewModel = viewModel;

            InitializeComponent();
        }
    }
}