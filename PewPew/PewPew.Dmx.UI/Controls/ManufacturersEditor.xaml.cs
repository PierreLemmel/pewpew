﻿using PewPew.Dmx.UI.ViewModels;
using PewPew.UI;
using PewPew.UI.Events;

namespace PewPew.Dmx.UI.Controls
{
    /// <summary>
    /// Interaction logic for ManufacturersEditor.xaml
    /// </summary>
    public partial class ManufacturersEditor : PewPewControl
    {
        public ManufacturersEditor(
            ManufacturersEditorViewModel viewModel,
            IEventSender<ControlEvent> sender) : base(viewModel, sender)
        {
            InitializeComponent();
        }
    }
}