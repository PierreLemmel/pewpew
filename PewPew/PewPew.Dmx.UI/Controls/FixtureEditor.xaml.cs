﻿using PewPew.Dmx.UI.ViewModels;
using PewPew.UI;
using PewPew.UI.Events;

namespace PewPew.Dmx.UI.Controls
{
    /// <summary>
    /// Interaction logic for FixtureEditor.xaml
    /// </summary>
    public partial class FixtureEditor : PewPewControl
    {
        public FixtureEditor(
            FixtureEditorViewModel viewModel,
            IEventSender<ControlEvent> sender)
            : base(viewModel, sender)
        {
            InitializeComponent();
        }
    }
}