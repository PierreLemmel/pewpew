﻿using System;

namespace PewPew.Dmx.OpenDmx.FTD2XX
{
    internal sealed class FT_Exception : Exception
    {
        public FT_STATUS Status { get; }
        public string Function { get; }

        internal FT_Exception(FT_STATUS status, string function) : base($"Unexpected status '{status}' in function '{function}'")
        {
            Status = status;
            Function = function;
        }
    }
}