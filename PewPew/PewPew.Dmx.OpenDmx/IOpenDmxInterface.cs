﻿using PewPew.Lifecycle;
using System;

namespace PewPew.Dmx.OpenDmx
{
    public interface IOpenDmxInterface : IStartable, IStoppable, IDisposable
    {
        byte this[int index] { get; set; }

        void ClearFrame();
        void CopyData(int channelOffset, byte[] data, int length);
    }
}