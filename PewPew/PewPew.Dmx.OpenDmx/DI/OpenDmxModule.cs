﻿using Autofac;
using PewPew.Dmx.OpenDmx.FTD2XX;

namespace PewPew.Dmx.OpenDmx.DI
{
    public class OpenDmxModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FTD2XXInterface>().As<IOpenDmxInterface>();
        }
    }
}