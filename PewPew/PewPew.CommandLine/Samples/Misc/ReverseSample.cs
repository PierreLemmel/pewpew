﻿using System;

namespace PewPew.CommandLine.Samples.Misc
{
    internal class ReverseSample : ISample
    {
        public void Run()
        {
            while (true)
            {
                Console.WriteLine("Write input to reverse (press enter to exit): ");
                Console.WriteLine();

                string input = Console.ReadLine() ?? "";

                if (input.IsEmpty()) return;

                Console.WriteLine();
                Console.WriteLine("Reversed:");
                Console.WriteLine(input.Reverse());
                Console.WriteLine(); 
            }
        }
    }
}
