﻿using PewPew.DataAccess;
using PewPew.Dmx.Models;
using System;

namespace PewPew.CommandLine.Samples.DataAccess
{
    public class AddToDbSample : ISample
    {
        private readonly IModelWriter<Manufacturer> modelWriter;

        public AddToDbSample(IModelWriter<Manufacturer> modelWriter)
        {
            this.modelWriter = modelWriter;
        }

        public void Run()
        {
            Console.WriteLine("Adding model to DB");

            Manufacturer manufacturer = new Manufacturer(Guid.NewGuid(), "Fun-Generation");
            modelWriter.Add(manufacturer);

            Console.WriteLine("Model added to DB");
        }
    }
}
