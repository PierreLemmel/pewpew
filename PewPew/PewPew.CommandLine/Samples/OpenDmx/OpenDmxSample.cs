﻿using PewPew.Dmx.OpenDmx.FTD2XX;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PewPew.CommandLine.Samples.OpenDmx
{
    public class OpenDmxSample : ISampleAsync
    {
        private const int RefreshFrequency = 1000;

        private const int FlatJardinOffset = 1;
        private const int FlatCourOffset = 9;
        private const int ParLEDJardinOffset = 17;
        private const int ParLEDCourOffset = 25;

        public async Task RunAsync(int duration)
        {
            CancellationTokenSource cts = new CancellationTokenSource(duration);
            var token = cts.Token;

            using FTD2XXInterface ftd2xxInterface = new FTD2XXInterface();

            Log.Info("Starting FTD2XX Interface");
            ftd2xxInterface.Start();
            Log.Info("FTD2XX Interface Started");

            CreateRefreshLoopback(token, ftd2xxInterface);

            await ChaserDemo(token, ftd2xxInterface);

            Log.Info("Stopping FTD2XX Interface");
            ftd2xxInterface.Stop();
            Log.Info("OpenDmx FTD2XX Interface");
        }

        private async Task ChaserDemo(CancellationToken token, FTD2XXInterface openDmx)
        {
            const int delay = 100;

            int step = 0;
            while (!token.IsCancellationRequested)
            {
                openDmx.ClearFrame();

                switch (step % 4)
                {
                    case 0:
                        openDmx[ParLEDJardinOffset + 0] = 0xff;
                        openDmx[ParLEDJardinOffset + 1] = 0xff;
                        openDmx[ParLEDJardinOffset + 2] = 0xff;
                        openDmx[ParLEDJardinOffset + 4] = 0xff;

                        Log.Trace("PAR LED Jardin");
                        break;

                    case 1:
                        openDmx[FlatJardinOffset + 0] = 0xff;
                        openDmx[FlatJardinOffset + 1] = 0xff;
                        openDmx[FlatJardinOffset + 2] = 0xff;

                        Log.Trace("Flat Jardin");
                        break;

                    case 2:
                        openDmx[FlatCourOffset + 0] = 0xff;
                        openDmx[FlatCourOffset + 1] = 0xff;
                        openDmx[FlatCourOffset + 2] = 0xff;

                        Log.Trace("Flat Cour");
                        break;

                    case 3:
                        openDmx[ParLEDCourOffset + 0] = 0xff;
                        openDmx[ParLEDCourOffset + 1] = 0xff;
                        openDmx[ParLEDCourOffset + 2] = 0xff;
                        openDmx[ParLEDCourOffset + 4] = 0xff;

                        Log.Trace("PAR LED Cour");
                        break;
                }

                await Task.Delay(delay);
                step++;
            }
        }

        private void CreateRefreshLoopback(CancellationToken token, FTD2XXInterface openDmx)
        {
            Task task = Task.Delay(TimeSpan.FromMilliseconds(1000.0d / RefreshFrequency), token)
                .ContinueWith(
                    _ =>
                    {
                        openDmx.SendDmxFrame();
                        CreateRefreshLoopback(token, openDmx);

                    }, TaskContinuationOptions.NotOnCanceled
                )
                .ContinueWith(
                    _ =>
                    {
                        Log.Debug("RefreshTask Cancelled");
                        openDmx.ClearFrame();
                        openDmx.SendDmxFrame();
                        Log.Debug("DMX Frame Cleared");
                    },
                    TaskContinuationOptions.OnlyOnCanceled
                );
        }
    }
}