﻿using Autofac;
using PewPew.CommandLine.DI;
using PewPew.Dmx.OpenDmx.DI;

namespace PewPew.CommandLine
{
    public static class Bootstrap
    {
        public static IContainer BuildContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

#if DEBUG
            string environment = "debug";
#elif RELEASE
            string environment = "release";
#else
#error Unexpected environment
#endif

            builder.Properties.Add("env", environment);

            builder.RegisterModule<CommandLineModule>();
            builder.RegisterModule<OpenDmxModule>();

            IContainer container = builder.Build();

            return container;
        }
    }
}
