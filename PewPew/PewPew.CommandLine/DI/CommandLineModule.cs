﻿using Autofac;
using Microsoft.Extensions.Configuration;
using PewPew.DataAccess.Models;
using Assembly = System.Reflection.Assembly;

namespace PewPew.CommandLine.DI
{
    public class CommandLineModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            
            builder.RegisterImplementationsAsSelf<ISample>(currentAssembly);
            builder.RegisterImplementationsAsSelf<ISampleAsync>(currentAssembly);

            AddConfiguration(builder);
        }

        private void AddConfiguration(ContainerBuilder builder)
        {
            string environment = builder.Properties.Get<string>("env");

            IConfigurationBuilder cfgBuilder = new ConfigurationBuilder();

            cfgBuilder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            cfgBuilder.AddJsonFile($"appsettings.{environment}.json", optional: false, reloadOnChange: true);

            IConfiguration config = cfgBuilder.Build();

            builder.RegisterInstance(config);

            builder.RegisterConfigProvider<DataAccessOptions>("DataAccess");
        }
    }
}