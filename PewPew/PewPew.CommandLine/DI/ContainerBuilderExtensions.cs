﻿using Autofac;
using Autofac.Builder;
using Microsoft.Extensions.Configuration;
using PewPew.Configuration;

namespace PewPew.CommandLine.DI
{
    public static class ContainerBuilderExtensions
    {
        public static IRegistrationBuilder<ConfigurationProvider<TOptions>, SimpleActivatorData, SingleRegistrationStyle> RegisterConfigProvider<TOptions>(
            this ContainerBuilder builder, string section)
            where TOptions : new()
            => builder.Register(c => new ConfigurationProvider<TOptions>(c.Resolve<IConfiguration>(), section)).As<IConfigurationProvider<TOptions>>();
    }
}