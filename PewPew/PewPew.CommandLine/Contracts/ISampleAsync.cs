﻿using System.Threading.Tasks;

namespace PewPew.CommandLine
{
    public interface ISampleAsync
    {
        Task RunAsync(int duration);
    }
}