﻿namespace PewPew.CommandLine
{
    public interface ISample
    {
        void Run();
    }
}