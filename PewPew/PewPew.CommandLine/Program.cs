﻿using Autofac;
using PewPew.CommandLine.Samples.DataAccess;
using PewPew.CommandLine.Samples.Misc;
using System.Threading.Tasks;

namespace PewPew.CommandLine
{
    public static class Program
    {
        public const int DefaultDuration = 5000;

        //public static async Task Main(string[] args) => await RunSampleAsync<OpenDmxSample>(DefaultDuration);
        //public static void Main(string[] args) => RunSample<AddToDbSample>();
        public static void Main(string[] args) => RunSample<ReverseSample>();


        private static IContainer container = Bootstrap.BuildContainer();

        private static void RunSample<TSample>() where TSample : ISample
            => container.Resolve<TSample>().Run();
        private static async Task RunSampleAsync<TSampleAsync>(int duration) where TSampleAsync : ISampleAsync
            => await container.Resolve<TSampleAsync>().RunAsync(duration);
    }
}
