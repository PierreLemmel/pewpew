using PewPew.UI.Menus;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.UI.Base.Tests.Menus.SampleClasses
{
    internal class InjectedViewMenuConstructorPriorityCapture
    {
        public IReadOnlyCollection<int> Priorities { get; }

        public InjectedViewMenuConstructorPriorityCapture(IEnumerable<PewPewViewMenu> viewMenus)
            => Priorities = viewMenus.Select(vm => vm.Priority).ToList();
    }
}