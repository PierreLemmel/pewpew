using PewPew.UI.Menus;

namespace PewPew.UI.Base.Tests.Menus.SampleClasses
{
    internal class SomeOtherViewMenu : PewPewViewMenu
    {
        public SomeOtherViewMenu() : base(new SomeViewModel())
        {
        }
    }
}