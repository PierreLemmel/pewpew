using PewPew.UI.Menus;

namespace PewPew.UI.Base.Tests.Menus.SampleClasses
{
    internal class SomeViewMenu : PewPewViewMenu
    {
        public SomeViewMenu() : base(new SomeViewModel())
        {
        }
    }
}