using Autofac;
using NFluent;
using NUnit.Framework;
using PewPew.UI.Base.Tests.Menus.SampleClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace PewPew.UI.Base.Tests.Menus
{
    public class ContainerBuilderExtensionsShould
    {
        [Test]
        [Apartment(ApartmentState.STA)]
        public void AddViewMenu_Should_Inject_Correct_Priority_In_Parent_Cosntructor()
        {
            ContainerBuilder builder = new ContainerBuilder();

            const int overwrittenPriority = 14;

            builder.AddViewMenu<SomeViewMenu>();
            builder.AddViewMenu<SomeOtherViewMenu>(overwrittenPriority);

            builder.RegisterType<InjectedViewMenuConstructorPriorityCapture>();

            IContainer container = builder.Build();

            IReadOnlyCollection<int> injectedPriorities = container
                .Resolve<InjectedViewMenuConstructorPriorityCapture>()
                .Priorities;

            Check.That(injectedPriorities)
                .IsEquivalentTo(0, overwrittenPriority);
        }
    }
}