using NFluent;
using NUnit.Framework;
using System.Collections.Generic;

using static PewPew.UI.DialogResult;

namespace PewPew.UI.Base.Tests.Dialogs
{
    public class DialogResultShould
    {
        #region Undefined
        [Test]
        public void Undefined_Is_Always_Same_Reference()
        {
            DialogResult undef1 = Undefined;
            DialogResult undef2 = Undefined;

            bool referenceEquals = ReferenceEquals(undef1, undef2);

            Check.That(referenceEquals).IsTrue();
        }

        [Test]
        public void Undefined_Is_Equal_To_Self()
        {
            DialogResult undef1 = Undefined;
            DialogResult undef2 = Undefined;

            bool eqOperator = (undef1 == undef2);

            Check.That(eqOperator).IsTrue();
        }

        [Test]
        [TestCaseSource(nameof(Undefined_OtherResults))]
        public void Undefined_Is_Not_Equal_To_Others(DialogResult other) => Check.That(Undefined == other).IsFalse();

        [Test]
        public void Undefined_Is_Equivalent_To_False()
        {
            bool boolUndefined = Undefined;
            Check.That(boolUndefined).IsFalse();
        }

        [Test]
        public void Undefined_Returns_Undefined_String() => Check.That(Undefined.ToString()).IsEqualTo(nameof(Undefined));

        [Test]
        public void Undefined_IsDefined() => Check.That(Undefined.IsDefined).IsFalse();
        [Test]
        public void Undefined_IsSuccess() => Check.That(Undefined.IsSuccess).IsFalse();
        [Test]
        public void Undefined_IsCanceled() => Check.That(Undefined.IsCanceled).IsFalse();
        [Test]
        public void Undefined_IsDenied() => Check.That(Undefined.IsDenied).IsFalse();
        [Test]
        public void Undefined_IsFailure() => Check.That(Undefined.IsFailure).IsFalse();

        public static IEnumerable<DialogResult> Undefined_OtherResults => new DialogResult[] { Ok, Yes, No, Canceled };
        #endregion

        #region Ok
        [Test]
        public void Ok_Is_Always_Same_Reference()
        {
            DialogResult ok1 = Ok;
            DialogResult ok2 = Ok;

            bool referenceEquals = ReferenceEquals(ok1, ok2);

            Check.That(referenceEquals).IsTrue();
        }

        [Test]
        public void Ok_Is_Equal_To_Self()
        {
            DialogResult ok1 = Ok;
            DialogResult ok2 = Ok;

            bool eqOperator = (ok1 == ok2);

            Check.That(eqOperator).IsTrue();
        }

        [Test]
        [TestCaseSource(nameof(Ok_OtherResults))]
        public void Ok_Is_Not_Equal_To_Others(DialogResult other) => Check.That(Ok == other).IsFalse();

        [Test]
        public void Ok_Is_Equivalent_To_True()
        {
            bool boolOk = Ok;
            Check.That(boolOk).IsTrue();
        }

        [Test]
        public void Ok_Returns_Ok_String() => Check.That(Ok.ToString()).IsEqualTo(nameof(Ok));

        [Test]
        public void Ok_IsDefined() => Check.That(Ok.IsDefined).IsTrue();
        [Test]
        public void Ok_IsSuccess() => Check.That(Ok.IsSuccess).IsTrue();
        [Test]
        public void Ok_IsCanceled() => Check.That(Ok.IsCanceled).IsFalse();
        [Test]
        public void Ok_IsDenied() => Check.That(Ok.IsDenied).IsFalse();
        [Test]
        public void Ok_IsFailure() => Check.That(Ok.IsFailure).IsFalse();

        public static IEnumerable<DialogResult> Ok_OtherResults => new DialogResult[] { Undefined, No, Canceled };
        #endregion

        #region Yes
        [Test]
        public void Yes_Is_Always_Same_Reference()
        {
            DialogResult yes1 = Yes;
            DialogResult yes2 = Yes;

            bool referenceEquals = ReferenceEquals(yes1, yes2);

            Check.That(referenceEquals).IsTrue();
        }

        [Test]
        public void Yes_Is_Equal_To_Self()
        {
            DialogResult yes1 = Yes;
            DialogResult yes2 = Yes;

            bool eqOperator = (yes1 == yes2);

            Check.That(eqOperator).IsTrue();
        }

        [Test]
        public void Yes_Is_Not_Same_Reference_As_Ok() => Check.That(ReferenceEquals(Yes, Ok)).IsFalse();

        [Test]
        public void Yes_Is_Equal_To_Ok() => Check.That(Yes == Ok).IsTrue();

        [Test]
        [TestCaseSource(nameof(Yes_OtherResults))]
        public void Yes_Is_Not_Equal_To_Others(DialogResult other) => Check.That(Yes == other).IsFalse();

        [Test]
        public void Yes_Is_Equivalent_To_True()
        {
            bool boolYes = Yes;
            Check.That(boolYes).IsTrue();
        }

        [Test]
        public void Yes_Returns_Yes_String() => Check.That(Yes.ToString()).IsEqualTo(nameof(Yes));

        [Test]
        public void Yes_IsDefined() => Check.That(Yes.IsDefined).IsTrue();
        [Test]
        public void Yes_IsSuccess() => Check.That(Yes.IsSuccess).IsTrue();
        [Test]
        public void Yes_IsCanceled() => Check.That(Yes.IsCanceled).IsFalse();
        [Test]
        public void Yes_IsDenied() => Check.That(Yes.IsDenied).IsFalse();
        [Test]
        public void Yes_IsFailure() => Check.That(Yes.IsFailure).IsFalse();

        public static IEnumerable<DialogResult> Yes_OtherResults => new DialogResult[] { Undefined, No, Canceled };
        #endregion

        #region No
        [Test]
        public void No_Is_Always_Same_Reference()
        {
            DialogResult no1 = No;
            DialogResult no2 = No;

            bool referenceEquals = ReferenceEquals(no1, no2);

            Check.That(referenceEquals).IsTrue();
        }

        [Test]
        public void No_Is_Equal_To_Self()
        {
            DialogResult no1 = No;
            DialogResult no2 = No;

            bool eqOperator = (no1 == no2);

            Check.That(eqOperator).IsTrue();
        }

        [Test]
        [TestCaseSource(nameof(No_OtherResults))]
        public void No_Is_Not_Equal_To_Others(DialogResult other) => Check.That(No == other).IsFalse();

        [Test]
        public void No_Is_Equivalent_To_False()
        {
            bool boolNo = No;
            Check.That(boolNo).IsFalse();
        }

        [Test]
        public void No_Returns_No_String() => Check.That(No.ToString()).IsEqualTo(nameof(No));

        [Test]
        public void No_IsDefined() => Check.That(No.IsDefined).IsTrue();
        [Test]
        public void No_IsSuccess() => Check.That(No.IsSuccess).IsFalse();
        [Test]
        public void No_IsCanceled() => Check.That(No.IsCanceled).IsFalse();
        [Test]
        public void No_IsDenied() => Check.That(No.IsDenied).IsTrue();
        [Test]
        public void No_IsFailure() => Check.That(No.IsFailure).IsTrue();

        public static IEnumerable<DialogResult> No_OtherResults => new DialogResult[] { Undefined, Ok, Yes, Canceled };
        #endregion

        #region Cancel
        [Test]
        public void Canceled_Is_Always_Same_Reference()
        {
            DialogResult cancel1 = Canceled;
            DialogResult cancel2 = Canceled;

            bool referenceEquals = ReferenceEquals(cancel1, cancel2);

            Check.That(referenceEquals).IsTrue();
        }

        [Test]
        public void Canceled_Is_Equal_To_Self()
        {
            DialogResult cancel1 = Canceled;
            DialogResult cancel2 = Canceled;

            bool eqOperator = (cancel1 == cancel2);

            Check.That(eqOperator).IsTrue();
        }

        [Test]
        [TestCaseSource(nameof(Canceled_OtherResults))]
        public void Canceled_Is_Not_Equal_To_Others(DialogResult other) => Check.That(Canceled == other).IsFalse();

        [Test]
        public void Canceled_Is_Equivalent_To_False()
        {
            bool boolCancel = Canceled;
            Check.That(boolCancel).IsFalse();
        }

        [Test]
        public void Canceled_Returns_Canceled_String() => Check.That(Canceled.ToString()).IsEqualTo(nameof(Canceled));

        [Test]
        public void Canceled_IsDefined() => Check.That(Canceled.IsDefined).IsTrue();
        [Test]
        public void Canceled_IsSuccess() => Check.That(Canceled.IsSuccess).IsFalse();
        [Test]
        public void Canceled_IsCanceled() => Check.That(Canceled.IsCanceled).IsTrue();
        [Test]
        public void Canceled_IsDenied() => Check.That(Canceled.IsDenied).IsFalse();
        [Test]
        public void Canceled_IsFailure() => Check.That(Canceled.IsFailure).IsTrue();


        public static IEnumerable<DialogResult> Canceled_OtherResults => new DialogResult[] { Undefined, Ok, Yes, No };
        #endregion
    }
}