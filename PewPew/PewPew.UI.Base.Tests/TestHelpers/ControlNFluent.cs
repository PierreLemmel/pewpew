using NFluent;
using NFluent.Extensibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace PewPew.UI.Base.Tests
{
    public static class ControlNFluent
    {
        public static ICheckLink<ICheck<DependencyObject>> ContainsInChildren<TChild>(this ICheck<DependencyObject> check)
            where TChild : DependencyObject
        {
            ExtensibilityHelper.BeginCheck(check)
                .FailWhen(reference => !reference.TryFindInChildren<TChild>(out _), $"Impossible to find any {typeof(TChild).Name} in children")
                .OnNegate($"{typeof(TChild).Name} found in children")
                .EndCheck();

            return ExtensibilityHelper.BuildCheckLink(check);
        }

        public static ICheckLink<ICheck<DependencyObject>> DoesNotContainInChildren<TChild>(this ICheck<DependencyObject> check)
            where TChild : DependencyObject
            => check.Not.ContainsInChildren<TChild>();

        public static ICheckLink<ICheck<DependencyObject>> ContainsInParents<TParent>(this ICheck<DependencyObject> check)
            where TParent : DependencyObject
        {
            ExtensibilityHelper.BeginCheck(check)
                .FailWhen(reference => !reference.TryFindInParents<TParent>(out _), $"Impossible to find any {typeof(TParent).Name} in parents")
                .OnNegate($"{typeof(TParent).Name} found in parents")
                .EndCheck();

            return ExtensibilityHelper.BuildCheckLink(check);
        }

        public static ICheckLink<ICheck<DependencyObject>> DoesNotContainInParents<TParent>(this ICheck<DependencyObject> check)
            where TParent : DependencyObject
            => check.Not.ContainsInParents<TParent>();
    }
}