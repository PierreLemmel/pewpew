using NFluent;
using NUnit.Framework;
using PewPew.Helpers;
using PewPew.UI.Base.Tests.SampleClasses;
using PewPew.UI.Layout;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Base.Tests.Helpers
{
    [Apartment(ApartmentState.STA)]
    public class ControlTreeHelpersShould
    {
        [Test]
        public void EnumerateChildren_On_Empty_Grid_Returns_Empty()
        {
            Grid empty = new();

            IEnumerable<DependencyObject> children = empty.EnumerateChildren();
            Check.That(children).IsEmpty();
        }

        [Test]
        public void EnumerateChildren_On_Grid_With_Children_Returns_Children()
        {
            Grid grid = Grids.Create(new Control[,]
            {
                { Some.Label, Some.Label },
                { Some.Label, Some.Label },
            });

            IEnumerable<DependencyObject> children = grid.EnumerateChildren();
            Check.That(children).CountIs(4);
            Check.That(children).All(child => child is Label);
        }

        [Test]
        public void EnumerateChildren_On_Multi_Level_Split_Returns_Expected()
        {
            Control complex = VerticallySplittedPanel.Create(
                new ContentControl() { Content = Some.LabelGrid(4, 5) },
                HorizontallySplittedPanel.Create(
                    Some.Label,
                    VerticallySplittedPanel.Create(
                        Some.Label,
                        Some.Label
                    )
                )
            );

            IEnumerable<DependencyObject> children = complex.EnumerateChildren();

            Check.That(children.OfType<Label>()).CountIs(23);
            Check.That(children.OfType<ContentControl>().Where(cc => cc is not Label)).CountIs(3);
            Check.That(children.OfType<Grid>()).CountIs(4);
            Check.That(children.OfType<GridSplitter>()).CountIs(3);

            Check.That(children).CountIs(33);
        }

        [Test]
        public void EnumerateChildren_On_Multi_Level_With_Specific_Type_Returns_Expected()
        {
            Control complex = VerticallySplittedPanel.Create(
                new ContentControl() { Content = Some.LabelGrid(4, 5) },
                HorizontallySplittedPanel.Create(
                    Some.Label,
                    VerticallySplittedPanel.Create(
                        Some.Label,
                        Some.Label
                    )
                )
            );

            IEnumerable<Label> children = complex.EnumerateChildren<Label>();

            Check.That(children).CountIs(23);
        }

        [Test]
        public void EnumerateParents_On_Empty_Grid_Returns_Empty()
        {
            Grid empty = new();

            IEnumerable<DependencyObject> parents = empty.EnumerateParents();
            Check.That(parents).IsEmpty();
        }

        [Test]
        public void EnumerateParents_On_Grid_With_Parents_Returns_Parents()
        {
            Grid root = Grids.Create(new Control[,]
            {
                { Some.Label, Some.Label },
                { 
                    Some.Label,
                    Some.Label.OutVar(out Label label)
                },
            });

            IEnumerable<DependencyObject> parents = label.EnumerateParents();

            Check.That(parents).IsEquivalentTo(root);
        }

        [Test]
        public void EnumerateParents_On_Multi_Level_Split_Returns_Expected()
        {
            Control root = VerticallySplittedPanel.Create(
                new ContentControl() { Content = Some.LabelGrid(4, 5) },
                HorizontallySplittedPanel.Create(
                    Some.Label,
                    VerticallySplittedPanel.Create(
                        Some.Label,
                        Some.Label.OutVar(out Control label)
                    ).OutVar(out Control split2)
                ).OutVar(out Control split1)
            );

            IEnumerable<DependencyObject> parents = label.EnumerateParents();

            Check.That(parents).CountIs(6);
            Check.That(parents.OfType<Grid>()).CountIs(3);
            Check.That(parents.OfType<GridControl>()).CountIs(3);
        }

        [Test]
        public void EnumerateParents_On_Multi_Level_With_Specific_Type_Returns_Expected()
        {
            GridControl root = VerticallySplittedPanel.Create(
                new ContentControl() { Content = Some.LabelGrid(4, 5) },
                HorizontallySplittedPanel.Create(
                    Some.Label,
                    VerticallySplittedPanel.Create(
                        Some.Label,
                        Some.Label.OutVar(out Label label)
                    ).OutVar(out GridControl split2)
                ).OutVar(out GridControl split1)
            );

            IEnumerable<GridControl> parents = label.EnumerateParents<GridControl>();

            Check.That(parents).IsEquivalentTo(
                split2,
                split1,
                root
            );
        }
    }
}