using Moq;
using NFluent;
using NUnit.Framework;
using PewPew.Events.Handlers;
using PewPew.Fakes;
using PewPew.UI.Base.Tests.SampleClasses;
using PewPew.UI.Events.User;
using PewPew.UI.Orchestration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Base.Tests.Orchestration
{
    [Apartment(ApartmentState.STA)]
    public class TabbedMDIControlOrchestratorShould
    {
        [Test]
        public void ThrowInvalidOperationException_When_Orchestration_Is_Started_Twice()
        {
            TabbedMDIControlOrchestrator orchestrator = CreateSUT();

            Window window = BuildFakeWindow();

            orchestrator.StartOrchestration(window);

            Check.ThatCode(() => orchestrator.StartOrchestration(window))
                .Throws<InvalidOperationException>();
        }

        [Test]
        public void Be_Registered_After_Construction()
        {
            (IHandlerRegistrar registrar, TabbedMDIControlOrchestrator sut) = CreateFullSUT();

            Check.That(sut).IsRegisteredIn(registrar);
        }

        [Test]
        public void Be_Unregistered_After_Disposing()
        {
            (IHandlerRegistrar registrar, TabbedMDIControlOrchestrator sut) = CreateFullSUT();

            sut.Dispose();
            Check.That(sut).IsNotRegisteredIn(registrar);
        }

        [Test]
        public void Handle_ControlRequested_When_Control_Does_Not_Exist_Yet()
        {
            TabbedMDIControlOrchestrator sut = CreateSUT();
            Window orchestrated = Some.WindowWithRootContainer;

            sut.StartOrchestration(orchestrated);

            ControlRequested sppcRequested = new ControlRequested<SomePewPewControl>();

            Check.ThatCode(() => sut.Handle(sppcRequested))
                .DoesNotThrow();
        }

        [Test]
        public void Handle_ControlRequested_When_Control_Has_Already_Been_Added()
        {
            TabbedMDIControlOrchestrator sut = CreateSUT();
            Window orchestrated = Some.WindowWithRootContainer;

            sut.StartOrchestration(orchestrated);

            ControlRequested firstRequest = new ControlRequested<SomePewPewControl>();
            ControlRequested secondRequest = new ControlRequested<SomePewPewControl>();

            sut.Handle(firstRequest);

            Check.ThatCode(() => sut.Handle(secondRequest))
                .DoesNotThrow();
        }

        [Test]
        public void Contain_Control_When_Added_On_Root()
        {
            TabbedMDIControlOrchestrator sut = CreateSUT();
            Window orchestrated = Some.WindowWithRootContainer;

            sut.StartOrchestration(orchestrated);

            ControlRequested sppcRequested = new ControlRequested<SomePewPewControl>();

            sut.Handle(sppcRequested);

            Check.That(orchestrated)
                .ContainsInChildren<SomePewPewControl>();
        }

        private Window BuildFakeWindow() => Some.WindowWithRootContainer;

        private TabbedMDIControlOrchestrator CreateSUT() => CreateFullSUT().orchestrator;
        private (IHandlerRegistrar registrar, TabbedMDIControlOrchestrator orchestrator) CreateFullSUT()
        {
            Mock<IPewPewControlFactory> factoryMock = new();
            factoryMock.Setup(f => f.CreateControl(It.IsAny<Type>()))
                .Returns((Type type) => (PewPewControl)Activator.CreateInstance(type)!);

            IHandlerRegistrar registrar = new FakeRegistrar();

            return (registrar, new(factoryMock.Object, registrar));
        }
    }
}