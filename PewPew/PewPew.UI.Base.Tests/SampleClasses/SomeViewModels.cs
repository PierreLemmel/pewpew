namespace PewPew.UI.Base.Tests.SampleClasses
{
    public class SomeViewModel : ViewModel<SomeViewModel> { }
    public class SomeOtherViewModel : ViewModel<SomeOtherViewModel> { }
    public class YetSomeOtherViewModel : ViewModel<YetSomeOtherViewModel> { }

    public class SomeViewModel1 : ViewModel<SomeViewModel1> { }
    public class SomeOtherViewModel1 : ViewModel<SomeOtherViewModel1> { }
    public class YetSomeOtherViewModel1 : ViewModel<YetSomeOtherViewModel1> { }

    public class SomeViewModel2 : ViewModel<SomeViewModel2> { }
    public class SomeOtherViewModel2 : ViewModel<SomeOtherViewModel2> { }
    public class YetSomeOtherViewModel2 : ViewModel<YetSomeOtherViewModel2> { }

    public class SomeViewModel3 : ViewModel<SomeViewModel3> { }
    public class SomeOtherViewModel3 : ViewModel<SomeOtherViewModel3> { }
    public class YetSomeOtherViewModel3 : ViewModel<YetSomeOtherViewModel3> { }

    public class SomeViewModel4 : ViewModel<SomeViewModel4> { }
    public class SomeOtherViewModel4 : ViewModel<SomeOtherViewModel4> { }
    public class YetSomeOtherViewModel4 : ViewModel<YetSomeOtherViewModel4> { }

    public class SomeViewModel5 : ViewModel<SomeViewModel5> { }
    public class SomeOtherViewModel5 : ViewModel<SomeOtherViewModel5> { }
    public class YetSomeOtherViewModel5 : ViewModel<YetSomeOtherViewModel5> { }

    public class SomeViewModel6 : ViewModel<SomeViewModel6> { }
    public class SomeOtherViewModel6 : ViewModel<SomeOtherViewModel6> { }
    public class YetSomeOtherViewModel6 : ViewModel<YetSomeOtherViewModel6> { }

    public class SomeViewModel7 : ViewModel<SomeViewModel7> { }
    public class SomeOtherViewModel7 : ViewModel<SomeOtherViewModel7> { }
    public class YetSomeOtherViewModel7 : ViewModel<YetSomeOtherViewModel7> { }
    public class SomeViewModel8 : ViewModel<SomeViewModel8> { }
    public class SomeOtherViewModel8 : ViewModel<SomeOtherViewModel8> { }
    public class YetSomeOtherViewModel8 : ViewModel<YetSomeOtherViewModel8> { }

    public class SomeViewModel9 : ViewModel<SomeViewModel9> { }
    public class SomeOtherViewModel9 : ViewModel<SomeOtherViewModel9> { }
    public class YetSomeOtherViewModel9 : ViewModel<YetSomeOtherViewModel9> { }

    public class SomeCompletelyDifferentViewModel : ViewModel<SomeCompletelyDifferentViewModel> { }
}