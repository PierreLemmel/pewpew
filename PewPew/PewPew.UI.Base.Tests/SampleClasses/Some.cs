using AutoFixture;
using PewPew.UI.Controls;
using PewPew.UI.Orchestration;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace PewPew.UI.Base.Tests.SampleClasses
{
    internal static class Some
    {
        public static Window WindowWithRootContainer => new() { Content = new RootContainer() };


        private static readonly IFixture fixture = new Fixture();
        public static string String => fixture.Create<string>();

        public static Label Label => Labels.Create(String);

        public static Grid LabelGrid(int rows, int cols) => Grids.Create(Arrays.Bidimensionnal(rows, cols, () => Label));
        public static Grid VerticalLabelGrid(int count) => Grids.Vertical(Arrays.Create(count, () => Label));
        public static Grid HorizontalLabelGrid(int count) => Grids.Horizontal(Arrays.Create(count, () => Label));
    }
}