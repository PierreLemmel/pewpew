using Moq;
using PewPew.UI.Events;

namespace PewPew.UI.Base.Tests.SampleClasses
{
    public class SomePewPewControl : PewPewControl { public SomePewPewControl() : base(new SomeViewModel(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl : PewPewControl { public SomeOtherPewPewControl() : base(new SomeOtherViewModel(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl : PewPewControl { public YetSomeOtherPewPewControl() : base(new YetSomeOtherViewModel(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl1 : PewPewControl { public SomePewPewControl1() : base(new SomeViewModel1(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl1 : PewPewControl { public SomeOtherPewPewControl1() : base(new SomeOtherViewModel1(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl1 : PewPewControl { public YetSomeOtherPewPewControl1() : base(new YetSomeOtherViewModel1(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl2 : PewPewControl { public SomePewPewControl2() : base(new SomeViewModel2(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl2 : PewPewControl { public SomeOtherPewPewControl2() : base(new SomeOtherViewModel2(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl2 : PewPewControl { public YetSomeOtherPewPewControl2() : base(new YetSomeOtherViewModel2(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl3 : PewPewControl { public SomePewPewControl3() : base(new SomeViewModel3(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl3 : PewPewControl { public SomeOtherPewPewControl3() : base(new SomeOtherViewModel3(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl3 : PewPewControl { public YetSomeOtherPewPewControl3() : base(new YetSomeOtherViewModel3(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl4 : PewPewControl { public SomePewPewControl4() : base(new SomeViewModel4(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl4 : PewPewControl { public SomeOtherPewPewControl4() : base(new SomeOtherViewModel4(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl4 : PewPewControl { public YetSomeOtherPewPewControl4() : base(new YetSomeOtherViewModel4(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl5 : PewPewControl { public SomePewPewControl5() : base(new SomeViewModel5(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl5 : PewPewControl { public SomeOtherPewPewControl5() : base(new SomeOtherViewModel5(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl5 : PewPewControl { public YetSomeOtherPewPewControl5() : base(new YetSomeOtherViewModel5(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl6 : PewPewControl { public SomePewPewControl6() : base(new SomeViewModel6(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl6 : PewPewControl { public SomeOtherPewPewControl6() : base(new SomeOtherViewModel6(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl6 : PewPewControl { public YetSomeOtherPewPewControl6() : base(new YetSomeOtherViewModel6(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl7 : PewPewControl { public SomePewPewControl7() : base(new SomeViewModel7(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl7 : PewPewControl { public SomeOtherPewPewControl7() : base(new SomeOtherViewModel7(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl7 : PewPewControl { public YetSomeOtherPewPewControl7() : base(new YetSomeOtherViewModel7(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl8 : PewPewControl { public SomePewPewControl8() : base(new SomeViewModel8(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl8 : PewPewControl { public SomeOtherPewPewControl8() : base(new SomeOtherViewModel8(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl8 : PewPewControl { public YetSomeOtherPewPewControl8() : base(new YetSomeOtherViewModel8(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomePewPewControl9 : PewPewControl { public SomePewPewControl9() : base(new SomeViewModel9(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class SomeOtherPewPewControl9 : PewPewControl { public SomeOtherPewPewControl9() : base(new SomeOtherViewModel9(), Mock.Of<IEventSender<ControlEvent>>()) { } }
    public class YetSomeOtherPewPewControl9 : PewPewControl { public YetSomeOtherPewPewControl9() : base(new YetSomeOtherViewModel9(), Mock.Of<IEventSender<ControlEvent>>()) { } }

    public class SomeCompletlyDifferentControl : PewPewControl { public SomeCompletlyDifferentControl() : base(new SomeCompletelyDifferentViewModel(), Mock.Of<IEventSender<ControlEvent>>()) { } }
}