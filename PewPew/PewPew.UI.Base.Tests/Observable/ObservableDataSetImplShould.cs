using AutoFixture;
using NFluent;
using NUnit.Framework;
using PewPew.DataAccess;
using PewPew.DataAccess.Events;
using PewPew.Events.Handlers;
using PewPew.Fakes;
using PewPew.UI.Base.Tests.Observable.SampleClasses;
using PewPew.UI.Observable;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace PewPew.UI.Base.Tests.Observable
{
    public class ObservableDataSetImplShould
    {
        private readonly Guid id1 = Guid.NewGuid();
        private readonly Guid id2 = Guid.NewGuid();
        private readonly Guid id3 = Guid.NewGuid();
        private readonly Guid id4 = Guid.NewGuid();
        private readonly Guid id5 = Guid.NewGuid();

        private readonly SomeModel model1;
        private readonly SomeModel model2;
        private readonly SomeModel model3;
        private readonly SomeModel model4;
        private readonly SomeModel model5;

        private readonly IFixture fixture = new Fixture();

        public ObservableDataSetImplShould()
        {
            model1 = fixture.Create<SomeModel>();
            model2 = fixture.Create<SomeModel>();
            model3 = fixture.Create<SomeModel>();
            model4 = fixture.Create<SomeModel>();
            model5 = fixture.Create<SomeModel>();
        }

        private IReadOnlyList<SomeModel> GetDemoData() => new List<SomeModel>
        {
            model1,
            model2,
            model3,
            model4,
            model5,
        };


        #region Content
        [Test]
        public void Be_Equivalent_To_Model_Reader_Content_On_Creation()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            Check.That(observableDataSet).IsEquivalentTo(GetDemoData());
        }

        [Test]
        public void Be_Empty_After_DataSetCleared_Event()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            DataSetCleared<SomeModel> dataSetCleared = new();
            observableDataSet.Handle(dataSetCleared);

            Check.That(observableDataSet).IsEmpty();
        }


        [Test]
        public void Have_Expected_Content_After_EntityCreated_Event()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            SomeModel newModel = fixture.Create<SomeModel>();
            EntityCreated<SomeModel> entityCreated = new(newModel);

            observableDataSet.Handle(entityCreated);

            Check.That(observableDataSet).IsEquivalentTo(
                GetDemoData()
                .Append(newModel)
            );
        }

        [Test]
        public void Have_Expected_Content_After_EntitiesCreated_Event()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            IEnumerable<SomeModel> newModels = fixture.CreateMany<SomeModel>();
            EntitiesCreated<SomeModel> entitiesCreated = new(newModels);

            observableDataSet.Handle(entitiesCreated);

            Check.That(observableDataSet).IsEquivalentTo(
                GetDemoData()
                .Append(newModels)
            );
        }


        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void Have_Expected_Content_After_EntityUpdated_Event(int index)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            Guid initialId = initialData[index].Id;
            SomeModel updatedModel = fixture.Build<SomeModel>()
                .With(m => m.Id, initialId)
                .Create();
                
            EntityUpdated<SomeModel> entityUpdated = new(updatedModel);

            observableDataSet.Handle(entityUpdated);

            Check.That(observableDataSet).IsEquivalentTo(
                GetDemoData()
                .ReplaceAtIndex(index, updatedModel)
            );
        }

        [Test]
        [TestCase(new int[] { 0, 2, 3 })]
        [TestCase(new int[] { 1, 3, 4 })]
        [TestCase(new int[] { 0, 2, 4 })]
        public void Have_Expected_Content_After_EntitiesUpdated_Event(int[] indexes)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            IEnumerable<SomeModel> updatedEntities = indexes.Select(i =>
            {
                Guid initialId = initialData[i].Id;

                SomeModel updatedModel = fixture.Build<SomeModel>()
                            .With(m => m.Id, initialId)
                            .Create();

                return updatedModel;
            });


            EntitiesUpdated<SomeModel> entitiesUpdated = new(updatedEntities);

            observableDataSet.Handle(entitiesUpdated);

            IEnumerable<(int index, SomeModel updatedModel)> replaceData = indexes.Zip(updatedEntities);
            Check.That(observableDataSet).IsEquivalentTo(
                GetDemoData()
                .ReplaceAtIndexes(replaceData)
            );
        }


        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void Have_Expected_Content_After_EntityDeleted_Event(int index)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            Guid deletedId = initialData[index].Id;

            EntityDeleted<SomeModel> entityDeleted = new(deletedId);
            observableDataSet.Handle(entityDeleted);

            Check.That(observableDataSet).IsEquivalentTo(
                GetDemoData()
                .WithoutIndex(index)
            );
        }

        [Test]
        [TestCase(new int[] { 0, 2, 3 })]
        [TestCase(new int[] { 1, 3, 4 })]
        [TestCase(new int[] { 0, 2, 4 })]
        public void Have_Expected_Content_After_EntitiesDeleted_Event(int[] indexes)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            IEnumerable<Guid> entitiesId = indexes.Select(i => initialData[i].Id);

            EntitiesDeleted<SomeModel> entitiesDeleted = new(entitiesId);
            observableDataSet.Handle(entitiesDeleted);

            Check.That(observableDataSet).IsEquivalentTo(
                GetDemoData()
                .WithoutIndexes(indexes)
            );
        }
        #endregion

        #region Raise Events
        [Test]
        public void Raise_CollectionChanged_On_DataSetCleared_Event()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            bool triggered = false;
            NotifyCollectionChangedAction? action = null;
            observableDataSet.CollectionChanged += (sender, e) =>
            {
                triggered = true;
                action = e.Action;
            };

            DataSetCleared<SomeModel> dataSetCleared = new();
            observableDataSet.Handle(dataSetCleared);

            Check.That(triggered).IsTrue();
            Check.That(action).IsEqualTo(NotifyCollectionChangedAction.Reset);
        }


        [Test]
        public void Raise_CollectionChanged_On_EntityCreated_Event()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            bool triggered = false;
            NotifyCollectionChangedAction? action = null;
            observableDataSet.CollectionChanged += (sender, e) =>
            {
                triggered = true;
                action = e.Action;
            };

            SomeModel newModel = fixture.Create<SomeModel>();
            EntityCreated<SomeModel> entityCreated = new(newModel);

            observableDataSet.Handle(entityCreated);

            Check.That(triggered).IsTrue();
            Check.That(action).IsEqualTo(NotifyCollectionChangedAction.Add);
        }

        [Test]
        public void Raise_CollectionChanged_On_EntitiesCreated_Event()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            bool triggered = false;
            NotifyCollectionChangedAction? action = null;
            observableDataSet.CollectionChanged += (sender, e) =>
            {
                triggered = true;
                action = e.Action;
            };

            IEnumerable<SomeModel> newModels = fixture.CreateMany<SomeModel>();
            EntitiesCreated<SomeModel> entitiesCreated = new(newModels);

            observableDataSet.Handle(entitiesCreated);

            Check.That(triggered).IsTrue();
            Check.That(action).IsEqualTo(NotifyCollectionChangedAction.Add);
        }


        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void Raise_CollectionChanged_On_EntityUpdated_Event(int index)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            bool triggered = false;
            NotifyCollectionChangedAction? action = null;
            observableDataSet.CollectionChanged += (sender, e) =>
            {
                triggered = true;
                action = e.Action;
            };

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            Guid initialId = initialData[index].Id;
            SomeModel updatedModel = fixture.Build<SomeModel>()
                .With(m => m.Id, initialId)
                .Create();

            EntityUpdated<SomeModel> entityUpdated = new(updatedModel);
            observableDataSet.Handle(entityUpdated);

            Check.That(triggered).IsTrue();
            Check.That(action).IsEqualTo(NotifyCollectionChangedAction.Replace);
        }

        [Test]
        [TestCase(new int[] { 0, 2, 3 })]
        [TestCase(new int[] { 1, 3, 4 })]
        [TestCase(new int[] { 0, 2, 4 })]
        public void Raise_CollectionChanged_On_EntitiesUpdated_Event(int[] indexes)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            bool triggered = false;
            NotifyCollectionChangedAction? action = null;
            observableDataSet.CollectionChanged += (sender, e) =>
            {
                triggered = true;
                action = e.Action;
            };

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            IEnumerable<SomeModel> updatedEntities = indexes.Select(i =>
            {
                Guid initialId = initialData[i].Id;

                SomeModel newItem = fixture.Build<SomeModel>()
                    .With(m => m.Id, initialId)
                    .Create();
                
                return newItem;
            });

            EntitiesUpdated<SomeModel> entitiesUpdated = new(updatedEntities);
            observableDataSet.Handle(entitiesUpdated);

            Check.That(triggered).IsTrue();
            Check.That(action).IsEqualTo(NotifyCollectionChangedAction.Replace);
        }


        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void Raise_CollectionChanged_On_EntityDeleted_Event(int index)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            bool triggered = false;
            NotifyCollectionChangedAction? action = null;
            observableDataSet.CollectionChanged += (sender, e) =>
            {
                triggered = true;
                action = e.Action;
            };

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            Guid entityId = initialData[index].Id;

            EntityDeleted<SomeModel> entityDeleted = new(entityId);
            observableDataSet.Handle(entityDeleted);

            Check.That(triggered).IsTrue();
            Check.That(action).IsEqualTo(NotifyCollectionChangedAction.Remove);
        }

        /// <summary>
        /// We check for 'Reset' instead of 'Remove' because 'Remove' dan be handled inproperly
        /// when removed entities are not adjacent
        /// </summary>
        /// <param name="indexes"></param>
        [Test]
        [TestCase(new int[] { 0, 2, 3 })]
        [TestCase(new int[] { 1, 3, 4 })]
        [TestCase(new int[] { 0, 2, 4 })]
        public void Raise_CollectionChanged__With_Reset_Event_On_EntitiesDeleted_Event(int[] indexes)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            bool triggered = false;
            NotifyCollectionChangedAction? action = null;
            observableDataSet.CollectionChanged += (sender, e) =>
            {
                triggered = true;
                action = e.Action;
            };

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            IEnumerable<Guid> entitiesId = indexes.Select(i => initialData[i].Id);

            EntitiesDeleted<SomeModel> entitiesDeleted = new(entitiesId);
            observableDataSet.Handle(entitiesDeleted);

            Check.That(triggered).IsTrue();
            Check.That(action).IsEqualTo(NotifyCollectionChangedAction.Reset);
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void Provide_Deleted_Indices_On_EntityDeleted_Event(int index)
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, _) = CreateSystemUnderTest();

            int? oldStartingIndex = null;
            observableDataSet.CollectionChanged += (sender, e) =>
            {
                oldStartingIndex = e.OldStartingIndex;
            };

            IReadOnlyList<SomeModel> initialData = GetDemoData();
            Guid entityId = initialData[index].Id;

            EntityDeleted<SomeModel> entityDeleted = new(entityId);
            observableDataSet.Handle(entityDeleted);

            Check.That(oldStartingIndex).IsEqualTo(index);
        }
        #endregion

        #region Registration
        [Test]
        public void Register_To_Registrar_On_Creation()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, IHandlerRegistrar registrar) = CreateSystemUnderTest();

            Check.That(observableDataSet).IsRegisteredIn(registrar);
        }

        [Test]
        public void Unregister_To_Registrar_On_Disposed()
        {
            (ObservableDataSetImpl<SomeModel> observableDataSet, IHandlerRegistrar registrar) = CreateSystemUnderTest();

            observableDataSet.Dispose();

            Check.That(observableDataSet).IsNotRegisteredIn(registrar);
        }
        #endregion

        private (ObservableDataSetImpl<SomeModel> observableDataSet, IHandlerRegistrar registrar) CreateSystemUnderTest()
        {
            IHandlerRegistrar registrar = new FakeRegistrar();
            IModelReader<SomeModel> modelReader = new FakeModelReader<SomeModel>(GetDemoData());

            ObservableDataSetImpl<SomeModel> observableDataSet = new(registrar, modelReader);

            return (observableDataSet, registrar);
        }
    }
}