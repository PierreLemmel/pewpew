using System;

namespace PewPew.UI.Base.Tests.Observable.SampleClasses
{
    public class SomeModel : EntityModel, IEquatable<SomeModel>
    {
        public SomeModel(Guid id, string name) : base(id, name)
        {
        }

        public bool Equals(SomeModel? other) => other != null && other.Id == Id && other.Name == Name;
    }
}