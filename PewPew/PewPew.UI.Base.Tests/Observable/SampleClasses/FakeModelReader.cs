using PewPew.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PewPew.UI.Base.Tests.Observable.SampleClasses
{
    public class FakeModelReader<TModel> : IModelReader<TModel> where TModel : EntityModel
    {
        private readonly List<TModel> items;

        public FakeModelReader(IEnumerable<TModel> items) => this.items = items.ToList();

        public IReadOnlyCollection<TModel> GetAll() => items;
        public TModel GetById(Guid id) => items.Single(item => item.Id == id);
        public TModel GetByName(string name) => items.Single(item => item.Name == name);

        public IReadOnlyCollection<TModel> GetRange(IEnumerable<Guid> ids) => ids.Select(GetById).ToList();
    }
}