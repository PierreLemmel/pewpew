﻿using Autofac;
using Microsoft.Extensions.Configuration;
using PewPew.DataAccess;
using PewPew.DataAccess.Models;

namespace PewPew.DataMigration
{
    public static class Bootstrap
    {
        public static IContainer BuildContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

#if DEBUG
            string environment = "debug";
#elif RELEASE
            string environment = "release";
#else
#error Unexpected environment
#endif

            IConfigurationBuilder cfgBuilder = new ConfigurationBuilder();

            cfgBuilder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            cfgBuilder.AddJsonFile($"appsettings.{environment}.json", optional: false, reloadOnChange: true);

            IConfiguration config = cfgBuilder.Build();

            builder.RegisterInstance(config);

            builder.RegisterConfigProvider<DataAccessOptions>("DataAccess");

            builder.AddDataAccess();

            IContainer container = builder.Build();

            return container;
        }
    }
}
