﻿using Autofac;
using PewPew.DataAccess.Services;
using System;

namespace PewPew.DataMigration
{
    class Program
    {
        static void Main(string[] args)
        {
            IContainer container = Bootstrap.BuildContainer();

            IDbMigrator migrator = container.Resolve<IDbMigrator>();

            Log.Info("Beginning of StartupDbMigration");

            Log.Trace("Checking for Db Creation");
            migrator.CreateDatabaseIfNeeded();
            Log.Trace("Db Creation checked");

            Log.Trace("Checking if database schema is up to date");
            migrator.CreateSchemaIfNeeded();
            Log.Trace("Database schema checked");

            Log.Info("End of StartupDbMigration");
        }
    }
}
